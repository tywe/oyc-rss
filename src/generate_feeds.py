import pathlib
import pickle
import os
import yaml
from urllib.parse import quote

# pip
from podgen import Podcast, Episode, Media, Category, Person
from bs4 import BeautifulSoup
from loguru import logger

# local
import urls
from generate_images import make_image
from web_scrape import get_courses
from index import INDEX_HTML_BASE


def generate_feeds(courses):
    feeds = []
    for course in courses:
        output_path = f"../public/{course.id}"
        pathlib.Path(output_path).mkdir(parents=True, exist_ok=True)
        pathlib.Path(f"{output_path}/img").mkdir(parents=True, exist_ok=True)

        logger.debug(f"Generating feed for {course.number}")

        course_image = make_image(
            title=course.number,  # Size is more predictable
            subtitle=course.title,
            footer=f"Professor: {course.professor}",
            file_path=f"{output_path}/img/course.jpeg",
        )

        course_feed = Podcast(
            name=f"{course.number}: {course.title}",
            description=f"From Open Yale Courses. {course.number}, {course.semester}. {course.description}",
            explicit=True,
            website=course.url,
            copyright="Yale University, CC BY-NC-SA 3.0",
            authors=[Person(course.professor), Person("Yale University")],
            language="en-US",
            category=Category("Education", "Courses"),
            feed_url=f"{urls.FEED_BASE_URL}/{course.id}/feed.rss",
            image=f"{urls.FEED_BASE_URL}/{course.id}/img/course.jpeg",
            complete=True,
        )

        for index, lecture in enumerate(course.lectures):
            episode_number = f"{index+1:02d}"
            image_name = f"episode-{episode_number}.jpeg"

            if not os.getenv("SKIP_IMAGE_GENERATION"):
                episode_image = make_image(
                    title=course.number,  # Size is more predictable
                    subtitle=lecture.id,
                    footer=f"Professor: {course.professor}",
                    file_path=f"{output_path}/img/{image_name}",
                )

            overly_escaped_url = quote(lecture.mp3_url)
            escaped_url = overly_escaped_url.replace("%3A//", "://")

            new_episode = Episode(
                title=f"{lecture.id}: {lecture.title}",
                summary=lecture.description,
                position=episode_number,
                image=f"{urls.FEED_BASE_URL}/{course.id}/img/{image_name}",
                media=Media(
                    url=escaped_url,
                    type="audio/mpeg",
                    duration=lecture.mp3_duration,
                    size=lecture.mp3_size,
                ),
            )
            course_feed.add_episode(new_episode)

        course_feed.rss_file(f"{output_path}/feed.rss")
        feeds.append(course_feed)

    return feeds


def generate_index(feeds):
    soup = BeautifulSoup(INDEX_HTML_BASE, "html.parser")
    table = soup.find("table")
    for feed in feeds:
        course_row = soup.new_tag("tr")

        feed_name = soup.new_tag("td")
        feed_name.append(feed.name)

        feed_url = soup.new_tag("td")
        a = soup.new_tag("a")
        a.attrs["href"] = feed.feed_url
        a.string = feed.feed_url
        feed_url.append(a)
        course_row.append(feed_name)
        course_row.append(feed_url)
        table.append(course_row)

    with open("../public/index.html", "w", encoding="utf-8") as file:
        file.write(str(soup))


def read_courses_file(filename="courses.pickle"):
    with open(filename, "rb") as file:
        courses = pickle.load(file)
    return courses


def write_courses_file(courses, filename="courses.pickle"):
    logger.debug(f"Writing course information to {filename}")
    with open(filename, "wb") as file:
        pickle.dump(courses, file)
    logger.debug("Saving course information to src/courses.yaml")
    with open("courses.yaml", "w") as yaml_file:
        yaml.dump(courses, yaml_file)



if __name__ == "__main__":
    logger.info("Getting course information")
    if os.getenv("USE_COURSE_PICKLE"):
        courses = read_courses_file()
    else:
        courses = get_courses(f"{urls.COURSES_BASE_URL}/courses")
        write_courses_file(courses)

    logger.info("Generating feeds and images")
    feeds = generate_feeds(courses)
    generate_index(feeds)
