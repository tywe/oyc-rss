import urls

INDEX_HTML_BASE = f"""
<!doctype html>
<!--
DO NOT EDIT INDEX.HTML DIRECTLY
It's automatically generated based on src/index.py
Make changes there
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>OYC feed index</title>
  <meta name="description" content="Index of available OYC feeds from this site">
  <meta name="author" content="Tyler Wengerd">

  <meta property="og:title" content="OYC feed index<">
  <meta property="og:type" content="website">
  <meta property="og:url" content="{urls.FEED_BASE_URL}">
  <meta property="og:description" content="Index of available OYC feeds from this site">
  <link rel="stylesheet" href="main.css" type="text/css">
</head>

<body>
<table>
<tr>
<th>Course information</th>
<th>Feed link</th>
</tr>
<!-- Begin generated course info -->
<!-- End generated course info -->
</table>
<footer>
<a href="https://oyc.yale.edu/courses">Open Yale Courses at yale.edu</a> | 
<a href="https://archive.org/details/openyalecourses">Open Yale Courses archive at archive.org</a> | 
<a href="https://gitlab.com/tywe/oyc-rss">What is this?</a> |
<a href="https://creativecommons.org/licenses/by-nc-sa/3.0/">CC BY-NC-SA 3.0</a>
</footer>
</body>
</html>
"""
