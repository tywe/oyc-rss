# pip
import textwrap
from PIL import Image, ImageDraw, ImageFont

IMAGE_WIDTH = IMAGE_HEIGHT = 1500
YALE_BLUE = "#00356B"
YALE_LIGHT_GREY = "#F9F9F9"
FONT = "../fonts/title/Rosarivo-Regular.ttf"
HEADER = "Open Yale Courses"

HEADER_FONT = ImageFont.truetype(FONT, size=50)
TITLE_FONT = ImageFont.truetype(FONT, size=HEADER_FONT.size * 5)
SUBTITLE_FONT = ImageFont.truetype(FONT, size=int(HEADER_FONT.size * 2.25))
FOOTER_FONT = ImageFont.truetype(FONT, size=int(HEADER_FONT.size * 1.25))


def make_image(title, subtitle, footer, file_path, header=HEADER, resize_dimensions=None):
    img = Image.new("RGB", (IMAGE_WIDTH, IMAGE_HEIGHT), color=YALE_BLUE)
    canvas = ImageDraw.Draw(img)

    xy_coordinates = [int(IMAGE_WIDTH / 2), HEADER_FONT.size * 2]
    header = canvas.text(xy_coordinates, text=header, anchor="mt", font=HEADER_FONT, fill=YALE_LIGHT_GREY)

    xy_coordinates[1] += TITLE_FONT.size
    title = canvas.text(xy_coordinates, text=title, anchor="mt", font=TITLE_FONT, fill=YALE_LIGHT_GREY)

    wrapper = textwrap.TextWrapper(width=25)
    lines = wrapper.wrap(text=subtitle)
    subtitle_lines = "\n".join(lines)
    xy_coordinates[1] += TITLE_FONT.size + SUBTITLE_FONT.size
    subtitle = canvas.multiline_text(
        xy_coordinates,
        text=subtitle_lines,
        anchor="ma",
        align="center",
        spacing=int(SUBTITLE_FONT.size / 4),
        font=SUBTITLE_FONT,
        fill=YALE_LIGHT_GREY,
    )

    xy_coordinates[1] = IMAGE_HEIGHT - FOOTER_FONT.size * 3
    footer = canvas.text(xy_coordinates, text=footer, anchor="mt", font=FOOTER_FONT, fill=YALE_LIGHT_GREY)

    if resize_dimensions:
        img = img.resize(resize_dimensions)
    img.save(file_path, "jpeg", optimize=True, quality=85)

    return img


if __name__ == "__main__":
    make_image(
        title="OYC-RSS",
        subtitle="Open Yale Courses Podcast Feed Generator",
        footer="By Tyler Wengerd - Thanks to OYC",
        header="Oh hey it's",
        file_path="./cover.jpg",
        resize_dimensions=(500, 500),
    )
