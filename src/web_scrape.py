import pathlib
import re
import requests
import xml.etree.ElementTree as ET
from types import SimpleNamespace
from datetime import time, timedelta, datetime as dt

# pip
from bs4 import BeautifulSoup
from loguru import logger

# local
import urls


def get_courses(url):
    courses = []

    courses_response = requests.get(url)
    courses_page = BeautifulSoup(courses_response.text, "html.parser")

    courses_table = courses_page.find("table", attrs={"class": "views-table"})
    table_body = courses_table.find("tbody")
    course_rows = table_body.find_all("tr")

    # Uncomment to only use certain courses for testing
    # course_rows = course_rows[30:32]

    for index, row in enumerate(course_rows):
        logger.debug(f"Getting details for course: {index+1}/{len(course_rows)}")
        course = get_course_details(row)
        courses.append(course)

    add_archive_mp3_info(courses)
    return courses


def get_course_details(course_row):
    course = SimpleNamespace(
        department=course_row.find("td", attrs={"class": "views-field views-field-title active"}).find("a").text,
        number=course_row.find("td", attrs={"class": "views-field views-field-field-course-number"}).find("a").text,
        title=course_row.find("td", attrs={"class": "views-field views-field-title-1"}).find("a").text,
        professor=course_row.find("td", attrs={"class": "views-field views-field-field-professors-last-name"}).text.strip(),
        semester=course_row.find("td", attrs={"class": "views-field views-field-field-semester"}).text.strip(),
        url=f"{urls.COURSES_BASE_URL}{course_row.find('td', attrs={'class': 'views-field views-field-field-course-number'}).find('a').get('href')}",
    )
    # Exmple: afam-162-2010
    # Doing this because of ECON 252
    course.id = f"{course.number} {course.semester.split(' ')[-1]}".lower().replace(" ", "-")
    logger.debug(f"Course: {course.id}")
    course_page = get_page(course.url)
    course.lectures = get_lectures(course_page)
    course.description = get_description(course_page, page_type="course")

    return course


def get_page(url):
    response = requests.get(url)
    return BeautifulSoup(response.text, "html.parser")


def get_lectures(course_page):
    lectures = []

    sessions_table = course_page.find("table", attrs={"class": "views-table cols-0"})

    table_body = sessions_table.find("tbody")
    rows = table_body.find_all("tr")

    for row in rows:
        session = get_session_details(row)
        logger.debug(f"Session: {session.id}")
        if session.is_lecture and session.has_media:
            lectures.append(session)
        else:
            logger.debug(f"Skipped {session.id}: {'Not a lecture' if not session.is_lecture else 'No media found'}")
    return lectures


def get_session_details(session_row):
    session = SimpleNamespace(
        id=session_row.find(
            "td",
            attrs={"class": "views-field views-field-field-session-display-number"},
        ).text.strip(),
        title=session_row.find("td", attrs={"class": "views-field views-field-field-session-display-title"}).find("a").text,
        url=f"{urls.COURSES_BASE_URL}{session_row.find('td', attrs={'class': 'views-field views-field-field-session-display-title'}).find('a').get('href')}",
    )
    session_page = get_page(session.url)
    session.description = get_description(session_page, page_type="session")
    session.short_name = re.sub(r"\W+", "", session.id.lower())  # e.g. lecture01
    # Exams and some guest lectures don't have mp3s.
    session.is_lecture = "Lecture" in session.id
    if session.is_lecture:
        session.has_media = has_media(session_page)
    return session


def has_media(session_page):
    session_table = session_page.find("table", attrs={"class": "views-table cols-4"})
    table_body = session_table.find("tbody")
    session_media = table_body.find_all("tr")[0]

    try:
        session_media.find("td", attrs={"class": "views-field views-field-field-audio--file"}).find("a").get("href")
        return True
    except:
        return False


def get_description(page, page_type):
    try:
        if page_type == "course":
            about = page.find("div", attrs={"class": "views-field views-field-body"})
            overview_text = about.find("div", attrs={"class": "field-content"}).p.text
        elif page_type == "session":
            about = page.find("div", attrs={"typeof": "sioc:Item foaf:Document"})
            overview_text = about.p.text
        return overview_text
    except:
        return "No description found"


def add_archive_mp3_info(courses):
    archive_xml_response = requests.get(urls.ARCHIVE_XML_URL)
    root = ET.fromstring(archive_xml_response.content)
    for course in courses:
        archive_course_title = COURSE_NUMBER_TO_ARCHIVE_MEDIA_TITLE[f"{course.number} {course.semester}"]
        lecture_item = root.find(f'channel/item/[title="{archive_course_title}"]')
        logger.debug(f"Archive item found for {course.number}")

        media_details_url = lecture_item.find("enclosure").get("url")
        # https://archive.org/download/oyc_Introduction_to_Ancient_Greek_History/oyc_Introduction_to_Ancient_Greek_History_files.xml
        course_folder_name = media_details_url.split("/")[-2]
        media_folder_url = f"https://archive.org/download/{course_folder_name}"
        media_xml_url = f"{media_folder_url}/{course_folder_name}_files.xml"
        media_xml_response = requests.get(media_xml_url)
        media_root = ET.fromstring(media_xml_response.content)

        for index, lecture in enumerate(course.lectures):
            lecture_number = index + 1
            if archive_course_title in EXTRA_LECTURES and lecture_number in EXTRA_LECTURES[archive_course_title]:
                lecture.mp3_url = EXTRA_LECTURES[archive_course_title][lecture_number]["mp3_url"]
                lecture.mp3_size = EXTRA_LECTURES[archive_course_title][lecture_number]["mp3_size"]
                lecture.mp3_duration = parse_duration(EXTRA_LECTURES[archive_course_title][lecture_number]["mp3_duration"])
            else:
                # Only the original has a track number, but sometimes originals aren't MP3s
                # When this happens, the track number (not the title) tends to be 0-padded to 2 digits
                mp3_media_item = media_root.find(f"file/[format='VBR MP3'][track='{lecture_number}']")
                if not mp3_media_item:
                    original_item = media_root.find(f"file[@source='original'][track='{lecture_number:02d}']")
                    file_name_stem = pathlib.Path(original_item.get("name")).stem
                    mp3_file_name = file_name_stem + ".mp3"
                    mp3_media_item = media_root.find(f"file[@name='{mp3_file_name}']")

                logger.debug(f"Lecture #{lecture_number}")
                lecture.mp3_url = f"{media_folder_url}/{mp3_media_item.get('name')}"
                lecture.mp3_size = int(mp3_media_item.find("size").text)
                lecture.mp3_duration = parse_duration(mp3_media_item.find("length").text)


def parse_duration(duration: str) -> timedelta:
    if duration.count(":") == 2:
        # HH:MM:SS
        return dt.strptime(duration, "%H:%M:%S") - dt.strptime("00:00:00", "%H:%M:%S")
    elif duration.count(":") == 1:
        # MM:SS
        return dt.strptime(duration, "%M:%S") - dt.strptime("00:00", "%M:%S")
    else:
        # seconds.ms
        return timedelta(seconds=float(duration))


# I tried but this takes the least time
COURSE_NUMBER_TO_ARCHIVE_MEDIA_TITLE = {
    "AFAM 162 Spring 2010": "Open Yale Courses: African American History",
    "AMST 246 Fall 2011": "Open Yale Courses: Hemingway, Fitzgerald, Faulkner",
    "ASTR 160 Spring 2007": "Open Yale Courses: Frontiers and Controversies in Astrophysics",
    "BENG 100 Spring 2008": "Open Yale Courses: Frontiers of Biomedical Engineering",
    "CHEM 125a Fall 2008": "Open Yale Courses: Freshman Organic Chemistry I",
    "CHEM 125b Spring 2011": "Open Yale Courses: Freshman Organic Chemistry II",
    "CLCV 205 Fall 2007": "Open Yale Courses: Introduction to Ancient Greek History",
    "ECON 159 Fall 2007": "Open Yale Courses: Game Theory",
    "ECON 251 Fall 2009": "Open Yale Courses: Financial Theory",
    "ECON 252 Spring 2008": "Open Yale Courses: Financial Markets (2008)",
    "ECON 252 Spring 2011": "Open Yale Courses: Financial Markets (2011)",
    "EEB 122 Spring 2009": "Open Yale Courses: Principles of Evolution, Ecology and Behavior",
    "ENGL 220 Fall 2007": "Open Yale Courses: Milton",
    "ENGL 291 Spring 2008": "Open Yale Courses: The American Novel Since 1945",
    "ENGL 300 Spring 2009": "Open Yale Courses: Introduction to Theory of Literature",
    "ENGL 310 Spring 2007": "Open Yale Courses: Modern Poetry",
    "EVST 255 Spring 2010": "Open Yale Courses: Environmental Politics and Law",
    "GG 140 Fall 2011": "Open Yale Courses: The Atmosphere, the Ocean, and Environmental Change",
    "HIST 116 Spring 2010": "Open Yale Course - HIST 116: The American Revolution",  # This one's different!
    "HIST 119 Spring 2008": "Open Yale Courses: The Civil War and Reconstruction Era",
    "HIST 202 Fall 2008": "Open Yale Courses: European Civilization, 1648-1945",
    "HIST 210 Fall 2011": "Open Yale Courses: The Early Middle Ages",
    "HIST 234 Spring 2010": "Open Yale Courses: Epidemics in Western Society Since 1600",
    "HIST 251 Fall 2009": "Open Yale Courses: Early Modern England",
    "HIST 276 Fall 2007": "Open Yale Courses: France Since 1871",
    "HSAR 252 Spring 2009": "Open Yale Courses: Roman Architecture",
    "ITAL 310 Fall 2008": "Open Yale Courses: Dante in Translation",
    "MCDB 150 Spring 2009": "Open Yale Courses: Global Problems of Population Growth",
    "PHIL 176 Spring 2007": "Open Yale Courses: Death",
    "PHIL 181 Spring 2011": "Open Yale Courses: Philosophy and the Science of Human Nature",
    "PHYS 200 Fall 2006": "Open Yale Courses: Fundamentals of Physics I",
    "PHYS 201 Spring 2010": "Open Yale Courses: Fundamentals of Physics II",
    "PLSC 114 Fall 2006": "Open Yale Courses: Introduction to Political Philosophy",
    "PLSC 270 Fall 2009": "Open Yale Courses: Capitalism Success, Crisis and Reform",
    "PSYC 110 Spring 2007": "Open Yale Courses: Introduction to Psychology",
    "PSYC 123 Fall 2008": "Open Yale Courses: The Psychology, Biology and Politics of Food",
    "RLST 145 Fall 2006": "Open Yale Courses: Introduction to the Old Testament (Hebrew Bible)",
    "RLST 152 Spring 2009": "Open Yale Courses: Introduction to the New Testament History and Literature",
    "SOCY 151 Fall 2009": "Open Yale Courses: Foundations of Modern Social Theory",
    "SPAN 300 Fall 2009": "Open Yale Courses: Cervantes' Don Quixote",
}

EXTRA_LECTURES = {
    "Open Yale Courses: Fundamentals of Physics II": {
        24: {
            "mp3_url": "http://openmedia.yale.edu/cgi-bin/open_yale/media_downloader.cgi?file=/courses/spring10/phys201/mp3/phys201_24_041910.mp3",
            "mp3_duration": "01:14:19",
            "mp3_size": 71344340,
        },
        25: {
            "mp3_url": "http://openmedia.yale.edu/cgi-bin/open_yale/media_downloader.cgi?file=/courses/spring10/phys201/mp3/phys201_25_042110.mp3",
            "mp3_duration": "53:43",
            "mp3_size": 51571108,
        },
    }
}
