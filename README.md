# Open Yale Courses RSS

![Cover image](./cover.jpg)


## Overview

### Check it out

List of courses with subscription URLs: [https://tywe-oyc.web.app/](https://tywe-oyc.web.app/)

### More info

This code generates RSS feed files for podcast subscriptions to [Open Yale Courses](https://oyc.yale.edu/), with media hosted by [The Internet Archive](https://archive.org/details/openyalecourses?tab=about).

It also creates files for the [website linked above](https://tywe-oyc.web.app/) so you can subscribe to a podcast of your choice. You'll have to use the feed URL - I'm not registering these feeds with podcast services, because I don't want to.

## I want to subscribe to these podcasts

[Go here](https://tywe-oyc.web.app/) then copy the feed URL for the course you want and paste it into your favorite podcast player. Or your least favorite. It might break it, who knows

## I want to know more about how this works

I'm your huckleberry.

### Setup

Built with Python 3.9.6 using MacOS 11.4 and Windows 10.

_This example setup uses pipenv to install dependencies and pin the Python version._

```bash
# Optional - set up pipenv
pip install pipenv

# Install dependencies
pipenv install

# Enter virtual environment
pipenv shell
```

### Run

```bash
#
# Optional first step: Edit FEED_BASE_URL in urls.py to point to the base URL where these feed files will be hosted
#

# Generate a feed file per course and relevant images
cd src
python ./generate_feeds.py
```

This will generate the following files in the `public/` directory:

* A subdirectory named after the course number, which contains the following:
    * An RSS feed file named `feed.rss`
    * Images for the podcast cover and each lecture in the `img/` subdirectory
* index.html which contains a basic table of courses and their feed links

Media files are _not_ hosted as part of this project, and links in the feed point to the media download link hosted by archive.org instead of the OYC media source. Apologies to anyone managing OYC's media while I was testing this.

The Internet Archive's media is titled slightly differently than OYC. Some guest lectures don't have media, but OYC still counts it as a lecture (and calls it, say, lecture #20) while the Archive skips it altogether and numbers the next media available as the next lecture. Not sure if this messes with the episode title or anything yet! The lecture title will be the same, just a different number.

The webpage scraping and generation process takes 5-10 minutes.

#### Saved course details

To avoid the need to scrape every time a run happens (in case of a simple redeployment or testing or troubleshooting) a `courses.pickle` file is included that stores the `courses` object collected by the scraper. To regenerate feed files without re-scraping the site, set the `USE_COURSE_PICKLE` environment variable.

You can also skip image generation (if you've already got all the images you need) by setting the `SKIP_IMAGE_GENERATION` environment variable.

A YAML file is also generated in `src/courses.yml` for human convenience.

##### bash

```bash
USE_COURSE_PICKLE=1 python ./generate_feeds.py 
```

#### powershell

```powershell
$env:USE_COURSE_PICKLE=1
python ./generate_feeds.py
```

## Deployment

Feed content is hosted using Firebase at [https://tywe-oyc.web.app/](https://tywe-oyc.web.app/). The index page has a list of courses and their feed links.

You can deploy the entire contents of `public/` using any kind of hosting. I use Firebase and have included an example deployment setup below.

### Example firebase deployment

```bash
# alternatively, you could use `brew install firebase-cli`
npm install firebase

firebase login

# init must be run within this repo directory
#
# Project setup prompts: Not going to walk through that here. Create a new one if needed.
#
# Hosting setup prompts:
# ? What do you want to use as your public directory? public
# ? Configure as a single-page app (rewrite all urls to /index.html)? No
# ? Set up automatic builds and deploys with GitHub? No
# ? File public/404.html already exists. Overwrite? No
# i  Skipping write of public/404.html
# ? File public/index.html already exists. Overwrite? No
# i  Skipping write of public/index.html
firebase init hosting

#
# (Optional) cd to src/ and run python ./generate_feeds.py if you need to update the files in /public
#

# Deploy files in public/ while in the base directory
firebase deploy
```

## Notes

### Why?

I subscribed to HIST 119 [from this blogspot link](http://oyc-hist119.blogspot.com/) but couldn't find a working podcast feed/index of all courses. Also, this was fun.

### Subscribing

To add any feeds to your podcast player, you'll need to give it the feed URL. I'm not planning to submit these to any podcast networks.

### Issues?

I haven't tested this with many podcast clients and I haven't tested every feed yet, so some things might be broken or wonky. Feel free to submit a MR/PR or issue.

### Archive

Open Yale Courses are [archived on The Internet Archive](https://archive.org/details/openyalecourses). There are 2 courses there that have been removed from OYC and moved to Coursera. Since they aren't on the OYC website, I'm not including them in this feed.

### Not interested in OYC podcasts?

Well, there's still a fun little image generator over at `generate_images.py`. For a quick buzz, change the text input under the main section and enjoy your grey-on-blue image.

## License

Code copyright 2021 Tyler Wengerd under a [Creative Commons Attribution-Noncommercial-Share Alike 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/).

All media and course information created and owned by Yale University as part of [Open Yale Courses](https://oyc.yale.edu/) and The Internet Archive.

Most of the lectures and course material within Open Yale Courses are licensed under a Creative Commons Attribution-Noncommercial-Share Alike 3.0 license. Unless explicitly set forth in the applicable Credits section of a lecture, third-party content is not covered under the Creative Commons license. Please consult the [Open Yale Courses Terms of Use](https://oyc.yale.edu/terms) for limitations and further explanations on the application of the Creative Commons license.
