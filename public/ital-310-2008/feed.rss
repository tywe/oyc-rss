<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>ITAL 310: Dante in Translation</title>
    <link>https://oyc.yale.edu/italian-language-and-literature/ital-310</link>
    <description>From Open Yale Courses. ITAL 310, Fall 2008. The course is an introduction to Dante and his cultural milieu through a critical reading of the Divine Comedy and selected minor works (Vita nuova, Convivio, De vulgari eloquentia, Epistle to Cangrande). An analysis of Dante’s autobiography, the Vita nuova, establishes the poetic and political circumstances of the Comedy’s composition. Readings of Inferno, Purgatory and Paradise seek to situate Dante’s work within the intellectual and social context of the late Middle Ages, with special attention paid to political, philosophical and theological concerns. Topics in the Divine Comedy explored over the course of the semester include the relationship between ethics and aesthetics; love and knowledge; and exile and history.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:14 +0000</lastBuildDate>
    <itunes:author>Giuseppe Mazzotta and Yale University</itunes:author>
    <dc:creator>Giuseppe Mazzotta</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/ital-310-2008/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Introduction</title>
      <description><![CDATA[Professor Mazzotta introduces students to the general scheme and scope of the Divine Comedy and to the life of its author. Various genres to which the poem belongs (romance, epic, vision) are indicated, and special attention is given to its place within the encyclopedic tradition. The poem is then situated historically through an overview of Dante’s early poetic and political careers and the circumstances that led to his exile. Professor Mazzotta concludes by discussing the central role Dante’s exile was to play in his poetic project.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/01%20-%20Introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/01%20-%20Introduction.mp3" length="18005769" type="audio/mpeg"/>
      <itunes:duration>18:45</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Vita Nuova</title>
      <description><![CDATA[This lecture is devoted to the Vita nuova, Dante’s autobiographical account of his “double apprenticeship” in poetry and love. The poet’s love for Beatrice is explored as the catalyst for his search for a new poetic voice. Medieval theories of love and the diverse poetics they inspired are discussed in contrast. The novelty of the poet’s final resolution is tied to the relationship he discovers between love and knowledge. This relationship is then placed in its larger cultural context to highlight the Vita nuova’s anticipation of the Divine Comedy.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/02%20-%20Vita%20Nuova.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/02%20-%20Vita%20Nuova.mp3" length="66409624" type="audio/mpeg"/>
      <itunes:duration>01:09:10</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Inferno I, II, III, IV</title>
      <description><![CDATA[Professor Mazzotta introduces students to the Divine Comedy, focusing on the first four cantos of Inferno. Stylistic, thematic and formal features of the poem are discussed in the context of its original title, Comedy. The first canto is read to establish the double voice of the poet-pilgrim and to contrast the immanent journey with those described by Dante’s literary precursors. Among these is the pilgrim’s guide, Virgil. The following cantos are read with special attention to the ways in which Dante positions his poem vis-à-vis the classical tradition. The novelty of Dante’s otherworldly journey is here addressed in terms of the relationship, introduced in the previous lecture in the context of the Vita nuova, between love and knowledge or, more precisely, between their respective faculties, will and intellect.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/03%20-%20Inferno%201%2C%202%2C%203%2C%204.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/03%20-%20Inferno%201%2C%202%2C%203%2C%204.mp3" length="72105154" type="audio/mpeg"/>
      <itunes:duration>01:15:06</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Inferno V, VI, VII</title>
      <description><![CDATA[This lecture examines Inferno IV -VII. Dante’s Limbo, modeled on the classical locus amoenus, is identified as a place of repose and vulnerability. Here, in fact, among the poets of antiquity, the pilgrim falls prey to poetic hubris by joining in their ranks. The pilgrim is faced with the consequences of his poetic vocation when he descends to the circle of lust (Inferno V), where Francesca da Rimini, in her failure to distinguish romance from reality, testifies to the dangers inherent to the act of reading. From the destructive power of lust within the private world of the court, Dante moves on to the effects of its sister sin, gluttony, on the public sphere of the city. The relationship posited in Inferno VI between Ciacco and his native Florence is read as a critique of the “body politic.” In conclusion, Virgil’s discourse on Fortune in the circle of avarice and prodigality (Inferno VII) is situated within the Christian world of divine providence.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/04%20-%20Inferno%205%2C%206%2C%207.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/04%20-%20Inferno%205%2C%206%2C%207.mp3" length="68221895" type="audio/mpeg"/>
      <itunes:duration>01:11:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Inferno IX, X, XI</title>
      <description><![CDATA[In this lecture, Professor Mazzotta discusses Inferno IX-XI. An impasse at the entrance to the City of Dis marks Virgil’s first failure in his role as guide (Inferno IX). The invocation of Medusa by the harpies that descend while they wait for divine aid elicits Dante’s first address to the reader. The question of literary mediation, posed in the previous lecture in the context of Inferno V, is explored further, and the distinction Dante draws between the “allegory of poets” and the “allegory of theologians” is introduced. Inferno X is read with a view to the uniqueness of the sin it deals with - heresy. The philosophical errors of the shades encountered here, Farinata and Cavalcante, are tied to the political turmoil they prophecy for Florence. From the disorder of the earthly city, Dante moves on to the order on its infernal counterpart, mapped by Virgil in Inferno XI. The moral system of Dante’s Hell is then discussed with a view to its classical antecedents.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/05%20-%20Inferno%209%2C%2010%2C%2011.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/05%20-%20Inferno%209%2C%2010%2C%2011.mp3" length="74606639" type="audio/mpeg"/>
      <itunes:duration>01:17:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Inferno XII, XIII, XV, XVI</title>
      <description><![CDATA[This lecture focuses on the middle zone of Inferno, the area of violence (Inferno XII-XVI). Introductory remarks are made on the concentration of hybrid creatures in this area of Hell and followed by a close reading of cantos XIII and XV. The pilgrim’s encounter with Pier delle Vigne (Inferno XIII) is placed in literary context (Aeneid III). The questioning of authority staged in this scene resurfaces in the circle of sodomy (Inferno XV), where the pilgrim’s encounter with his teacher, Brunetto Latini, is read as a critique of the humanistic values he embodied.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/06%20-%20Inferno%2012%2C%2013%2C%2015%2C%2016.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/06%20-%20Inferno%2012%2C%2013%2C%2015%2C%2016.mp3" length="71743619" type="audio/mpeg"/>
      <itunes:duration>01:14:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Inferno XIX, XXI, XXV, XXVI</title>
      <description><![CDATA[This lecture deals primarily with Cantos XIX and XXVI of Inferno. Simony, the sin punished in Inferno XIX, is situated historically to point out the contiguity of the sacred and the profane and its relevance to the prophetic voice Dante established in this canto. The fine line between prophecy and profanation is shown to resurface in Inferno XXIV and XXV, where the poet falls prey, as did the pilgrim in Inferno IV, to poetic hubris. Once again, the dangers of Dante’s poetic vocation are dramatized in the canto that immediately follows. In Inferno XXVI, Dante’s tragic revision of the journey of Ulysses is shown to offset his own poetic enterprise, while acknowledging its risks.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/07%20-%20Inferno%2019%2C%2021%2C%2025%2C%2026.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/07%20-%20Inferno%2019%2C%2021%2C%2025%2C%2026.mp3" length="68405797" type="audio/mpeg"/>
      <itunes:duration>01:11:15</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Inferno XXVI, XXVII, XXVIII</title>
      <description><![CDATA[Professor Mazzotta begins this lecture by recapitulating the ambivalent nature of Ulysses’ sin and its relevance to Dante’s poetic project. Inferno XXVII is then read in conjunction with the preceding canto. The antithetical relationship between Dante’s false counselors, Ulysses and Guido da Montefeltro, anchors an overarching discussion of the relationship between rhetoric and politics. The latter half of the lecture is devoted to Inferno XXVIII, where Dante’s preeminent sower of discord, Bertran de Born, introduces the principle of the contrapasso. The law of retribution that governs Dante’s Inferno is discussed in light of classical and contemporary theories of justice/crime and punishment. In conclusion, the opening of Inferno XXIX is read as a retrospective gloss on the limitations of retributive justice.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/08%20-%20Inferno%2026%2C%2027%2C%2028.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/08%20-%20Inferno%2026%2C%2027%2C%2028.mp3" length="81491677" type="audio/mpeg"/>
      <itunes:duration>01:07:54</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Inferno XXX, XXXI, XXXII, XXXIII, XXXIV</title>
      <description><![CDATA[The final cantos of Inferno are read with a view to the role of the tragic within Dante’s Comedy. Using Dante’s discussion of tragedy in the De vulgari eloquentia as a point of departure, Professor Mazzotta traces the disintegration of language that accompanies the pilgrim’s descent into the pit of Hell, the zone of treachery, from the distorted speech of Nimrod inInferno XXXI to the silence of Satan in Inferno XXXIV. The ultimate triumph of comedy over tragedy is dramatized by the pilgrim’s ascent, by means of Lucifer, onto the shores of Mount Purgatory.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/09%20-%20Inferno%2030%2C%2031%2C%2032%2C%2033.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/09%20-%20Inferno%2030%2C%2031%2C%2032%2C%2033.mp3" length="73745226" type="audio/mpeg"/>
      <itunes:duration>01:16:49</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Purgatory I, II</title>
      <description><![CDATA[In this lecture, Professor Mazzotta introduces Purgatory and proceeds with a close reading of Cantos I and II. The topography of Mount Purgatory is described, and the moral system it structures is contrasted with that of Hell. Dante’s paradoxical choice of Cato, a pagan suicide, as guardian to the entrance of Purgatory ushers in a discussion of freedom from the standpoint of classical antiquity, on the one hand, and Judaism, on the other. In his refusal to be enslaved by the past, both on earth and in the afterlife, Cato is seen to embody the virtues of exile, setting an example for the penitent souls of Ante-purgatory (Purgatory II), including the pilgrim, who still clings to the comforts of the past.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/10%20-%20%20Inferno%2034%3B%20Purgatorio%201%2C%202.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/10%20-%20%20Inferno%2034%3B%20Purgatorio%201%2C%202.mp3" length="68135795" type="audio/mpeg"/>
      <itunes:duration>01:10:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Purgatory V, VI, IX, X</title>
      <description><![CDATA[This lecture covers Purgatory V, VI, IX and X. The purgatorial theme of freedom introduced in the previous lecture is revisited in the context of Canto V, where Buonconte da Montefeltro’s appearance among the last minute penitents is read as a critique of the genealogical bonds of natural necessity. The poet passes from natural to civic ancestry inPurgatory VI, where the mutual affection of Virgil and Sordello, a former citizen of the classical poet’s native Mantua, sparks an invective against the mutual enmity that enslaves contemporary Italy. The transition from ante-Purgatory to Purgatory proper in Canto IX leads to an elaboration on the moral and poetic structure of Purgatory, exemplified on the terrace of pride in Canto X.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/11%20-%20Purgatorio%205%2C%206%2C%2010%2C%2011.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/11%20-%20Purgatorio%205%2C%206%2C%2010%2C%2011.mp3" length="74004778" type="audio/mpeg"/>
      <itunes:duration>01:17:05</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Purgatory X, XI, XII, XVI, XVII</title>
      <description><![CDATA[In this lecture, Professor Mazzotta moves from the terrace of pride (Purgatory X-XII) to the terrace of wrath (PurgatoryXVI-XVII). The relationship between art and pride, introduced in the previous lecture in the context of Canto X, is pursued along theological lines in the cantos immediately following. The “ludic theology” Dante embraces in these cantos resurfaces on the terrace of wrath, where Marco Lombardo’s speech on the traditional problem of divine foreknowledge and human freedom highlights the playfulness of God’s creation. The motifs of human and divine creation explored thus far are shown to converge at the numerical center of the poem (Purgatory XVII) in Dante’s apostrophe to the imagination.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/12%20-%20Purgatorio%2012%2C%2015%2C%2016%2C%2017.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/12%20-%20Purgatorio%2012%2C%2015%2C%2016%2C%2017.mp3" length="68817905" type="audio/mpeg"/>
      <itunes:duration>01:11:41</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Purgatory XIX, XXI, XXII</title>
      <description><![CDATA[This lecture deals primarily with Purgatory XIX, XXI and XXII. The ambiguity of the imagination discussed in the preceding lecture as the selfsame path to intellectual discovery and disengagement is explored in expressly poetic terms. While the pilgrim’s dream of the siren in Purgatory XIX warns of the death-dealing power of aesthetics, the encounter between Statius and Virgil in the cantos that follow points to its life-giving potential by casting poetry as a means of conversion.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/13%20-%20Purgatorio%2018%2C%2019%2C%2021%2C%2022.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/13%20-%20Purgatorio%2018%2C%2019%2C%2021%2C%2022.mp3" length="75904821" type="audio/mpeg"/>
      <itunes:duration>01:19:04</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Purgatory XXIV, XXV, XXVI</title>
      <description><![CDATA[Guest lecturer Professor David Lummus discusses Purgatory XXIV-XXVI. On the terraces of gluttony and lust, the pilgrim’s encounters with masters of the Italian love lyric give rise to the Comedy’s most sustained treatment of poetics. Through Dante’s older contemporary Bonagiunta (Purgatory XXIV), the pilgrim distinguishes the poetic style of his youth from that of the courtly love tradition pursued by his interlocutor. In Purgatory XXVI, Dante reinforces his own poetic genealogy through his encounter with Guido Guinizelli, founder of the Sweet New Style of poetry he crafted in his youth. The interpretative key to the language of paternity and filiation that pervades these cantos is found in Purgatory XXV, where Statius’ embryological exposition of the divine creation of the soul conveys the divinity of poetic inspiration.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/14%20-%20Purgatorio%2024%2C%2026%2C%2027%2C%2028.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/14%20-%20Purgatorio%2024%2C%2026%2C%2027%2C%2028.mp3" length="51204686" type="audio/mpeg"/>
      <itunes:duration>53:20</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Purgatory XXX, XXXI, XXXIII</title>
      <description><![CDATA[This lecture deals with Dante’s representation of the Earthly Paradise at the summit of Mount Purgatory. The quest for freedom begun under the aegis of Cato in Purgatory I reaches its denouement at the threshold of Eden, where Virgil proclaims the freedom of the pilgrim’s will (Purgatory XXVII). Left with pleasure as his guide, the pilgrim nevertheless falls short of a second Adam in his encounter with Matelda. His lingering susceptibility to earthly delights is underscored at the arrival of Beatrice (Purgatory XXX) whose harsh treatment of the pilgrim is read as a retrospective gloss on the dream of the Siren in Purgatory XIX. By dramatizing his character’s failings within the Earthly Paradise, Dante replaces the paradigm of conversion as a once-for-all event with that of an ongoing process to be continued in Paradise under the guidance of Beatrice.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/15%20-%20Purgatorio%2030%2C%2031%2C%2033.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/15%20-%20Purgatorio%2030%2C%2031%2C%2033.mp3" length="65933986" type="audio/mpeg"/>
      <itunes:duration>01:08:40</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Paradise I, II</title>
      <description><![CDATA[Professor Mazzotta introduces students to Paradise. The Ptolemaic structure of Dante’s cosmos is described along with the arts and sciences associated with its spheres. Beatrice’s role as teacher in Dante’s cosmological journey is distinguished from that of her successor, St. Bernard of Clairvaux. An introduction to Dante’s third and final guide to the Beatific Vision helps situate the poetics of Paradise vis-à-vis the mystical tradition. Professor Mazzotta’s introduction to the canticle is followed by a close reading of the first canto. The end of the pilgrim’s journey is discussed in light of the two theological modes Dante pulls together in the exordium of Paradise I. The poetic journey staged in the opening tercets is then explored in light of the mythological and Christian figures (Marsyas, St. Paul) Dante claims as his poetic precursors.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/16%20-%20%20Paradiso%201%2C%202.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/16%20-%20%20Paradiso%201%2C%202.mp3" length="72154055" type="audio/mpeg"/>
      <itunes:duration>01:15:09</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: Paradise IV, VI, X</title>
      <description><![CDATA[This lecture deals with Paradise IV, VI and X. At the beginning of Paradise IV, the pilgrim raises two questions to which the remainder of the canto is devoted. The first concerns Piccarda (Paradise III) who was constrained to break her religious vows. The second concerns the arrangement of the souls within the stars. The common thread that emerges from Beatrice’s reply is the relationship between intellect and will. Just as Piccarda’s fate reveals the limitations of the will, the representation of the souls in Paradise, a condescension to the pilgrim’s human faculty, as Beatrice explains, reveal the limitations of the intellect. By dramatizing the limitations of both faculties, Dante underscores their interdependence. In Paradise VI, Dante turns his attention to politics. Through the emperor Justinian’s account of Roman history, Dante places the antithetical views of Virgil and Augustine in conversation. Key to understanding Dante’s position between these two extremes is the vituperation of contemporary civil strife that follows Justinian’s encomium of the Empire. In Paradise X, the pilgrim enters the Heaven of the Sun, where St. Thomas and St. Bonaventure introduce him to two rings of spirits celebrated for their wisdom. The unlikely presence of Solomon and Siger of Brabant among the first of these concentric rings is discussed as a poetic reflection on the boundaries between knowledge and revelation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/17%20-%20Paradiso%204%2C%206%2C%2010.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/17%20-%20Paradiso%204%2C%206%2C%2010.mp3" length="72782248" type="audio/mpeg"/>
      <itunes:duration>01:15:48</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Paradise XI, XII</title>
      <description><![CDATA[Professor Mazzotta continues his discussion of the Heaven of the Sun (Paradise X-IV), where the earthly disputes between the Franciscan and Dominican orders give way to mutual praise. The tribute St. Thomas pays to the founder of the Franciscan order (Paradise XI) is repaid by St. Bonaventure through his homage to St. Dominic (Paradise XII). The chiasmic structure of these cantos is reinforced by the presence of Nathan and Joachim of Flora, the counterweights to Solomon and Siger, among the second ring of sages. Special attention is then paid to the lives St. Francis and St. Dominic presented in Paradise XI and XII, where the former’s marriage to Lady Poverty finds its poetic counterpart in the latter’s marriage to theology. The critique of the world and its values shared by these religious founders is explored in light of the “ludic theology” that pervades these cantos.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/18%20-%20Paradiso%2011%2C%2012.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/18%20-%20Paradiso%2011%2C%2012.mp3" length="70260700" type="audio/mpeg"/>
      <itunes:duration>01:13:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Paradise XV, XVI, XVII</title>
      <description><![CDATA[This lecture focuses on the cantos of Cacciaguida (Paradise XV-XVII). The pilgrim’s encounter with his great-great grandfather brings to the fore the relationship between history, self and exile. Through his ancestor’s mythology of their native Florence, Dante is shown to move from one historiographic mode to another, from the grandeur of epic to the localism of medieval chronicles. Underlying both is the understanding of history in terms of genealogy reinforced and reproved by Dante’s mythic references to fathers and sons, from Aeneas and Anchises to Phaeton and Apollo to Hippolytus and Theseus. The classical and medieval idea of the self’s relation to history in terms of the spatial continuity these genealogies provide is unsettled by Cacciaguida’s prophecy of Dante’s exile. The very premise of the poem’s composition, exile is redeemed as an alternative means of reentering the world of history.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/19%20-%20Paradiso%2015%2C%2016%2C%2017.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/19%20-%20Paradiso%2015%2C%2016%2C%2017.mp3" length="68305487" type="audio/mpeg"/>
      <itunes:duration>01:11:09</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Paradise XVIII, XIX, XXI, XXI</title>
      <description><![CDATA[In this lecture, Professor Mazzotta examines Paradise XVIII-XIX and XXI-XXII. In Paradise XVIII, Dante enters the Heaven of Jupiter, where the souls of righteous rulers assume the form of an eagle, the emblem of the Roman Empire. The Eagle’s outcry against the wickedness of Christian kings leads Dante to probe the boundaries of divine justice by looking beyond the confines of Christian Europe. By contrasting the political with the moral boundaries that distinguish one culture from another, Dante opens up the Christian economy of redemption to medieval notions of alterity. InParadise XXI, Dante moves from the exemplars of the active life to the contemplative spirits of the Heaven of Saturn, Peter Damian and St. Benedict. The question of perspective through which the theme of justice was explored resurfaces to distinguish between the visionary claims of the contemplative and mystical traditions. As Dante ascends to the Heaven of the Fixed Stars, catching sight of the earth below (Paradise XXII), his own visionary claims are distinguished by an awareness of his place in history.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/20%20-%20Paradiso%2019%2C%2021%2C%2022.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/20%20-%20Paradiso%2019%2C%2021%2C%2022.mp3" length="72627603" type="audio/mpeg"/>
      <itunes:duration>01:15:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Paradise XXIV, XXV, XXVI</title>
      <description><![CDATA[This lecture covers Paradise XXIV-XXVI. In the Heaven of the Fixed Stars, Dante is examined on the three theological virtues by the apostles associated with each: St. Peter with faith (Paradise XXIV), St. James with hope (Paradise XXV), and St. John with love (Paradise XXVI). While mastering these virtues is irrelevant to the elect, it is crucial to the message of reform the pilgrim-turned-poet will relay on his return home. Dante’s scholastic profession of faith before St. Peter (Paradise XXIV) is read testament to the complication of faith and reason. The second of the theological virtues is discussed in light of the classical disparagement of hope as a form of self-deception and its redemption by the biblical tradition through the story of Exodus, the archetype of Dante’s journey. The pilgrim’s three-part examination continues inParadise XXVI under the auspices of St. John, where love, the greatest of the virtues is distinguished by its elusiveness. The emphasis on love’s resistance to formal definition sets the stage for the pilgrim’s encounter with Adam, who sheds light on the linguistic consequences of the Fall.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/21%20-%20Paradiso%2024%2C%2025%2C%2026.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/21%20-%20Paradiso%2024%2C%2025%2C%2026.mp3" length="73208984" type="audio/mpeg"/>
      <itunes:duration>01:16:15</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Paradise XXVII, XXVIII, XXIX</title>
      <description><![CDATA[This lecture focuses on Paradise XXVII-XXIX. St. Peter’s invective against the papacy from the Heaven of the Fixed Stars is juxtaposed with Dante’s portrayal of its contemporary incumbent, Boniface VIII, in the corresponding canto of Inferno. Recalls of infernal characters proliferate as the pilgrim ascends with Beatrice into the primum mobile. Bid to look back on the world below, Dante perceives the mad track of his uneasy archetype, Ulysses. Dante’s remembrance of this tragic shipwreck at the very boundary of time and space gains interest in light of his allusion to Francesca at the outset ofParadise XXIX. These resonances of intellectual and erotic transgression reinforce the convergence of cosmology and creation Dante assigns to the heaven of metaphysics]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/22%20-%20Paradiso%2027%2C%2029%2C%2030.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/22%20-%20Paradiso%2027%2C%2029%2C%2030.mp3" length="71211975" type="audio/mpeg"/>
      <itunes:duration>01:14:10</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Paradise XXX, XXXI, XXXII, XXXIII</title>
      <description><![CDATA[Professor Mazzotta lectures on the final cantos of Paradise (XXX-XXXIII). The pilgrim’s journey through the physical world comes to an end with his ascent into the Empyrean, a heaven of pure light beyond time and space. Beatrice welcomes Dante into the Heavenly Jerusalem, where the elect are assembled in a celestial rose. By describing the Empyrean as both a garden and a city, Dante recalls the poles of his own pilgrimage while dissolving the classical divide between urbs and rus, between civic life and pastoral retreat. Beatrice’s invective against the enemies of empire from the spiritual realm of the celestial rose attests to the strength of Dante’s political vision throughout his journey into God. Dante’s concern with the harmony of oppositions as he approaches the beatific vision is crystallized in the prayer to the Virgin Mary offered by St. Bernard, Dante’s third and final guide. In his account of the vision that follows, the end of Dante’s pilgrimage and the measure of its success converge in the poet’s admission of defeat in describing the face of God.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/23%20-%20Paradiso%2031%2C%2033.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/23%20-%20Paradiso%2031%2C%2033.mp3" length="65330453" type="audio/mpeg"/>
      <itunes:duration>01:08:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: General Review</title>
      <description><![CDATA[The last class of the semester consists of a brief recapitulation of topics in the Divine Comedy addressed throughout the course, followed by an extensive question and answer session with the students. The questions posed allow Professor Mazzotta to elaborate on issues raised over the course of the semester, from Dante’s place within the medieval love tradition to the relationship between his roles as poet and theologian.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Dante_in_Translation/24%20-%20General%20Review.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Dante_in_Translation/24%20-%20General%20Review.mp3" length="70855874" type="audio/mpeg"/>
      <itunes:duration>01:13:48</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/ital-310-2008/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
  </channel>
</rss>
