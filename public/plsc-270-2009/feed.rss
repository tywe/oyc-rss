<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>PLSC 270: Capitalism: Success, Crisis, and Reform</title>
    <link>https://oyc.yale.edu/political-science/plsc-270</link>
    <description>From Open Yale Courses. PLSC 270, Fall 2009. In this course, we will seek to interpret capitalism using ideas from biological evolution: firms pursuing varied strategies and facing extinction when those strategies fail are analogous to organisms struggling for survival in nature. For this reason, it is less concerned with ultimate judgment of capitalism than with the ways it can be shaped to fit our more specific objectives–for the natural environment, public health, alleviation of poverty, and development of human potential in every child. Each book we read will be explicitly or implicitly an argument about good and bad consequences of capitalism.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:17 +0000</lastBuildDate>
    <itunes:author>Douglas W. Rae and Yale University</itunes:author>
    <dc:creator>Douglas W. Rae</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/plsc-270-2009/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Exploding Worlds and Course Introduction</title>
      <description><![CDATA[Professor Rae introduces the concept of capital as accumulated wealth used to produce more wealth. Questions about what constitutes capital are posed and discussed. The biggest story in recent economic history is the substitution of labor intensive production to capital intensive production. This transition, and the various speeds and scales with which it has occurred in different places at different times, has generated large income disparities around the world. Characteristics of capitalism are presented and discussed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/01%20-%20Exploding%20Worlds%20and%20Course%20Introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/01%20-%20Exploding%20Worlds%20and%20Course%20Introduction.mp3" length="44337616" type="audio/mpeg"/>
      <itunes:duration>46:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Thomas Malthus and Inevitable Poverty</title>
      <description><![CDATA[Professor Rae shows how countries over the last two centuries have experienced improved life expectancies and increased incomes per capita. Dynamic graphical representation of this trend reveals how improved life expectancies tend to predate increases in wealth. Malthus’ “iron law of wages” and diminishing returns are explained. Questions about why the industrial revolution occurred in England at the time that it did are then posed. Professor Rae then shows the importance of the “world demographic transition” to economic history and contemporary economics. All countries tend to follow similar demographic patterns over the course of their economic development. Countries tend to have high birth and death rates in Phase I, falling death rate and high birth rate in Phase II, falling birth rate to meet the death rate in Phase III, and low birth and death rates in Phase IV. These demographic patterns are associated with different levels of capital and labor. While all countries follow this demographic transition, they do so at different times, and world trade is a way of “arbitraging” between different stages in the world demographic transition.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/02%20-%20Thomas%20Malthus%20and%20Inevitable%20Poverty.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/02%20-%20Thomas%20Malthus%20and%20Inevitable%20Poverty.mp3" length="47411288" type="audio/mpeg"/>
      <itunes:duration>49:23</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Counting the Fingers of Adam Smith's Invisible Hand</title>
      <description><![CDATA[Professor Rae introduces Adam Smith’s notion of the “invisible hand” of the market. Several preconditions must be met for the invisible hand to work. Markets must be open, and there cannot be just one buyer or one seller who can control product prices. No producer can hold a pivotal private technology, and there must be more or less truthful information across the whole market. Governments must enforce property and contracts. However, many of these preconditions are at odds with the Porter Forces, which represent general rules of thumb, or principles, for a firm trying to make above average profits. These principles include avoiding direct competition, establishing high barriers to entry, and avoiding powerful buyers and powerful suppliers. Professor Rae suggests that submission to Adam Smith’s invisible hand may be contrary to basics of corporate strategy. Corporations can leverage powerful political influence to affect the movements of the “invisible hand.” Guest speaker Jim Alexander, formerly of Enron, discusses problems of very imperfect information, as well as the principal-agent problem. Professor Rae also discusses Adam Smith’s complicated ideas about self-interest and morality.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/03%20-%20%20Counting%20the%20Fingers%20of%20%20Adam%20Smith%27s%20Invisible%20Hand.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/03%20-%20%20Counting%20the%20Fingers%20of%20%20Adam%20Smith%27s%20Invisible%20Hand.mp3" length="43892072" type="audio/mpeg"/>
      <itunes:duration>45:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Karl Marx, Joseph Schumpeter, and an Economic System Incapable of Coming to Rest</title>
      <description><![CDATA[Professor Rae relates Marxist theories of monopoly capitalism to Schumpeter’s theory of creative destruction. Both Marx and Schumpeter agree that capitalism is a system that is “incapable of standing still,” and is always revising (or revolutionizing) itself. Professor Rae critiques Marxist determinism and other features of Marx’s theories. To highlight Schumpeterian creative destruction, Professor Rae uses examples from technological revolutions in energy production since water-powered mills. Marx’s labor theory of value is discussed. Professor Rae highlights aspects overlooked by Marx, including supply and demand for labor, labor quality, and the role of capital in economic growth. Professor Rae also notes problems with Marx’s predictions, including the prediction that the revolution will occur in the most advanced capitalist economies. Professor Rae also discusses Marx’s theory of the universal class, the end of exploitation, and the withering away of the state.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/04%20-%20Karl%20Marx%2C%20Joseph%20Schumpeter%2C%20and%20an%20Economic%20System%20Incapable%20of%20Coming%20to%20Rest.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/04%20-%20Karl%20Marx%2C%20Joseph%20Schumpeter%2C%20and%20an%20Economic%20System%20Incapable%20of%20Coming%20to%20Rest.mp3" length="45209897" type="audio/mpeg"/>
      <itunes:duration>47:05</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Property, Freedom, and the Essential Job of Government</title>
      <description><![CDATA[A practical theory of freedom is discussed, based on Hayek’s Constitution of Liberty. Free societies can be thought of as great learning machines capable of aggregating individuals’ knowledge and accomplishments. Professor Rae uses examples from automotives and university administration to illustrate how freedom allows everybody to profit from others’ knowledge. Professor Rae also highlights Hayek’s story of the rock climber who is stuck at the bottom of the crevasse, and discusses whether refusing to assist another is an implicit act of coercion. Hayek’s theories of freedom are applied to modern cases of extreme poverty in developing countries. Professor Rae also discusses Yale University Press’ decision not to publish controversial cartoons depicting the prophet Mohammed within a recent book. The lecture concludes with de Soto’s notions of live and dead capital, and the importance of property rights in unlocking the productive power of capitalism.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/05%20-%20Property%2C%20Freedom%2C%20and%20the%20Essential%20Job%20of%20Government.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/05%20-%20Property%2C%20Freedom%2C%20and%20the%20Essential%20Job%20of%20Government.mp3" length="45563491" type="audio/mpeg"/>
      <itunes:duration>47:27</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Rise of the Joint Stock Corporation</title>
      <description><![CDATA[Professor Rae explains how the growing scale and complexity of railroads in the US were foundational to the development of modern capitalism. Operating the railroad system required professional managers and new management techniques, and the scale of railroad financing gave rise to the formation of the joint stock corporation. Professor Rae then discusses how different forms of company ownership differ along liability, liquidity, financial scalability, accountability, and role of ownership dimensions. Joint stock corporations are shown to be extremely efficient ways to raise large amounts of money, even if they suffer principal-agent problems.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/06%20-%20Rise%20of%20the%20Joint%20Stock%20Corporation.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/06%20-%20Rise%20of%20the%20Joint%20Stock%20Corporation.mp3" length="42330158" type="audio/mpeg"/>
      <itunes:duration>44:05</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Can You Sell a Scheme for Operating on Beating Hearts and Make a Business of It?</title>
      <description><![CDATA[Dean of the Yale School of Management, Sharon Oster, explains the CardioThoracic business case. Barriers to CardioThoracic’s success are discussed, including competition from other medical firms, “gatekeeper problems,” other medical procedures, and difficulties understanding needs of the firm’s customers. Various players in the case are identified, as well as their specific interests and potential strategies for articulating these interests. Dean Oster analyzes interest misalignments, information asymmetries, and discrepancies in values among the various players in the case.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/07%20-%20%20Can%20You%20Sell%20a%20Scheme%20for%20Operating%20on%20Beating%20Hearts%20and%20Make%20a%20Business%20of%20It_.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/07%20-%20%20Can%20You%20Sell%20a%20Scheme%20for%20Operating%20on%20Beating%20Hearts%20and%20Make%20a%20Business%20of%20It_.mp3" length="44455481" type="audio/mpeg"/>
      <itunes:duration>46:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Mortal Life Cycle of a Great Technology</title>
      <description><![CDATA[Professor Rae uses the case of Polaroid cameras to highlight key features of the capitalist system. Polaroid’s business model, corporate culture, and firm trajectory are discussed. Important firm decisions are analyzed, including product offerings and mergers. Professor Rae explores factors that led to Polaroid’s demise, including the company’s relentless focus on scientific innovation at the expense of market research and product development. Polaroid was unable to keep up with market changes, such as the advent of the one-hour photo processing and the revolution in digital photography.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/08%20-%20%20Mortal%20Life%20Cycle%20of%20a%20Great%20Technology.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/08%20-%20%20Mortal%20Life%20Cycle%20of%20a%20Great%20Technology.mp3" length="47421319" type="audio/mpeg"/>
      <itunes:duration>49:23</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Guest Lecture by Jim Alexander: Managing the Crooked E</title>
      <description><![CDATA[Jim Alexander, former CFO of the Enron subsidiary Enron Global Power and Pipeline, offers an insider’s account of Enron’s corporate culture and operations before the company’s spectacular fall. The leaders of Enron, Mr. Alexander asserts, disregarded concerns over the company’s ethics. Enron strategically found and exploited loopholes in accounting regulations to make their transactions as opaque as possible. Lack of regulation and oversight allowed Enron’s traders to inflate their numbers. Organizations that were in a position to oversee Enron’s operations sometimes faced grossly misaligned incentives that rewarded negligence. Mr. Alexander emphasizes the notion of the “rational economic man” in Enron’s corporate culture, and its predominance over notions of ethical corporate behavior.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/09%20-%20Guest%20Lecture%20by%20Jim%20Alexander_%20Managing%20the%20Crooked%20E%20.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/09%20-%20Guest%20Lecture%20by%20Jim%20Alexander_%20Managing%20the%20Crooked%20E%20.mp3" length="47354028" type="audio/mpeg"/>
      <itunes:duration>49:19</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Guest Lecture by Will Goetzmann: Institutions and Incentives in Mortgages and Mortgage-Backed Securities</title>
      <description><![CDATA[Guest speaker Will Goetzmann, Director of the Yale International Center for Finance and professor at the Yale School of Management, provides a brief history of debt and financial crises. Professor Goetzmann begins with a discussion on debt slavery in the ancient world, and moves on to real estate financing in New York City. Professor Goetzmann also presents recent research by himself and others on the collapse of the real estate market. He explores the notion that the collapse of the mortgage market followed from the fallout of the larger financial crisis, rather than the other way around. Data on the real estate market is presented and discussed. Larger claims about responsibility of different players for the economic crisis are briefly assessed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/10%20-%20Guest%20Lecture%20by%20Will%20Goetzmann_%20Institutions%20and%20Incentives%20in%20Mortgages%20and%20Mortgage-Backed%20Securities.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/10%20-%20Guest%20Lecture%20by%20Will%20Goetzmann_%20Institutions%20and%20Incentives%20in%20Mortgages%20and%20Mortgage-Backed%20Securities.mp3" length="48264761" type="audio/mpeg"/>
      <itunes:duration>50:16</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Accountability and Greed in Investment Banking</title>
      <description><![CDATA[Professor Rae explores the creation of incentives and disincentives for individual action. The discussion begins with the Coase Theorem, which outlines three conditions for efficient transactions: 1) clear entitlements to property, 2) transparency, and 3) low transaction costs. Professor Rae then tells the story of a whaling law case from 1881 to highlight the power of incentives and property rights. The conversation then moves to Hernando de Soto’s portrayal of the development of property rights in the American West, and then shifts to a discussion of New Haven deeds, property values, and valuation of real estate. The lecture concludes with a discussion of Mory’s.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/11%20-%20Accountability%20and%20Greed%20in%20Investment%20Banking.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/11%20-%20Accountability%20and%20Greed%20in%20Investment%20Banking.mp3" length="48820229" type="audio/mpeg"/>
      <itunes:duration>50:51</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 13: The Mortgage Meltdown in Cleveland</title>
      <description><![CDATA[Professor Rae discusses the subprime mortgage crisis. Major actors are presented and analyzed, including homebuyers, brokers, appraisers, lenders, i-banks, and rating and government agencies. Major actors’ incentives and risks are assessed. Professor Rae also presents a brief history of government involvement in mortgage markets. Deregulation of the industry and its consequences are explored, and Professor Rae facilitates a discussion on apportioning blame for the collapse of the U.S. housing market.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/12%20-%20The%20Mortgage%20Meltdown%20in%20Cleveland.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/12%20-%20The%20Mortgage%20Meltdown%20in%20Cleveland.mp3" length="47199383" type="audio/mpeg"/>
      <itunes:duration>49:09</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 14: The Political and Judicial Elements of American Capitalism</title>
      <description><![CDATA[Professor Rae uses the Merck-Vioxx business case to highlight political elements of U.S. capitalism, including government regulatory agencies, federalism, lobbying, regulatory capture, tort law and liability, and patent law. Professor Rae discusses the importance and influence of concentrated business interests in Washington DC. The Merck legal battles underline how important political and judicial details are in the operation of capitalism. The case also shows the constraints that reform-minded politicians face in attempting to change the status quo.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/13%20-%20%20The%20Political%20and%20Judicial%20Elements%20of%20American%20Capitalism.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/13%20-%20%20The%20Political%20and%20Judicial%20Elements%20of%20American%20Capitalism.mp3" length="43817675" type="audio/mpeg"/>
      <itunes:duration>45:38</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Mass Affluence Comes to the Western World</title>
      <description><![CDATA[Professor Rae discusses the rise of mass affluence, the joint stock corporation, and advertising/consumer culture in America. Gregory Clark’s theory of the causes of the Industrial Revolution, including England’s “downward social mobility” in the medieval and early modern periods, are explored. According to this theory, the upper classes produced children in greater numbers than in other countries, and there were fewer jobs of high social status. This led to upper-class children working in “lower-class” jobs, infusing lower economic strata with upper class outlooks toward work. Clark also touches on a genetic, Darwinian explanation for England’s Industrial Revolution. Professor Rae also discusses other causal explanations for the Industrial revolution, including exogenous and endogenous growth theories, institutions, and Schumpeter’s theory of creative destruction. The wealth-generating power of the joint stock corporation is also presented.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/14%20-%20Mass%20Affluence%20Comes%20to%20the%20Western%20World.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/14%20-%20Mass%20Affluence%20Comes%20to%20the%20Western%20World.mp3" length="45611556" type="audio/mpeg"/>
      <itunes:duration>47:30</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Braudel's Bell Jar</title>
      <description><![CDATA[Professor Rae explores Hernando de Soto’s theories of dead and live capital and the power of property rights. According to de Soto, informal property must carefully be integrated into the formal property system. Professor Rae presents the example of Baltimore’s row house vacancy problem, and the difficulties in designing and implementing innovative property policies when existing interests of local stakeholders are firmly entrenched. The Coase theorem and transaction costs are revisited. Professor Rae also facilitates a discussion with students on how a developing country should most productively invest 5% of its gross domestic product (GDP). Complexities of economic development are explored.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/15%20-%20%20Braudel%27s%20Bell%20Jar.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/15%20-%20%20Braudel%27s%20Bell%20Jar.mp3" length="43938465" type="audio/mpeg"/>
      <itunes:duration>45:46</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 17: The Case of Mister Balram Halwai</title>
      <description><![CDATA[Professor Rae discusses Aravind Adiga’s novel The White Tiger. The novel reveals the difficulties developing countries face dismantling entrenched inequalities. Corruption and chronic rent-seeking behavior can be major obstacles. Other aspects of the novel, including India’s religious history, the role of caste structure, and entrepreneurialism, are also explored. Links are made between themes from the novel and previous class discussions on the nature of capitalism.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/16%20-%20The%20Case%20of%20Mister%20Balram%20Halwai%20.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/16%20-%20The%20Case%20of%20Mister%20Balram%20Halwai%20.mp3" length="45978942" type="audio/mpeg"/>
      <itunes:duration>47:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Microfinance in South India</title>
      <description><![CDATA[Professor Rae teaches the SELCO business case, about a distributed electric power generation scheme targeting rural Indians. In presenting the case, Professor Rae discusses several analytical frameworks for thinking through business cases, including SWOT (strengths, weaknesses, opportunities, threats), and the Porter Forces. Issues with SELCO’s business model are discussed, including the scalability of their operations, and capacity to break into higher margin markets. SELCO’s financing structure and partnership with a local microfinance institution are also explored. Students offer suggestions about how to improve SELCO’s business.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/17%20-%20%20Microfinance%20in%20South%20India.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/17%20-%20%20Microfinance%20in%20South%20India.mp3" length="45977270" type="audio/mpeg"/>
      <itunes:duration>47:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Plight of the Bottom Billion</title>
      <description><![CDATA[In a pre-recorded lecture, Professor Rae discusses problems with using gross domestic product (GDP) as a measure for societal well-being. For example, GDP fails to capture wealth inequality and socially undesirable conditions that can increase GDP. He then touches on some of the “traps” presented in Paul Collier’s book, The Bottom Billion, that are keeping the poorest of the developing countries mired in poverty. In the second half of lecture, a video of Paul Collier is shown in which the author urges the developed world to take as a model America’s reconstruction package to post-WWII Europe. According to Collier, the developed world must rethink its aid and trade policies toward the developing world. Collier also discusses the relationship between democracy and the so-called “resource curse,” and how the rich world can create institutions to support reformers in the poorest countries.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/18%20-%20%20Plight%20of%20the%20Bottom%20Billion%20.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/18%20-%20%20Plight%20of%20the%20Bottom%20Billion%20.mp3" length="44399474" type="audio/mpeg"/>
      <itunes:duration>46:14</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Policy Targets for Capitalist Development</title>
      <description><![CDATA[Professor Rae begins by briefly discussing his recent trip to Washington, where he became more closely acquainted with the health care reform bill. Professor Rae uses this example to highlight the intimate connections between capitalist market systems and the government. Then, with the help of two guest speakers, Professor Rae discusses the dramatic downfall and planned revival of one of Yale’s most iconic institutions: Mory’s club. Various methods for increasing the club’s relevance to the contemporary Yale community are discussed, including reforming membership rules.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/19%20-%20Policy%20Targets%20for%20Capitalist%20Development%20.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/19%20-%20Policy%20Targets%20for%20Capitalist%20Development%20.mp3" length="47009212" type="audio/mpeg"/>
      <itunes:duration>48:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Guest Lecture by Paolo Zanonni, Part I</title>
      <description><![CDATA[Guest speaker Paolo Zanonni, partner at Goldman Sachs, discusses the firm’s transition from a straight partnership to a hybrid partnership / joint stock corporation. The impetus for the transition was to obtain the advantages of the joint stock corporation, especially in raising permanent capital, while maintaining the beneficial incentive structure of a partnership. The partnership selection process, which fosters leadership, entrepreneurialism, and conformity to the firm’s corporate culture, has remained virtually unchanged since Goldman’s IPO. Mr. Zanonni describes the corporate culture and values of the firm as seen from the inside.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/20%20-%20Guest%20Lecture%20by%20Paolo%20Zanonni%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/20%20-%20Guest%20Lecture%20by%20Paolo%20Zanonni%2C%20Part%20I.mp3" length="49878501" type="audio/mpeg"/>
      <itunes:duration>51:57</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Guest Lecture by Paolo Zanonni, Part II</title>
      <description><![CDATA[Guest speaker Paolo Zanonni, partner at Goldman Sachs, explains a major deal in the European utilities market. Enel, a major European utility, attempted to totally transform its position by expanding into the Spanish market and acquiring the Spanish utility Endesa. The deal was exceedingly complex, and involved multiple European governments, intense regional politics, and a handful of enormous utility companies. The transaction shows the important links between politics and free-market operations.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/21%20-%20Guest%20Lecture%20by%20Paolo%20Zanonni%2C%20Part%20II.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/21%20-%20Guest%20Lecture%20by%20Paolo%20Zanonni%2C%20Part%20II.mp3" length="52293888" type="audio/mpeg"/>
      <itunes:duration>54:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Marrying the Devil in Texas</title>
      <description><![CDATA[Professor Rae discusses the case TXU v. EDF, about an electric company private equity deal that involves environmental interest groups. Professor Rae structures the discussion by contrasting the deal as viewed from the perspective of Austin, TX and Washington, DC. Actors in both locations prioritize different aspects of the deal differently. The case highlights the importance of the “customer voter base,” and the role of public opinion toward both companies and their associated politicians. Professor Rae highlights how private companies can ally themselves with environmental groups to achieve mutually beneficial goals.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/22%20-%20Marrying%20the%20Devil%20in%20Texas.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/22%20-%20Marrying%20the%20Devil%20in%20Texas.mp3" length="45969329" type="audio/mpeg"/>
      <itunes:duration>47:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Capitalist Enterprise and Clean Water for a Bolivian City</title>
      <description><![CDATA[In this final lecture of the semester, Professor Rae gives a summary of major themes, thinkers, and cases covered in the course. He begins by reviewing some foundational ideas, including Adam Smith’s invisible hand, Marxist historicism, Malthusian economics, and Schumpeter’s notion of creative destruction. Professor Rae also reviews the importance to capitalism of the modern nation state, which guarantees property rights and contracts, and recalls Hernando de Soto’s theories about the importance of formal property rights for developing countries. Various forms of corporate structure and ownership are discussed. Professor Rae concludes by introducing a case about water privatization in Bolivia.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/23%20-%20Capitalist%20Enterprise%20and%20Clean%20Water%20for%20a%20Bolivian%20City.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Capitalism_Success__Crisis_and_Reform/23%20-%20Capitalist%20Enterprise%20and%20Clean%20Water%20for%20a%20Bolivian%20City.mp3" length="48178243" type="audio/mpeg"/>
      <itunes:duration>50:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/plsc-270-2009/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
  </channel>
</rss>
