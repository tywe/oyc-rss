<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>HIST 116: The American Revolution</title>
    <link>https://oyc.yale.edu/history/hist-116</link>
    <description>From Open Yale Courses. HIST 116, Spring 2010. The American Revolution entailed some remarkable transformations–converting British colonists into American revolutionaries, and a cluster of colonies into a confederation of states with a common cause–but it was far more complex and enduring than the fighting of a war. As John Adams put it, “The Revolution was in the Minds of the people… before a drop of blood was drawn at Lexington”–and it continued long past America’s victory at Yorktown. This course will examine the Revolution from this broad perspective, tracing the participants’ shifting sense of themselves as British subjects, colonial settlers, revolutionaries, and Americans.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:09 +0000</lastBuildDate>
    <itunes:author>Joanne Freeman and Yale University</itunes:author>
    <dc:creator>Joanne Freeman</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/hist-116-2010/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Introduction: Freeman's Top Five Tips for Studying the Revolution</title>
      <description><![CDATA[Professor Freeman offers an introduction to the course, summarizing the readings and discussing the course’s main goals. She also offers five tips for studying the Revolution: 1) Avoid thinking about the Revolution as a story about facts and dates; 2) Remember that words we take for granted today, like “democracy,” had very different meanings; 3) Think of the “Founders” as real people rather than mythic historic figures; 4) Remember that the “Founders” aren’t the only people who count in the Revolution; 5) Remember the importance of historical contingency: that anything could have happened during the Revolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/001.Introduction-FreemansTopFiveTipsForStudyingTheRevolution.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/001.Introduction-FreemansTopFiveTipsForStudyingTheRevolution.mp3" length="30095872" type="audio/mpeg"/>
      <itunes:duration>40:22</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Being a British Colonist</title>
      <description><![CDATA[Professor Freeman discusses what it meant to be a British colonist in America in the eighteenth century. She explains how American colonists had deep bonds of tradition and culture with Great Britain. She argues that, as British colonists with a strong sense of their British liberties, settlers in America valued their liberties above all else. She also explains that many Americans had a sense of inferiority when they compared their colonial lifestyles to the sophistication of Europe. Professor Freeman discusses the social order in America during the eighteenth century, and suggests that the lack of an entrenched aristocracy made social rank more fluid in America than in Europe. She ends the lecture by suggesting that the great importance that American colonists placed on British liberties and their link with Britain helped pave the way for the Revolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/002.BeingABritishColonist.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/002.BeingABritishColonist.mp3" length="29677568" type="audio/mpeg"/>
      <itunes:duration>39:29</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Being a British American</title>
      <description><![CDATA[Professor Freeman discusses the differences between society in the American colonies and society in Britain in the eighteenth century. She uses examples from colonists’ writings to show that the American colonies differed from British society in three distinct ways: the distinctive character of the people who migrated to the colonies; the distinctive conditions of life in British America; and the nature of British colonial administration.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/003.BeingABritishAmerican.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/003.BeingABritishAmerican.mp3" length="30219776" type="audio/mpeg"/>
      <itunes:duration>40:14</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: "Ever at Variance and Foolishly Jealous": Intercolonial Relations</title>
      <description><![CDATA[Professor Freeman discusses colonial attempts to unite before the 1760s and the ways in which regional distrust and localism complicated matters. American colonists joined together in union three times before the 1760s. Two of these attempts were inspired by the necessity of self-defense; the third attempt was instigated by the British as a means of asserting British control over the colonies.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/004.EverAtVarianceAndFoolishlyJealous-IntercolonialRelations.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/004.EverAtVarianceAndFoolishlyJealous-IntercolonialRelations.mp3" length="31394304" type="audio/mpeg"/>
      <itunes:duration>41:38</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Outraged Colonials: The Stamp Act Crisis</title>
      <description><![CDATA[Professor Freeman concludes her discussion (from the previous lecture) of the three early instances in which the American colonies joined together to form a union. She then turns to a discussion of the Stamp Act crisis, and how American colonists found a shared bond through their dissatisfaction with the Stamp Act. Faced with massive national debts incurred by the recent war with France, Prime Minister George Grenville instituted several new taxes to generate revenue for Britain and its empire. The colonists saw these taxes as signaling a change in colonial policy, and thought their liberties and rights as British subjects were being abused. These feelings heightened with the Stamp Act of 1765. Finding a shared cause in their protestations against these new British acts, Americans set the foundation for future collaboration between the colonies.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/005.OutragedColonials-TheStampActCrisis.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/005.OutragedColonials-TheStampActCrisis.mp3" length="30773760" type="audio/mpeg"/>
      <itunes:duration>41:09</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Resistance or Rebellion? (Or, What the Heck is Happening in Boston?)</title>
      <description><![CDATA[Professor Freeman discusses the mounting tensions between the colonists and the British in the late 1760s and early 1770s. The Virginia Resolves were published and read throughout the colonies in 1765, and generated discussion about colonial rights and liberties. Colonies began working together to resolve their problems, and formed the Stamp Act Congress in 1765. Meanwhile, Boston was becoming more radicalized and mobs began acting out their frustration with British policies. Colonists began to believe that the British were conspiring to oppress their liberties, a belief that seemed to be confirmed when the British stationed troops in Boston. The mounting tension between the Bostonians and British troops culminated in the violence of the Boston Massacre in March 1770.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/006.ResistanceOrRebellionorWhatTheHeckIsHappeningInBoston.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/006.ResistanceOrRebellionorWhatTheHeckIsHappeningInBoston.mp3" length="35534848" type="audio/mpeg"/>
      <itunes:duration>46:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Being a Revolutionary</title>
      <description><![CDATA[Professor Freeman continues her discussion of the Boston Massacre and how it represented a growing sense of alienation between the American colonists and the British authorities. The Americans and British both felt that the colonies were subordinate to Parliament in some way, but differed in their ideas of the exact nature of the imperial relationship. This period saw the formation of non-importation associations to discourage merchants from importing British goods, as well as committees of correspondence to coordinate resistance. One instance of such resistance occurred in December 1773, when Boston radicals who were frustrated with the Tea Act threw shipments of tea into Boston Harbor.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/007.BeingARevolutionary.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/007.BeingARevolutionary.mp3" length="34633728" type="audio/mpeg"/>
      <itunes:duration>46:04</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: The Logic of Resistance</title>
      <description><![CDATA[Professor Freeman lays out the logic of American resistance to British imperial policy during the 1770s. Prime Minister Lord North imposed the Intolerable Acts on Massachusetts to punish the radicals for the Boston Tea Party, and hoped that the act would divide the colonies. Instead, the colonies rallied around Massachusetts because they were worried that the Intolerable Acts set a new threatening precedent in the imperial relationship. In response to this seeming threat, the colonists formed the First Continental Congress in 1774 to determine a joint course of action. The meeting of the First Continental Congress is important for four reasons: it forced the colonists to clarify and define their grievances with Britain; it helped to form ties between the colonies; it served as a training ground for young colonial politicians; and in British eyes, it symbolized a step towards rebellion. The lecture concludes with a look at the importance of historical lessons for the colonists, and how these lessons helped form a “logic of resistance” against the new measures that Parliament was imposing upon the colonies.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/008.TheLogicOfResistance.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/008.TheLogicOfResistance.mp3" length="35617280" type="audio/mpeg"/>
      <itunes:duration>47:44</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Who Were the Loyalists?</title>
      <description><![CDATA[The lecture first concludes the discussion of the First Continental Congress, which met in 1774. Ultimately, although its delegates represented a range of opinions, the voices of the political radicals in the Congress were the loudest. In October 1774, the Continental Congress passed both the radical Suffolk Resolves and the Declaration and Resolves, which laid out the colonists’ grievances with Parliament. The Congress also sent a petition to the King which warned him that the British Parliament was stripping the American colonists of their rights as English citizens. Given such radical measures, by early 1775, many American colonists were choosing sides in the growing conflict, and many chose to be Loyalists. Professor Freeman concludes her lecture with a discussion of the varied reasons why different Loyalists chose to support the British Crown, and what kinds of people tended to be Loyalists in the American Revolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/009.WhoWereTheLoyalists.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/009.WhoWereTheLoyalists.mp3" length="34172416" type="audio/mpeg"/>
      <itunes:duration>45:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Common Sense</title>
      <description><![CDATA[This lecture focuses on the best-selling pamphlet of the American Revolution: Thomas Paine’s Common Sense, discussing Paine’s life and the events that led him to write his pamphlet. Published in January of 1776, it condemned monarchy as a bad form of government, and urged the colonies to declare independence and establish their own form of republican government. Its incendiary language and simple format made it popular throughout the colonies, helping to radicalize many Americans and pushing them to seriously consider the idea of declaring independence from Britain.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/010.CommonSense.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/010.CommonSense.mp3" length="32024576" type="audio/mpeg"/>
      <itunes:duration>43:08</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Independence</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses the Declaration of Independence and sets the document in its historical context. The Declaration was not the main focus of the Second Continental Congress, which was largely concerned with organizing the defensive war effort. The Congress had sent King George III the Olive Branch Petition in a last attempt at reconciliation in August 1775, but the King ignored the petition and declared the colonies to be in rebellion. Throughout the colonies, local communities began debating the issue of independence on their own, often at the instruction of their colonial legislatures, and these local declarations of independence contributed to the formal declaration of independence by the Continental Congress in July 1776. Professor Freeman concludes the lecture by describing the decision to have Thomas Jefferson draft the Declaration.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/011.Independence.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/011.Independence.mp3" length="31098368" type="audio/mpeg"/>
      <itunes:duration>41:55</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Civil War</title>
      <description><![CDATA[Professor Freeman concludes the discussion of the Declaration of Independence. The Declaration was widely circulated and read aloud throughout the colonies. Professor Freeman argues that by 1775-1776, British and American citizens were operating under different assumptions about how the conflict between them could be resolved. The American colonists began to organize themselves for defensive measures against an aggressive British state. Meanwhile, the British assumed that the rebels were a minority group, and if they could suppress this radical minority through an impressive display of force, the rest of the colonists would submit to their governance again. Spring of 1775 saw the beginnings of military conflict between the British army and colonial militias, with fighting at Lexington, Concord, and Breed’s Hill. As a result, the colonists began to seriously consider the need for independence, and the Continental Congress began the process of organizing a war.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/012.CivilWar.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/012.CivilWar.mp3" length="32320000" type="audio/mpeg"/>
      <itunes:duration>43:29</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Organizing a War</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses four difficulties that the Continental Congress faced in organizing the colonial war effort: regionalism, localism, the supply shortage that the Continental Army faced in providing for its troops, and the Continental Congress’s inexperience in organizing an army. The lecture concludes with a discussion of a Connecticut newspaper from July 1776.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/013.OrganizingAWar.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/013.OrganizingAWar.mp3" length="36430848" type="audio/mpeg"/>
      <itunes:duration>49:00</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Heroes and Villains</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses Benedict Arnold as a case study of the ways in which ideas about regionalism, social rank, and gender–and the realities of the Continental Congress and the Continental Army–played out in this period. Like many Americans during this period, Benedict Arnold thought that he could improve his social rank and reputation in the military, but he was unable to advance due to the Continental Congress’s policy on military promotions. Frustrated and facing mounting personal debts, he decided to aid the British in exchange for a reward. Arnold and his wife Peggy developed a plan for Arnold to smuggle American military plans to the British with the help of a young British soldier named John André. However, André was captured while smuggling Arnold’s papers and the plot quickly unraveled. In the end, Arnold fled; his wife played upon conventional stereotypes of women to avoid punishment; and André was executed but idealized in the process.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/014.HeroesAndVillains.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/014.HeroesAndVillains.mp3" length="36498944" type="audio/mpeg"/>
      <itunes:duration>48:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Citizens and Choices: Experiencing the Revolution in New Haven</title>
      <description><![CDATA[To show how Americans experienced the war and made difficult choices, Professor Freeman offers a spur-of-the-moment lecture on New Haven during the Revolution, discussing how Yale College students and New Haven townspeople gradually became caught up in the war. Warfare finally came to New Haven in July 1779 when the British army invaded the town. Professor Freeman draws on first-hand accounts to provide a narrative of the invasion of New Haven.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/015.CitizensAndChoices-ExperiencingTheRevolutionInNewHaven.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/015.CitizensAndChoices-ExperiencingTheRevolutionInNewHaven.mp3" length="31648768" type="audio/mpeg"/>
      <itunes:duration>42:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: The Importance of George Washington</title>
      <description><![CDATA[This lecture focuses on George Washington and the combined qualities that made him a key figure in Revolutionary America, arguing that the most crucial reason for his success as a national leader was that he proved repeatedly that he could be trusted with power - a vital quality in a nation fearful of the collapse of republican governance at the hands of a tyrant.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/016.TheImportanceOfGeorgeWashington.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/016.TheImportanceOfGeorgeWashington.mp3" length="33441280" type="audio/mpeg"/>
      <itunes:duration>44:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: The Logic of a Campaign (or, How in the World Did We Win?)</title>
      <description><![CDATA[In this lecture, Professor Freeman explains the logic behind American and British military strategy during the early phases of the Revolution. First, she discusses the logistic disadvantages of the British during the war: the difficulties shipping men and supplies from more than three thousand miles away; the vast expanse of countryside with no one central target to attack; difficulties in recruiting British soldiers to fight in America; and the fact that the British faced a citizen army comprised of highly motivated soldiers who didn’t act in predictable ways. In addition, the British consistently underestimated the revolutionaries in America, and overestimated Loyalist support. Professor Freeman also discusses the four main phases of the Revolutionary War, differentiated by shifts in British strategy. During the earliest phase of the war, the British thought that a show of military force would quickly lead to reconciliation with the colonists. During the second phase, the British resolved to seize a major city - New York - in the hope that isolating New England from the rest of the colonies would end hostilities. By 1777, the war had entered its third phase, and the British set their sights on seizing Philadelphia and defeating George Washington. This phase ended with the Battle of Saratoga in late 1777.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/017.TheLogicOfACampaignorHowInTheWorldDidWeWin.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/017.TheLogicOfACampaignorHowInTheWorldDidWeWin.mp3" length="34869248" type="audio/mpeg"/>
      <itunes:duration>46:49</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Fighting the Revolution: The Big Picture</title>
      <description><![CDATA[Today’s lecture concludes Professor Freeman’s discussion of the four phases of the Revolutionary War. America’s victory at the Battle of Saratoga in 1777 marked the end of the third phase of the war, and led to a turning point in the conflict: France’s decision to recognize American independence and enter into an alliance with the fledging nation. Although the British made one final attempt at reconciliation in 1778 with the Conciliatory Propositions, they were rejected by the Continental Congress. The fourth and final phase of the war lasted from 1779 to 1781, as the British Army focused its attention on the American South. The British seized Charleston and South Carolina, and defeated the Continental Army in a series of battles. But with the help of the French fleet, Washington was able to defeat Cornwallis’s army at the Battle of Yorktown in 1781. Peace negotiations to end the Revolutionary War began in Paris in June of 1782.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/018.FightingTheRevolution-TheBigPicture.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/018.FightingTheRevolution-TheBigPicture.mp3" length="35942400" type="audio/mpeg"/>
      <itunes:duration>46:13</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: War and Society</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses the experiences of African Americans, women, and Native Americans during the Revolution, framing her discussion within a larger historical debate over whether or not the Revolution was “radical.” Freeman ultimately concludes that while white American males improved their position in society as a result of the Revolution, women, African Americans, and Native Americans did not benefit in the same ways.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/019.WarAndSociety.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/019.WarAndSociety.mp3" length="32205312" type="audio/mpeg"/>
      <itunes:duration>43:19</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Confederation</title>
      <description><![CDATA[This lecture discusses the ongoing political experimentation involved in creating new constitutions for the new American states. Having declared independence from Great Britain, Americans had to determine what kind of government best suited their individual states as well as the nation at large; to many, this was the “whole object” of their revolutionary turmoil. Different people had different ideas about what kind of republican government would work best for their state. Should there be a unicameral or a bicameral legislature? How should political representation be organized and effected? How far should the principle of popular sovereignty be taken?]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/020.Confederation.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/020.Confederation.mp3" length="32356352" type="audio/mpeg"/>
      <itunes:duration>43:25</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: A Union Without Power</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses the Articles of Confederation. Although they seem hopelessly weak in the long view of history, the Articles made perfect sense as a first stab at a national government by a people who deeply distrusted centralized power - a direct product of their recent experience of the British monarchy. Among the many issues that complicated the drafting of the Articles, three central issues included: how war debts to European nations would be divided among the states; whether western territories should be sold by the national government to pay for those debts; and how large and small states would compromise on representation. When a series of events - like Shays’ Rebellion - highlighted the weaknesses of the Articles, some Americans felt ready to consider a stronger national government.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/021.AUnionWithoutPower.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/021.AUnionWithoutPower.mp3" length="36153856" type="audio/mpeg"/>
      <itunes:duration>47:04</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: The Road to the Constitutional Convention</title>
      <description><![CDATA[In this lecture, Professor Freeman discusses how the new nation moved towards creating a stronger, more centralized national government than the Articles of Confederation. Complications of commerce between individual states - a factor that wasn’t regulated by the Articles - led to a series of interstate gatherings, like the Mount Vernon Conference of March 1785. Some strong nationalists saw these meetings as an ideal opportunity to push towards revising the Articles of Confederation. Professor Freeman ends with a discussion of James Madison’s preparations for the Federal Convention, and the importance of his notes in understanding the process by which delegates drafted a new Constitution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/022.TheRoadToAConstitutionalConvention.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/022.TheRoadToAConstitutionalConvention.mp3" length="33338880" type="audio/mpeg"/>
      <itunes:duration>44:20</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Creating a Constitution</title>
      <description><![CDATA[Professor Freeman discusses the debate over the Constitution at the Federal Convention of 1787 - a convention that by no means had an inevitable outcome. Indeed, even attending the Convention at all was a subject of debate in the individual states; many people feared that the Convention would produce a pseudo-monarchical form of government that would abandon the true significance of the Revolution. Ostensibly called to revise the Articles of Confederation, the meeting ultimately produced an entirely new form of government, in part, thanks to the influence of James Madison’s “Virginia Plan” of government. Professor Freeman focuses on three subjects of debate among the many that occupied the Convention: the debates over representation, slavery, and the nature of the executive branch.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/023.CreatingAConstitution.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/023.CreatingAConstitution.mp3" length="34223104" type="audio/mpeg"/>
      <itunes:duration>45:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Creating a Nation</title>
      <description><![CDATA[Professor Freeman discusses the national debate over the proposed Constitution, arguing that in many ways, when Americans debated its ratification, they were debating the consequences and meaning of the Revolution. Some feared that a stronger, more centralized government would trample on the rights and liberties that had been won through warfare, pushing the new nation back into tyranny, monarchy, or aristocracy. The Federalist essays represented one particularly ambitious attempt to quash Anti-Federalist criticism of the Constitution. In the end, the Anti-Federalists did have one significant victory, securing a Bill of Rights to be added after the new Constitution had been ratified by the states.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/024.CreatingANation.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/024.CreatingANation.mp3" length="30472704" type="audio/mpeg"/>
      <itunes:duration>40:55</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
    <item>
      <title>Lecture 25: Being an American: The Legacy of the Revolution</title>
      <description><![CDATA[Professor Freeman discusses when we can consider a revolution to have ended, arguing that a revolution is finally complete when a new political regime gains general acceptance throughout society - and that, for this reason, it is the American citizenry who truly decided the fate and trajectory of the American Revolution. Yet, in deciding the meaning of the Revolution, the evolving popular memory of its meaning counts as well. Founders like Thomas Jefferson and John Adams frequently told younger Americans not to revere the Revolution and its leaders as demigods, insisting that future generations were just as capable, if not more so, of continuing and improving America’s experiment in government. Professor Freeman concludes the lecture by suggesting that the ultimate lesson of the American Revolution is that America’s experiment in government was supposed to be an ongoing process; that the Revolution taught Americans that their political opinions and actions mattered a great deal - and that they still do.]]></description>
      <guid isPermaLink="false">https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/025.BeingAnAmerican-TheLegacyOfTheRevolution.mp3</guid>
      <enclosure url="https://archive.org/download/OpenYaleCourse-Hist116TheAmericanRevolution/025.BeingAnAmerican-TheLegacyOfTheRevolution.mp3" length="30607360" type="audio/mpeg"/>
      <itunes:duration>41:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-116-2010/img/episode-25.jpeg"/>
      <itunes:order>25</itunes:order>
    </item>
  </channel>
</rss>
