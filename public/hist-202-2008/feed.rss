<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>HIST 202: European Civilization, 1648-1945</title>
    <link>https://oyc.yale.edu/history/hist-202</link>
    <description>From Open Yale Courses. HIST 202, Fall 2008. This course offers a broad survey of modern European history, from the end of the Thirty Years’ War to the aftermath of World War II. Along with the consideration of major events and figures such as the French Revolution and Napoleon, attention will be paid to the experience of ordinary people in times of upheaval and transition. The period will thus be viewed neither in terms of historical inevitability nor as a procession of great men, but rather through the lens of the complex interrelations between demographic change, political revolution, and cultural development. Textbook accounts will be accompanied by the study of exemplary works of art, literature, and cinema.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:11 +0000</lastBuildDate>
    <itunes:author>John Merriman and Yale University</itunes:author>
    <dc:creator>John Merriman</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/hist-202-2008/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Introduction</title>
      <description><![CDATA[The course will concern European history from 1648 to 1945. The assigned readings include both standard historical texts and works of fiction, as well as films. Although the period in question encompasses many monumental events and “great men,” attention will also be paid to the development of themes over the long term and the experiences of people and groups often excluded from official histories. Among the principle questions to be addressed are the consolidation of state power, the formation of identities, linguistic and national affiliations, and the effects of economic change.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/01%20-%20Introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/01%20-%20Introduction.mp3" length="34852450" type="audio/mpeg"/>
      <itunes:duration>36:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Absolutism and the State</title>
      <description><![CDATA[The rise of absolutism in Europe must be understood in the context of insecurity attending the religious wars of the first half of the seventeenth century, and the Thirty Years’ War in particular. Faced with the unprecedented brutality and devastation of these conflicts, European nobles and landowners were increasingly willing to surrender their independence to the authority of a single, all-powerful monarch in return for guaranteed protection. Among the consequences of this consolidation of state power were the formation of large standing armies and bureaucratic systems, the curtailment of municipal privileges, and the birth of international law.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/02%20-%20Absolutism%20and%20the%20State.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/02%20-%20Absolutism%20and%20the%20State.mp3" length="43392611" type="audio/mpeg"/>
      <itunes:duration>45:12</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Dutch and British Exceptionalism</title>
      <description><![CDATA[Several reasons can be found to explain why Great Britain and the Netherlands did not follow the other major European powers of the seventeenth century in adopting absolutist rule. Chief among these were the presence of a relatively large middle class, with a vested interest in preserving independence from centralized authority, and national traditions of resistance dating from the English Civil War and the Dutch war for independence from Spain, respectively. In both countries anti-absolutism formed part of a sense of national identity, and was linked to popular anti-Catholicism. The officially Protestant Dutch, in particular, had a culture of decentralized mercantile activity far removed from the militarism and excess associated with the courts of Louis XIV and Frederick the Great.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/03%20-%20Dutch%20and%20British%20Exceptionalism.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/03%20-%20Dutch%20and%20British%20Exceptionalism.mp3" length="44678253" type="audio/mpeg"/>
      <itunes:duration>46:32</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Peter the Great</title>
      <description><![CDATA[Peter the Great’s historical significance stems not only from his military ambitions and the great expansion of the Russian Empire under his supervision, but also from his efforts to introduce secular, Western customs and ideas into Russian culture. Despite his notorious personal brutality, Peter’s enthusiasm for science and modern intellectual concerns made an indelible mark both on Russia’s relationship to the West and on its internal politics. The struggle under Peter’s reign between Westernizers and Slavophiles, or those who resist foreign influences, can be seen at work in Russia up to the present day.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/04%20-%20Peter%20the%20Great.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/04%20-%20Peter%20the%20Great.mp3" length="43876189" type="audio/mpeg"/>
      <itunes:duration>45:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: The Enlightenment and the Public Sphere</title>
      <description><![CDATA[While the major philosophical projects of the Enlightenment are associated with the names of individual thinkers such as Montesquieu, Rousseau, and Voltaire, the cultural transformation in France in the years leading up to the Revolution should also be understood in the context of the public sphere and popular press. Alongside such luminaries as those associated with Diderot’s Encyclopédie were a host of lesser pamphleteers and libellists eager for fame and some degree of fortune. If the writings of this latter group were typically vulgar and bereft of literary merit, they nonetheless contributed to the “desacralization” of monarchy in the eyes of the growing literate public. Lawyers’ briefs, scandal sheets and pornographic novels all played a role in robbing the monarchy of its claim to sacred authority at the same time as they helped advance the critique of despotism that would serve as a major impetus for the Revolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/05%20-%20The%20Enlightenment%20and%20the%20Public%20Sphere%20.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/05%20-%20The%20Enlightenment%20and%20the%20Public%20Sphere%20.mp3" length="46055011" type="audio/mpeg"/>
      <itunes:duration>47:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Maximilien Robespierre and the French Revolution</title>
      <description><![CDATA[Robespierre’s ascetic personal life and severe philosophy of political engagement are attributed by some to his difficult childhood. As a revolutionary, one of his most significant insights was that the Revolution was threatened not only by France’s military adversaries abroad, but also by domestic counter-revolutionaries. Under this latter heading were gathered two major groups, urban mercantilists and rural peasants. Relative strength of religious commitment is the major factor in explaining why some regions of France rose up in defense of the monarchy while others supported the Revolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/06%20-%20Maximilien%20Robespierre%20and%20the%20French%20Revolution.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/06%20-%20Maximilien%20Robespierre%20and%20the%20French%20Revolution.mp3" length="47946694" type="audio/mpeg"/>
      <itunes:duration>49:56</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Napoleon</title>
      <description><![CDATA[One way of understanding Napoleon’s life is through attention to his Corsican origins. Although Napoleon himself would later disavow his earlier identification with the island in favor of French identity, many of his actions and attitudes agree with stereotypical notions of Corsican culture. Did Napoleon inaugurate the era of total war? This question, posed in a recent book, is up for debate. On one hand, the violence of the Revolution and the Napoleonic wars may not seem uniquely devastating in comparison to the ravages of the Thirty Years’ War. On the other hand, the faltering of distinctions between civilian and combatant as well as the large-scale mobilization of state resources for war do anticipate the modern concept of total war, typically associated with World War II.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/07%20-%20Napoleon.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/07%20-%20Napoleon.mp3" length="46467118" type="audio/mpeg"/>
      <itunes:duration>48:24</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Industrial Revolutions</title>
      <description><![CDATA[The Industrial Revolution was for a long time treated as a decisive break in which some countries, specifically England, innovated and progressed rapidly while others were left behind. This type of analysis leads many historians to overlook the more gradual process of industrialization in countries like France, and the persistence of older methods of artisanal production alongside new forms of mechanization. To understand the Industrial Revolution it is also necessary to take into account the Agricultural Revolution; the consequences of these twin developments include urban expansion and the “proletarianization” of rural laborers. Among the consequences of industrialization for workers are the imposition of industrial discipline and the emergence of schemes such as Taylorism dedicated to more efficiently exploiting industrial labor.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/08%20-%20Industrial%20Revolutions.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/08%20-%20Industrial%20Revolutions.mp3" length="46306622" type="audio/mpeg"/>
      <itunes:duration>48:14</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Middle Classes</title>
      <description><![CDATA[The nineteenth century in Europe is, in many ways, synonymous with the rise of the bourgeoisie. It is misleading, however, to consider this newly dominant middle class as a homogenous group; rather, the century may be more accurately described in terms of the rise of plural middle classes. While the classes comprising this group were united by their search for power based on property rights rather than hereditary privilege, they were otherwise strikingly diverse. Contemporary stereotypes of the bourgeois as a grasping philistine ought to therefore be nuanced. Along with the real, undeniable cruelty of many capitalists with respect to their workers, the middle classes also pioneered the first philanthropic voluntary associations, broadened the reach of public education, and inspired the development of effective birth control.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/09%20-%20Middle%20Classes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/09%20-%20Middle%20Classes.mp3" length="48562766" type="audio/mpeg"/>
      <itunes:duration>50:35</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Popular Protest</title>
      <description><![CDATA[Collective violence, in the form of popular protest, was one of the principal ways in which people resisted the expansion of capitalism and the state throughout the nineteenth century. The nature of this protest can be charted through three different, but related examples: grain riots across Europe in the first half of the century, the mythical figure of Captain Swing in England, and the Demoiselles of the Ariège in France. While these movements were ultimately repressed by the forces of capital and state power, they represented an attempt on the part of working people, the “remainders” of history, to impose an idea of popular justice.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/10%20-%20Popular%20Protest.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/10%20-%20Popular%20Protest.mp3" length="45375827" type="audio/mpeg"/>
      <itunes:duration>47:15</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Why No Revolution in 1848 in Britain</title>
      <description><![CDATA[Revolutions occur when a critical mass of people come together to make specific demands upon their government. They invariably involve an increase in popular involvement in the political process. One of the central questions concerning 1848, a year in which almost every major European nation faced a revolutionary upsurge, is why England did not have its own revolution despite the existence of social tensions. Two principal reasons account for this fact: first, the success of reformist political measures, and the existence of a non-violent Chartist movement; second, the elaboration of a British self-identity founded upon a notion of respectability. This latter process took place in opposition to Britain’s cultural Other, Ireland, and its aftereffects can be seen in Anglo-Irish relations well into the twentieth century.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/11%20-%20Why%20no%20Revolution%20in%201848%20in%20Britain.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/11%20-%20Why%20no%20Revolution%20in%201848%20in%20Britain.mp3" length="40774514" type="audio/mpeg"/>
      <itunes:duration>42:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Nineteenth-Century Cities</title>
      <description><![CDATA[The nineteenth century witnessed an unprecedented degree of urbanization, an increase in urban population growth relative to population growth generally. One of the chief consequences of this growth was class segregation, as the bourgeoisie and upper classes were forced to inhabit the same confined space as workers. Significantly, this had opposed effects in Europe, where the working classes typically inhabit the periphery of cities, and the United States, where they are most often in the city center itself. The growth of cities was accompanied by a high-pitched rhetoric of disease and decay, as the perceived hygienic problems of concentrated urban populations were extrapolated to refer to the city itself as a biological organism. The Baron Haussmann’s reconstruction of Paris under the Second Empire is a classic example of the intertwinement of urban development, capitalism and state power.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/12%20-%20%20Nineteenth-Century%20Cities.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/12%20-%20%20Nineteenth-Century%20Cities.mp3" length="49425016" type="audio/mpeg"/>
      <itunes:duration>51:29</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Nationalism</title>
      <description><![CDATA[In light of the many ethnic and national conflicts of the twentieth century, the dissolution of the Austro-Hungarian Empire in 1918 appears less surprising than the fact that it remained intact for so long. National identity is not an essential characteristic of peoples, and in many cases in Europe it is a relatively recent invention. As such, there are many different characteristics according to which national communities can be defined, or, in Benedict Anderson’s phrase, imagined. Along with religion and ethnicity, language has played a particularly important role in shaping the imaginary identification of individuals with abstract communities. No one factor necessarily determines this identification, as evidenced by modern countries such as Belgium and Switzerland that incorporate multiple linguistic and cultural groups in one national community.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/13%20-%20Nationalism.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/13%20-%20Nationalism.mp3" length="48801421" type="audio/mpeg"/>
      <itunes:duration>50:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Radicals</title>
      <description><![CDATA[Socialism in the nineteenth century can be divided into two different strains of thought: reformist and revolutionary. While reformist socialists believed in changing the State through legal activity, such as voting, revolutionary socialists viewed such measures as ineffective and perhaps even complicit in maintaining the status quo. Along the spectrum of leftwing political thought, syndicalists and anarchists shared the conviction that the State could not be reformed from within. In some cases, this conviction resulted in acts of violence, so-called propaganda by the deed. Émile Henry, a French anarchist, was among the first militants to target civilian rather than official targets; as such, he can be seen as one of the first modern terrorists.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/14%20-%20Radicals.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/14%20-%20Radicals.mp3" length="47603132" type="audio/mpeg"/>
      <itunes:duration>49:35</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Imperialists and Boy Scouts</title>
      <description><![CDATA[The boom in European colonial expansion in the second half of the nineteenth century, the so-called New Imperialism, can be seen to follow from three principle factors, in ascending order of importance: religious proselytizing, profit, and inter-imperial political strategy. With respect to the latter concern, the conflicts emerging from imperialism set the stage for World War I. Along with its military and industrial consequences, imperialism also entailed a large-scale cultural program dedicated to strengthening support for its objectives among the domestic populations of the imperial powers. The creation of the Boy Scouts is an exemplary form of such a program, founded upon a mythology of the American frontier reformulated to encompass Africa and Asia.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/15%20-%20Imperialists%20and%20Boy%20Scouts.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/15%20-%20Imperialists%20and%20Boy%20Scouts.mp3" length="49013744" type="audio/mpeg"/>
      <itunes:duration>51:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: The Coming of the Great War</title>
      <description><![CDATA[If the early years of the twentieth century were marked by a general consensus that a major war was impending, no similar consensus existed concerning the likely form that war would take. Not only the carnage of World War I, but also the nature of its alliances would have been difficult to imagine. Indeed, in 1900 many people would have predicted conflict, rather than collaboration, between France and Britain. The reasons for the eventual entente between France and Britain and France and Russia consist principally in economic and geopolitical motivations. Cultural identity also played a role, particularly in relations between France and Germany. The territory of Alsace-Lorraine formed a crucible for the questions of nationalism and imaginary identity that would be contested in the Great War.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/16%20-%20The%20Coming%20of%20the%20Great%20War.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/16%20-%20The%20Coming%20of%20the%20Great%20War.mp3" length="46122720" type="audio/mpeg"/>
      <itunes:duration>48:02</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: War in the Trenches</title>
      <description><![CDATA[With the failure of Germany’s offensive strategy, WWI became a war of defense, in which trenches played a major role. The use of trenches and barbed wire, coupled with the deployment of new, more deadly forms of artillery, created extremely bloody stalemate situations. The hopelessness of this arrangement resulted in a number of mutinies on the French side, motivated neither by defeatism nor by ideology, but rather by the sheer horror of trench warfare. Due to the unprecedented scale of casualties, WWI impressed itself irresistibly upon the cultural imagination of the combatant nations.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/17%20-%20War%20in%20the%20Trenches.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/17%20-%20War%20in%20the%20Trenches.mp3" length="48360474" type="audio/mpeg"/>
      <itunes:duration>50:22</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Sites of Memory, Sites of Mourning (Guest Lecture by Jay Winters)</title>
      <description><![CDATA[As a result of World War I, Europe had a different understanding of war in the twentieth century than the United States. One of the most important ways in which the First World War was experienced on the continent and in Britain was through commemoration. By means of both mass-media technologies and older memorial forms, sites of memory offered opportunities for personal as well as political reconciliation with the unprecedented consequences of the war. The influence of these sites is still felt today, in a united Europe, as the importance of armies has diminished in favor of social welfare programs.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/18%20-%20Sites%20of%20Memory%2C%20Sites%20of%20Mourning.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/18%20-%20Sites%20of%20Memory%2C%20Sites%20of%20Mourning.mp3" length="44622665" type="audio/mpeg"/>
      <itunes:duration>46:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: The Romanovs and the Russian Revolution</title>
      <description><![CDATA[The period between the Russian Revolution of February 1917, which resulted in the overthrow of the autocracy and the establishment of a provisional government, and the Bolshevik Revolution in October of that same year, offers an instructive example of revolutionary processes at work. During this interval, the fate of Nicholas II and his wife, Alexandra, was bound up in the struggle for power amongst competing political factions in Russia. Until his death, Nicholas was convinced that the Russian people would rescue him from his captors. Such a belief would prove to be delusional, and the efforts on the part of liberals, socialists, and some Bolsheviks to arrange for a trial would fail to save the czar from the verdict of history.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/19%20-%20The%20Romanovs%20and%20the%20Russian%20Revolution.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/19%20-%20The%20Romanovs%20and%20the%20Russian%20Revolution.mp3" length="45080330" type="audio/mpeg"/>
      <itunes:duration>46:57</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Successor States of Eastern Europe</title>
      <description><![CDATA[Contrary to the “Great Illusion” that the end of World War I heralded a new era of peace, the interwar period can be considered to form part of a Thirty Years’ War, spanning the period from 1914 to 1945. In the wake of the Treaty of Versailles, Europe was divided both literally and figuratively, with the so-called revisionist powers frustrated over their new borders. One of the most significant and ultimately most pernicious debates at Versailles concerned the identity of states with ethnic majorities. For those nations that resented the new partition of Europe, ethnic minorities, and Jews in particular, furnished convenient scapegoats. The persecution of minority groups in Central and Eastern Europe following the First World War thus set the stage for the atrocities of World War II.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/20%20-%20Successor%20States%20of%20Eastern%20Europe.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/20%20-%20Successor%20States%20of%20Eastern%20Europe.mp3" length="42786570" type="audio/mpeg"/>
      <itunes:duration>44:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Stalinism</title>
      <description><![CDATA[One of the central questions in assessing Stalinism is whether or not the abuses of the latter were already present in the first years of the Russian Revolution. The archival evidence suggests that this is partly the case, and that even in its early stages Soviet Russia actively persecuted not just those who were believed to have profited unfairly, without laboring, but also non-Russian ethnic groups. Stalin, although not an ethnic Russian himself, was committed to the assimilation of national identity, and universal identification with the Soviet State. This commitment, coupled with his paranoia, lead to executions and deportations aimed at solidifying the state through exclusion of “undesirable” or politically suspect elements. Throughout years of economic hardship and violent purges, Soviet rhetoric consistently emphasized a glorious future in order to justify the miseries of the present. Such a future proved, in many ways, to be an illusion.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/21%20-%20Stalinism.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/21%20-%20Stalinism.mp3" length="44908549" type="audio/mpeg"/>
      <itunes:duration>46:46</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Fascists</title>
      <description><![CDATA[While Nazi Germany’s crimes were unprecedented, Adolf Hitler himself was in many respects a typical figure. An idle youth, of seemingly mediocre talents, his political career and passionate hatreds were formed by the experience of World War I. The rise of fascism in Germany, as elsewhere, must be understood in the context of a postwar climate of resentment and instability. Germany’s economic crisis, in particular, led the middle classes to support National Socialism well before any other group. This resentment would find a ready outlet in the form of increasingly persecuted minority populations, above all the Jews. In considering Nazism against the backdrop of a more general wave of extreme rightwing and fascist political sentiment, it is important to note that the policies of the Third Reich were not only known to but also endorsed by the majority of the German population.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/22%20-%20Fascists.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/22%20-%20Fascists.mp3" length="45579791" type="audio/mpeg"/>
      <itunes:duration>47:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Collaboration and Resistance in World War II</title>
      <description><![CDATA[One of the principal myths concerning collaboration during World War II in France, as in other countries, is that the domestic collaborators did so despite themselves, or to prevent even greater atrocities. In fact, many French, Belgians, Hungarians, Poles, Dutch and others voluntarily and enthusiastically abetted the occupying Germans. This collaboration, inspired by anti-Semitism and xenophobia, often resulted in extremely zealous persecution of Jewish nationals, communists, and others. Along with the myth of reluctant collaboration, France has also been obliged to confront the myth of widespread resistance, promulgated in part by a victorious Charles de Gaulle. Many questions concerning collaboration and resistance still remain unresolved in formerly occupied European countries to this day.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/23%20-%20Collaboration%20and%20Resistance%20in%20World%20War%20II.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/23%20-%20Collaboration%20and%20Resistance%20in%20World%20War%20II.mp3" length="43680584" type="audio/mpeg"/>
      <itunes:duration>45:30</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: The Collapse of Communism and Global Challenges</title>
      <description><![CDATA[The disintegration of the Soviet Union resulted from a number of different factors. Three important ones are nationalism among Soviet satellite states, democratic opposition movements, and economic crisis. Along with these elements, the role of Mikhail Gorbachev should not be discounted. Although his attempt to reform communism was rejected, his reformist positions as Soviet premier helped open the way for full-fledged political dissidence. One of the major challenges faced by Europe in the wake of the collapse of communism has been that posed by ethnic nationalism, a problem that erupted violently in the Balkans in the 1990s. Immigration and the defense of human rights are two problems that now confront the United States, as well as a United Europe.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_European_Civilization__1648-1945/24%20-%20The%20Collapse%20of%20Communism%20and%20Global%20Challenges.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_European_Civilization__1648-1945/24%20-%20The%20Collapse%20of%20Communism%20and%20Global%20Challenges.mp3" length="39591690" type="audio/mpeg"/>
      <itunes:duration>41:14</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-202-2008/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
  </channel>
</rss>
