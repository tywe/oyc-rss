<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>HIST 276: France Since 1871</title>
    <link>https://oyc.yale.edu/history/hist-276</link>
    <description>From Open Yale Courses. HIST 276, Fall 2007. This course covers the emergence of modern France. Topics include the social, economic, and political transformation of France; the impact of France’s revolutionary heritage, of industrialization, and of the dislocation wrought by two world wars; and the political response of the Left and the Right to changing French society.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:13 +0000</lastBuildDate>
    <itunes:author>John Merriman and Yale University</itunes:author>
    <dc:creator>John Merriman</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/hist-276-2007/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Introduction</title>
      <description><![CDATA[Professor Merriman lists the books on the syllabus, and offers a brief précis of each of them. Three of the principal themes of the course will be national identity, linguistic identity, and the consequences of the two world wars. Although the course will consider some well-known historical figures, such as Hitler and de Gaulle, it will also examine the individual histories of ordinary people.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/01%20-%20Introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/01%20-%20Introduction.mp3" length="40896558" type="audio/mpeg"/>
      <itunes:duration>42:36</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Centralized State and Republic</title>
      <description><![CDATA[Despite various attempts at reform, France remains the most centralized state in Europe. The organization of the country around the Parisian center was originally a consequence of the French Revolution, which gave birth to the departmental regions. These regions have retained an oppositional relationship towards the metropolitan center. In 1875, an enduring republic was formed despite the competing claims of the Comte de Chambord and the Orleanists. This republic owed its founding largely to support from workers and peasants in the various non-Parisian departments.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/02%20-%20The%20Paris%20Commune%20and%20Its%20Legacy.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/02%20-%20The%20Paris%20Commune%20and%20Its%20Legacy.mp3" length="43003491" type="audio/mpeg"/>
      <itunes:duration>44:47</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 5: The Waning of Religious Authority</title>
      <description><![CDATA[Religion in France after the Revolution can be understood in terms of two forms of de-Christianization. The first of these is political, and takes place in the de jure separation of church and state. The second is a decline in religious practice among individual citizens. While the history of the former change is well documented, the latter is a more ambiguous phenomenon. Despite the statistical decline in religious participation in the nineteenth and twentieth centuries, Catholicism in particular continues to play a significant role in the cultural imagination, or imaginaire, of many French people.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/03%20-%20Centralized%20State%20and%20Republic.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/03%20-%20Centralized%20State%20and%20Republic.mp3" length="46510168" type="audio/mpeg"/>
      <itunes:duration>48:26</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Workshop and Factory</title>
      <description><![CDATA[The Industrial Revolution in France is often said to have been entirely overshadowed by British industrial development. This analysis is inaccurate because it ignores the significance of domestic and other non-factory occupations. Indeed, it was the class of artisan workers, rather than industrial factory workers, who were first responsible for the organization of labor movements. One of the great innovations of the factory was the imposition of industrial discipline, against which many workers rebelled, often in the form of strikes.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/04%20-%20A%20Nation_%20Peasants%2C%20Language%2C%20and%20French%20Identity.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/04%20-%20A%20Nation_%20Peasants%2C%20Language%2C%20and%20French%20Identity.mp3" length="46414038" type="audio/mpeg"/>
      <itunes:duration>48:20</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Mass Politics and the Political Challenge from the Left</title>
      <description><![CDATA[The history of socialism in France can be understood in terms of the competition between revolutionary socialists and reform socialists. The former advocated abandoning electoral politics, while the latter attempted to improve conditions for workers by means of reforms within the political system. These two attitudes found figureheads in Jules Guesdes and Paul Brousse, respectively. Reform socialists and revolutionary socialists are united under the leadership of Jean Jaures, whose organizing efforts define and influence French socialism well into the twentieth century.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/05%20-%20Workshop%20and%20Factory.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/05%20-%20Workshop%20and%20Factory.mp3" length="40515797" type="audio/mpeg"/>
      <itunes:duration>42:12</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Dynamite Club: The Anarchists</title>
      <description><![CDATA[Anarchists, unlike syndicalists and other leftists, seek to destroy the state rather than to capture state power for themselves. Emile Henry and other late nineteenth-century radicals inaugurated the modern practice of terrorism in their individualism and their indiscriminate choice of civilian targets. Despite the terrifying consequences of individual acts of terrorism, these pale in comparison to the consequences of state terrorism.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/06%20-%20The%20Waning%20of%20Religious%20Authority.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/06%20-%20The%20Waning%20of%20Religious%20Authority.mp3" length="49422090" type="audio/mpeg"/>
      <itunes:duration>51:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 9: General Boulanger and Captain Dreyfus</title>
      <description><![CDATA[Two of the major crises of nineteenth-century France, the Boulanger Affair and the Dreyfus Affair, can be understood in terms of the rising forces of anti-Semitism and Far Right politics. The German conquest of Alsace and Lorraine, in particular, fueled nationalist and right-wing sentiments, especially in rural France. Political orientations and prejudices were formed by the popular media of the time, such as illustrated periodicals and patriotic songs.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/07%20-%20Mass%20Politics%20and%20the%20Political%20Challenge%20from%20the%20Left.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/07%20-%20Mass%20Politics%20and%20the%20Political%20Challenge%20from%20the%20Left.mp3" length="45818028" type="audio/mpeg"/>
      <itunes:duration>47:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Cafés and the Culture of Drink</title>
      <description><![CDATA[Because drinking is such an integral part of French culture, alcohol abuse has been historically ignored. Although there have been celebrated attempts to address this problem, such as Zola L’Assomoir, it is only in the past five or ten years that the government has seriously tried to tackle the problem of alcoholism. One of the major ways in which alcohol is embedded in the cultural identity of the country is the close association of certain wines and liquors with their regions of production. Likewise, different types of bars serve as loci for social interaction, and have always played a central role in rural as well as urban life.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/08%20-%20Dynamite%20Club_%20The%20Anarchists.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/08%20-%20Dynamite%20Club_%20The%20Anarchists.mp3" length="48812705" type="audio/mpeg"/>
      <itunes:duration>50:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Paris and the Belle Époque</title>
      <description><![CDATA[Modern Paris was indelibly shaped by the rebuilding project ordered by Napoleon III and carried out by Baron Haussmann in the 1850s and ’60s. The large-scale demolition of whole neighborhoods in central Paris, coupled with a boom in industrial development outside the city, cemented a class division between center and periphery that has persisted into the twenty-first century. Curiously, this division is the obverse of the arrangement of most American cities, in which the inner city is typically impoverished while the suburbs are wealthy.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/09%20-%20General%20Boulanger%20and%20Captain%20Dreyfus.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/09%20-%20General%20Boulanger%20and%20Captain%20Dreyfus.mp3" length="45476555" type="audio/mpeg"/>
      <itunes:duration>47:22</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 12: French Imperialism (Guest Lecture by Charles Keith)</title>
      <description><![CDATA[France’s colonial properties were thought of in the latter half of the nineteenth century as consolation for the bitter loss of Alsace and Lorraine to Germany. As civilian administrators came to replace military personnel in the colonies, and as more and more French settlers arrived, empire and colonialism came to play an important function in France’s cultural self-presentation. World War I heralded the eventual decline of the French empire, a decline realized at the hands of the colonized subjects themselves.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/10%20-%20Cafes%20and%20the%20Culture%20of%20Drink.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/10%20-%20Cafes%20and%20the%20Culture%20of%20Drink.mp3" length="47515778" type="audio/mpeg"/>
      <itunes:duration>49:29</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 13: The Origins of World War I</title>
      <description><![CDATA[The traditional, diplomatic history of World War I is helpful in understanding how a series of hitherto improbable alliances come to be formed in the early years of the twentieth century. In the case of France and Russia, this involves a significant ideological compromise. Along with the history of imperial machinations, however, World War I should be understood in the context of the popular imagination and the growth of nationalist sentiment in Europe.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/11%20-%20Paris%20and%20the%20Belle%20Epoque.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/11%20-%20Paris%20and%20the%20Belle%20Epoque.mp3" length="48943527" type="audio/mpeg"/>
      <itunes:duration>50:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Trench Warfare</title>
      <description><![CDATA[The sacred union that united France’s political parties during World War I contributed to a resilient morale on the home front. Germany’s invasion of France, and the conflict over Alsace-Lorraine in particular, contributed to French concern over atrocities and the national investment in the war effort. New weapons and other fighting technologies, coupled with the widespread use of trenches, made fighting tremendously difficult and gruesome on all fronts.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/12%20-%20French%20Imperialism%20%28Charles%20Keith%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/12%20-%20French%20Imperialism%20%28Charles%20Keith%29.mp3" length="42171752" type="audio/mpeg"/>
      <itunes:duration>43:55</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 15: The Home Front</title>
      <description><![CDATA[1917 is a critical moment in World War I, as the Bolsheviks seize power in Russia and Woodrow Wilson leads the U.S. into war on the side of the Allied powers. Although morale held steady on the home front in France, there were multiple mutinies and strikes as the war progressed. These mutinies were not in favor of German victory; rather, they were in protest of corruption at home, in the form of incompetence and profiteering. Literary and historical records of World War I bear witness to the difficulty faced by soldiers in reentering civilian life after returning home.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/13%20-%20The%20Origins%20of%20World%20War%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/13%20-%20The%20Origins%20of%20World%20War%20I.mp3" length="44027491" type="audio/mpeg"/>
      <itunes:duration>45:51</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 16: The Great War, Grief, and Memory (Guest Lecture by Bruno Cabanes)</title>
      <description><![CDATA[The human cost of World War I cannot be understood only in terms of demographics. To better understand the consequences of the war upon both soldiers and civilians it is necessary to consider mourning in its private, as well as its public dimensions. Indeed, for many French people who lived through the war, public spectacles of bereavement, such as the Unknown Soldier, were also conceived of as intensely private affairs. Both types of mourning are associated with a wide variety of rituals and procedures.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/14%20-%20Trench%20Warfare.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/14%20-%20Trench%20Warfare.mp3" length="43378818" type="audio/mpeg"/>
      <itunes:duration>45:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 17: The Popular Front</title>
      <description><![CDATA[A plethora of Far Right and fascist organizations emerged in the wake of World War I. Economic depression, nationalism, anti-Semitism and xenophobia all played a part in this upsurge. On the left, the tension between communist revolutionaries and socialist reformers was reconciled, for a time, in the Popular Front government of Leon Blum. While the Popular Front would eventually fall, it pioneered many of the reforms and progressive measures that French workers enjoy today.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/15%20-%20The%20Home%20Front.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/15%20-%20The%20Home%20Front.mp3" length="48262671" type="audio/mpeg"/>
      <itunes:duration>50:16</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 18: The Dark Years: Vichy France</title>
      <description><![CDATA[For decades after the end of World War II the question of French collaboration with the Nazis was obscured. One of the reasons for this was the desire of de Gaulle and others to downplay the central role of communists in resisting the occupation. In fact, many French civilians were involved to greater or lesser degrees in informing upon their fellows or otherwise furthering the interests of the German invaders. Under the Vichy regime, right-wing politics in France developed an ideological program founded upon an appeal to nationalism, the soil, and the rejection of perceived decadence.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/16%20-%20The%20Great%20War%2C%20Grief%2C%20and%20Memory%20%28Bruno%20Cabanes%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/16%20-%20The%20Great%20War%2C%20Grief%2C%20and%20Memory%20%28Bruno%20Cabanes%29.mp3" length="44132816" type="audio/mpeg"/>
      <itunes:duration>45:58</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Resistance</title>
      <description><![CDATA[If the extent of French collaboration during World War II has been obscured, so too has the nature of resistance. Although the communist Left represented the core of the resistance movement, resistors came from any different backgrounds, including in their ranks Catholics, Protestants, Jews and socialists. Unlike the relationship between de-Christianization and right-wing politics, in the case of the resistance there is no clear correlation between regional locations and cells of resistors. It has been argued that the definition of resistance itself should be broadened to include the many acts of passive resistance carried out by French civilians during the occupation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/17%20-%20The%20Popular%20Front.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/17%20-%20The%20Popular%20Front.mp3" length="45739451" type="audio/mpeg"/>
      <itunes:duration>47:38</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Battles For and Against Americanization</title>
      <description><![CDATA[Anti-Americanism in France has historically been directed toward the U.S. government and corporations rather than American citizens. In the wake of World War II, the Marshall Plan for rebuilding Europe was considered by many to be a form of American imperialism. Along with the establishment of American military bases on French soil, the years after World War II bore witness to a great influx of American products, notably refrigerators and Coca-Cola. French concern over American cultural imports persists today, and has extended to include policies aimed at keeping the French language free of English words.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/18%20-%20The%20Dark%20Years_%20Vichy%20France.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/18%20-%20The%20Dark%20Years_%20Vichy%20France.mp3" length="49375697" type="audio/mpeg"/>
      <itunes:duration>51:25</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Vietnam and Algeria</title>
      <description><![CDATA[France’s colonial territories were of very high importance after the embarrassment of occupation during World War II. Algeria, in particular, was a complicated case because it involved large numbers of French settlers, the pieds-noirs. Despite international support for Algerian independence, right-wing factions in the military and among the colonizers remained committed to staying the course. After Charles de Gaulle presided over French withdrawal, the cause of thepieds-noirs has remained divisive in French political life, particularly on the right.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/19%20-%20Resistance.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/19%20-%20Resistance.mp3" length="47727265" type="audio/mpeg"/>
      <itunes:duration>49:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Charles De Gaulle</title>
      <description><![CDATA[Charles de Gaulle’s importance in postwar French political life was matched by his importance in the nation’s collective imagination. This authority was consciously contrived by de Gaulle, who wished to bear upon his figurative body the will of the French people to maintain the power of their nation in the face of a political environment characterized by the opposition between the U.S. and the Soviet Union. Ultimately, de Gaulle’s symbolic originality proved more lasting than his political innovations.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/20%20-%20Battles%20For%20and%20Against%20Americanization.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/20%20-%20Battles%20For%20and%20Against%20Americanization.mp3" length="45726913" type="audio/mpeg"/>
      <itunes:duration>47:37</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 23: May 1968</title>
      <description><![CDATA[The student protests of May 1968 in France were linked to international protests against the American war in Vietnam and other political and social consequences of the Cold War. In many respects, the terrible condition of many schools in France that led students to revolt remains a problem. Recent attempts to impose American-style reforms on the university system have met with protests that echo some of the demands made in ‘68; although, other conditions for revolution seem as though they may never again be realized in the same way.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/21%20-%20Vietnam%20and%20Algeria.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/21%20-%20Vietnam%20and%20Algeria.mp3" length="36793453" type="audio/mpeg"/>
      <itunes:duration>38:19</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Immigration</title>
      <description><![CDATA[French culture is threatened both by European Unification and the rise of xenophobia within France itself. The defeat of the referendum on the European Constitution testified to the dissatisfaction of many people in rural France with the economic realities of the new international community. Racist policies targeting residents of France’s poor suburbs threaten the national ideal of liberty, equality, and fraternity. These problems remain to be resolved if France is to preserve its unique identity.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_France_Since_1871/22%20-%20Charles%20De%20Gaulle.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_France_Since_1871/22%20-%20Charles%20De%20Gaulle.mp3" length="45119618" type="audio/mpeg"/>
      <itunes:duration>46:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/hist-276-2007/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
  </channel>
</rss>
