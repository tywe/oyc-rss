<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>ASTR 160: Frontiers and Controversies in Astrophysics</title>
    <link>https://oyc.yale.edu/astronomy/astr-160</link>
    <description>From Open Yale Courses. ASTR 160, Spring 2007. This course focuses on three particularly interesting areas of astronomy that are advancing very rapidly: Extra-Solar Planets, Black Holes, and Dark Energy. Particular attention is paid to current projects that promise to improve our understanding significantly over the next few years. The course explores not just what is known, but what is currently not known, and how astronomers are going about trying to find out.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:18:58 +0000</lastBuildDate>
    <itunes:author>Charles Bailyn and Yale University</itunes:author>
    <dc:creator>Charles Bailyn</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/astr-160-2007/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Introduction</title>
      <description><![CDATA[Professor Bailyn introduces the course and discusses the course material and requirements. The three major topics that the course will cover are (1) exoplanets–planets around stars other than the Sun, (2) black holes–stars whose gravitational pull is so strong that even their own light rays cannot escape, and (3) cosmology–the study of the Universe as a whole. Class proper begins with a discussion on planetary orbits. A brief history of astronomy is also given and its major contributors over the centuries are introduced: Ptolemy, Galileo, Copernicus, Kepler, and Newton.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/01%20-%20Introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/01%20-%20Introduction.mp3" length="44794028" type="audio/mpeg"/>
      <itunes:duration>46:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Planetary Orbits</title>
      <description><![CDATA[Exoplanets are introduced and students learn how astronomers detect their presence as well as the challenges associated with it. Physics equations are explained as well as their importance in the context of the course. A number of problems are worked out to get students used to dealing with large numbers in calculating planetary masses, interplanetary distances, etc.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/02%20-%20Planetary%20Orbits.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/02%20-%20Planetary%20Orbits.mp3" length="49327631" type="audio/mpeg"/>
      <itunes:duration>51:22</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Our Solar System and the Pluto Problem</title>
      <description><![CDATA[Class begins with a review of the first problem set. Newton’s Third Law is applied in explaining how exoplanets are found. An overview of the Solar System is given; each planet is presented individually and its special features are highlighted. Astronomy is discussed as an observational science, and the subject of how to categorize objects in the Solar System is addressed. The Pluto controversy is given special attention and both sides of the argument regarding its status are considered.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/03%20-%20Our%20Solar%20System%20and%20the%20Pluto%20Problem.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/03%20-%20Our%20Solar%20System%20and%20the%20Pluto%20Problem.mp3" length="44075556" type="audio/mpeg"/>
      <itunes:duration>45:54</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Discovering Exoplanets: Hot Jupiters</title>
      <description><![CDATA[The formation of planets is discussed with a special emphasis on the bodies in the Solar System. Planetary differences between the celestial bodies in the Inner and Outer Solar System are observed. Professor Bailyn explains how the outlook of our Solar System can predict what other star systems may look like. It is demonstrated how momentum equations are applied in astronomers’ search for exoplanets. Planet velocities are discussed and compared in relation to a planet’s mass. Finally, the Doppler shift is introduced and students learn how it is used to measure the velocity of distant objects, such as galaxies and planets.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/04%20-%20Discovering%20Exoplanets_%20Hot%20Jupiters.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/04%20-%20Discovering%20Exoplanets_%20Hot%20Jupiters.mp3" length="44806985" type="audio/mpeg"/>
      <itunes:duration>46:40</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Planetary Transits</title>
      <description><![CDATA[Professor Bailyn talks about student responses for a paper assignment on the controversy over Pluto. The central question is whether the popular debate is indeed a “scientific controversy.” A number of scientific “fables” are discussed and a moral is associated with each: the demotion of Pluto (moral: science can be affected by culture); the discovery of 51 Peg b (morals: expect the unexpected, and look at your data); the disproof of pulsation as explanation for the Velocity Curves (moral: sometimes science works like science).]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/05%20-%20Planetary%20Transits.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/05%20-%20Planetary%20Transits.mp3" length="47378687" type="audio/mpeg"/>
      <itunes:duration>49:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Microlensing, Astrometry and Other Methods</title>
      <description><![CDATA[The class begins with a discussion on transits – important astronomical events that help astronomers to find new planets. The event occurs when a celestial body moves across the face of the star it revolves around and blocks some of its light. By calculating the amount of light that is being obscured astronomers can obtain important information about both star and planet, such as size, density, radial velocity and more. The concept of planetary migration is explained in order to better understand the dramatic differences between bodies in the Inner and Outer Solar System. Finally, potential problems in the Solar System that may occur as a result of migration are addressed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/06%20-%20Microlensing%2C%20Astrometry%20and%20Other%20Methods.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/06%20-%20Microlensing%2C%20Astrometry%20and%20Other%20Methods.mp3" length="45554714" type="audio/mpeg"/>
      <itunes:duration>47:27</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Direct Imaging of Exoplanets</title>
      <description><![CDATA[Class begins with a problem on transits and learning what information astronomers obtain through observing them. For example, radii of stars can be estimated. Furthermore, applying the Doppler shift method, one can find the mass of a star. Finally, a star’s density can be calculated. A second method for identifying planets around stars is introduced: the astrometry method. The method allows for an extremely accurate assessment of a star’s precise position in the sky. Special features of the astrometry method are discussed and a number of problems are solved. A short summary is given on the three methods astronomers use to identify exoplanets. Class ends with an overview of upcoming space missions and the hope of detecting the presence of biological activity on other planet.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/07%20-%20Direct%20Imaging%20of%20Exoplanets.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/07%20-%20Direct%20Imaging%20of%20Exoplanets.mp3" length="45201956" type="audio/mpeg"/>
      <itunes:duration>47:05</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Introduction to Black Holes</title>
      <description><![CDATA[The second half of the course begins, focusing on black holes and relativity. In introducing black holes, Professor Bailyn offers a definition, talks about how their existence is detected, and explains why (unlike in the case with exoplanets where Newtonian physics was applied) Einstein’s Theory of Relativity is now required when studying black holes. The concepts of escape and circular velocity are introduced. A number of problems are worked out and students learn how to calculate an object’s escape velocity. A historical overview is offered of our understanding and discovery of black holes in the context of stellar evolution.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/08%20-%20Introduction%20to%20Black%20Holes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/08%20-%20Introduction%20to%20Black%20Holes.mp3" length="42290452" type="audio/mpeg"/>
      <itunes:duration>44:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Special and General Relativity</title>
      <description><![CDATA[The discussion of black holes continues with an introduction of the concept of event horizon. A number of problems are worked out to familiarize students with mathematics related to black hole event horizons. In a longer question and answer session, Professor Bailyn discusses the more mystifying aspects of the nature of black holes and the possibility of time travel. Finally, the issues of reconciling Newton’s laws of motion with Special Relativity, and Newton’s law of gravity with General Relativity, are addressed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/09%20-%20Special%20and%20General%20Relativity.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/09%20-%20Special%20and%20General%20Relativity.mp3" length="46077580" type="audio/mpeg"/>
      <itunes:duration>47:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Tests of Relativity</title>
      <description><![CDATA[The lecture begins with the development of post-Newtonian approximations from Newtonian terms. Several problems are worked out in calculating mass, force and energy. A discussion follows about how concepts like mass and velocity are approached differently in Newtonian physics and Relativity. Attention then turns to the discovery that space and time change near the speed of light, and how this realization affected Einstein’s theories. Finally, the possibility of traveling faster than the speed of light is addressed, including how physicists might predict from laboratory conditions how this might occur. Muons, unstable particles that form at the top of the Earth’s atmosphere, are used as an example.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/10%20-%20Tests%20of%20Relativity.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/10%20-%20Tests%20of%20Relativity.mp3" length="43043197" type="audio/mpeg"/>
      <itunes:duration>44:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Special and General Relativity (cont.)</title>
      <description><![CDATA[The lecture begins with a comprehensive overview of the historical conditions under which Einstein developed his theories. Of particular impact were the urgent need at the turn of the 19th century to synchronize clocks around the world; Einstein’s position at a patent office; and a series of experiments that he himself carried out. In 1905 Einstein published three papers that are still considered the greatest papers in the field of physics. The lecture then moves to General Relativity and how it encompasses Newton’s laws of gravity. A visual demonstration shows how space-time undergoes curvature when mass is introduced. Class ends with a question-and-answer period on a variety of topics in Special Relativity.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/11%20-%20Special%20and%20General%20Relativity%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/11%20-%20Special%20and%20General%20Relativity%20%28cont.%29.mp3" length="39345094" type="audio/mpeg"/>
      <itunes:duration>40:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Stellar Mass Black Holes</title>
      <description><![CDATA[One last key concept in Special Relativity is introduced before discussion turns again to black celestial bodies (black holes in particular) that manifest the relativistic effects students have learned about in the previous lectures. The new concept deals with describing events in a coordinate system of space and time. A mathematical explanation is given for how space and time reverse inside the Schwarzschild radius through sign changes in the metric. Evidence for General Relativity is offered from astronomical objects. The predicted presence and subsequent discovery of Neptune as proof of General Relativity are discussed, and stellar mass black holes are introduced.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/12%20-%20Stellar%20Mass%20Black%20Holes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/12%20-%20Stellar%20Mass%20Black%20Holes.mp3" length="47712637" type="audio/mpeg"/>
      <itunes:duration>49:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Stellar Mass Black Holes (cont.)</title>
      <description><![CDATA[Class begins with clarification of equations from the previous lecture. Four post-Newtonian gravitational effects are introduced and discussed in detail. The first of these is the so-called Perihelion Precession, which occurs when the major axis of a planet’s elliptical orbit precesses within its orbital plane, in response to changing gravitational forces exerted by other planets. Secondly, deflection of light is described as the curving of light as it passes near a large mass. Gravitational redshift is explained as a frequency effect that occurs as light moves away from a massive body such as a star or black hole. Finally, the existence and effects of gravitational waves are discussed. The lecture closes with a brief history of the 1919 eclipse expedition that made Einstein famous.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/13%20-%20Stellar%20Mass%20Black%20Holes%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/13%20-%20Stellar%20Mass%20Black%20Holes%20%28cont.%29.mp3" length="47066472" type="audio/mpeg"/>
      <itunes:duration>49:01</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Pulsars</title>
      <description><![CDATA[Professor Bailyn begins with a summary of the four post-Newtonian effects of general relativity that were introduced and explained last time: precession of the perihelion, the deflection of light, the gravitational redshift, and gravitational waves. The concept of gravitational lensing is discussed as predicted by Einstein’s general relativity theory. The formation of a gravitational lens can be observed when light from a bright distant source bends around a massive object between the source (such as a quasar) and the observer. Professor Bailyn then offers a slideshow of gravitational lenses. The issue of finding suitable astronomical objects that lend the opportunity to observe post-Newtonian relativistic effects is addressed. The lecture ends with Jocelyn Bell and the discovery of pulsars.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/14%20-%20Pulsars.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/14%20-%20Pulsars.mp3" length="46715386" type="audio/mpeg"/>
      <itunes:duration>48:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Supermassive Black Holes</title>
      <description><![CDATA[The lecture begins with a question-and-answer session about black holes. Topics include the extent to which we are sure black holes exist in the center of all galaxies, how massive they are, and how we can observe them. The lecture then turns to strong-field relativity: relativistic effects that are unrelated to Newtonian theory. The possibility of testing predictions of the existence of black holes is discussed in the context of strong-field relativity. One way we might learn about black holes is through observation of the orbit of the companion star in an X-ray binary star system. Through this we can estimate the mass of the compact object. The lecture ends with an explanation of how astronomers find black holes, and how Professor Bailyn was able to discover one himself.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/15%20-%20Supermassive%20Black%20Holes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/15%20-%20Supermassive%20Black%20Holes.mp3" length="45721061" type="audio/mpeg"/>
      <itunes:duration>47:37</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Hubble's Law and the Big Bang</title>
      <description><![CDATA[The third and final part of the course begins, consisting of a series of lectures on cosmology. A brief history of how cosmology developed into a scientific subject is offered. The discovery of dark energy, along with dark matter, played a crucial role in the development of cosmology. The lecture then discusses the discovery of spiral nebulae in 1920, as well as the “Great Debate” over what they were. Hubble’s famous redshift diagram is presented as the basis for Hubble’s Constant and Big Bang cosmology. The difficulty of measuring distance of objects in space, and how to do it using the parallax method and the standard candle method, are discussed. Measure brightness using the magnitude scale is explained. Class ends with a review of logarithms.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/16%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/16%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang.mp3" length="48778015" type="audio/mpeg"/>
      <itunes:duration>50:48</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: Hubble's Law and the Big Bang (cont.)</title>
      <description><![CDATA[Class begins with a review of magnitudes and the problem set involving magnitude equations. Implications of the Hubble Law and Hubble Diagram are discussed. Professor Bailyn elaborates on the Big Bang theory of cosmology and addresses controversial questions related to the age, development, and boundaries of the universe. The fate of the universe, and possibly its end (known as the Big Crunch) are addressed. Imagining an expanding three-dimensional universe is proposed. The lecture ends with a question-and-answer session during which students inquire about a variety of topics related to cosmology, such as the center of the universe, its current expansion, and hypothetical collapse.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/17%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/17%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang%20%28cont.%29.mp3" length="49772340" type="audio/mpeg"/>
      <itunes:duration>51:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Hubble's Law and the Big Bang (cont.)</title>
      <description><![CDATA[Professor Bailyn returns to the subject of the expansion of the universe to offer explanations that do not require belief in the Big Bang theory. One alternative is a theory that, in the past, the entire universe was reduced to an “initial singularity,” in which everything was much closer, and therefore denser and hotter. Since the universe is in constant flux, however, it follows that in the future things will drift apart. The Steady State explanation for the expansion of the universe is then explained. Coined as a derogatory term meant to ridicule supporters of the Big Bang theory, Steady State purports that new energy and matter are constantly created as the universe expands, to fill in the void that results from the expansion. The discovery of quasars refuted the Steady State theory. The lecture ends with a discussion of how observing very distant objects allows us to look back in time, and also gives us a glimpse into the future of galaxies and the universe.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/18%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/18%20-%20Hubble%27s%20Law%20and%20the%20Big%20Bang%20%28cont.%29.mp3" length="48202903" type="audio/mpeg"/>
      <itunes:duration>50:12</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Omega and the End of the Universe</title>
      <description><![CDATA[Class begins with a review of the issues previously addressed about the origin and fate of the universe. The role of gravity in the expansion of the universe is discussed and given as the reason why the rate of expansion cannot remain constant and will eventually slow down. The actual density of the universe is calculated using various methods. Finally, the unsolved problem of dark matter is addressed and two explanatory hypotheses are proposed. One is that the universe is comprised of WIMPs (Weakly Interactive Massive Particles) that fulfill two requirements: they have mass and do not interact with light. The second hypothesis is that dark matter is made of MACHOs (Massive Astrophysical Compact Halo Objects), which scientists have attempted to identify through gravitational lenses.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/19%20-%20Omega%20and%20the%20End%20of%20the%20Universe.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/19%20-%20Omega%20and%20the%20End%20of%20the%20Universe.mp3" length="47196039" type="audio/mpeg"/>
      <itunes:duration>49:09</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Dark Matter</title>
      <description><![CDATA[This lecture introduces an important concept related to the past and future of the universe: the Scale factor, which is a function of time. With reference to a graph whose coordinates are the Scale factor and time, the problem of dark matter is addressed again. Cosmological redshifts are measured to determine the scale of the universe. The discovery of the repulsive, anti-gravitational force of dark energy is explained. The lecture concludes with discussion of Einstein’s biggest mistake: the invention of the cosmological constant to balance gravity.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/20%20-%20Dark%20Matter.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/20%20-%20Dark%20Matter.mp3" length="47629463" type="audio/mpeg"/>
      <itunes:duration>49:36</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Dark Energy and the Accelerating Universe and the Big Rip</title>
      <description><![CDATA[Class begins with a review of the mysterious nature of dark matter, which accounts for three quarters of the universe. Different models of the universe are graphed. The nature, frequency, and duration of supernovae are then addressed. Professor Bailyn presents data from the Supernova Cosmology Project and pictures of supernovae taken by the Hubble Space Telescope. The discovery of dark energy is revisited and the density of dark energy is calculated. The Big Rip is presented as an alternative hypothesis for the fate of the universe.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/21%20-%20Dark%20Energy%20and%20the%20Accelerating%20Universe%20and%20the%20Big%20Rip.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/21%20-%20Dark%20Energy%20and%20the%20Accelerating%20Universe%20and%20the%20Big%20Rip.mp3" length="43961871" type="audio/mpeg"/>
      <itunes:duration>45:47</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Supernovae</title>
      <description><![CDATA[Professor Bailyn offers a review of what is known so far about the expansion of the universe from observing galaxies, supernovae, and other celestial phenomena. The rate of the expansion of the universe is discussed along with the Big Rip theory and the balance of dark energy and dark matter in the universe over time. The point at which the universe shifts from accelerating to decelerating is examined. Worries related to the brightness of high redshift supernovae and the effects of gravitational lensing are explained. The lecture also describes current project designs for detecting supernovae at high or intermediate redshift, such as the Joint Dark Energy Mission (JDEM) and Large Synoptic Survey Telescope (LSST).]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/22%20-%20Supernovae.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/22%20-%20Supernovae.mp3" length="45015128" type="audio/mpeg"/>
      <itunes:duration>46:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Other Constraints: The Cosmic Microwave Background</title>
      <description><![CDATA[Reasons for the expansion of the universe are addressed at the start of this lecture, focusing especially on the acceleration of dark energy. Supernovae were the first evidence for the existence of dark energy. Two other proofs are presented. The first is the Cosmic Microwave Background, which is a form of electromagnetic radiation that is perfectly smooth and equal in all directions. It firmly supports the Big Bang theory. Projects attempting to measure it, such as COBE and WMAP, are discussed. Secondly, Large-Scale Clustering is introduced: by measuring the degree of clustering, astronomers hope to advance their understanding of dark energy and dark matter. Computer simulations of the evolution of the universe are shown.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/23%20-%20Other%20Constraints_%20The%20Cosmic%20Microwave%20Background.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/23%20-%20Other%20Constraints_%20The%20Cosmic%20Microwave%20Background.mp3" length="45244170" type="audio/mpeg"/>
      <itunes:duration>47:07</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: The Multiverse and Theories of Everything</title>
      <description><![CDATA[Professor Bailyn begins the class with a discussion of a recent New York Times article about the discovery of a new, earth-like planet. He then discusses concepts such as epicycles, dark energy and dark matter; imaginary ideas invented to explain 96% of the universe. The Anthropic Principle is introduced and the possibility of the multiverse is addressed. Finally, biological arguments are put forth for how complexity occurs on a cosmological scale. The lecture and course conclude with a discussion on the fine differences between science and philosophy.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/24%20-%20The%20Multiverse%20and%20Theories%20of%20Everything.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Frontiers_and_Controversies_in_Astrophysics/24%20-%20The%20Multiverse%20and%20Theories%20of%20Everything.mp3" length="44893920" type="audio/mpeg"/>
      <itunes:duration>46:45</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/astr-160-2007/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
  </channel>
</rss>
