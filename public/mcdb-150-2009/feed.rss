<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>MCDB 150: Global Problems of Population Growth</title>
    <link>https://oyc.yale.edu/molecular-cellular-and-developmental-biology/mcdb-150</link>
    <description>From Open Yale Courses. MCDB 150, Spring 2009. This survey course introduces students to the important and basic material on human fertility, population growth, the demographic transition and population policy. Topics include: the human and environmental dimensions of population pressure, demographic history, economic and cultural causes of demographic change, environmental carrying capacity and sustainability. Political, religious and ethical issues surrounding fertility are also addressed. The lectures and readings attempt to balance theoretical and demographic scale analyzes with studies of individual humans and communities. The perspective is global with both developed and developing countries included.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:14 +0000</lastBuildDate>
    <itunes:author>Robert Wyman and Yale University</itunes:author>
    <dc:creator>Robert Wyman</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/mcdb-150-2009/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Evolution of Sex and Reproductive Strategies</title>
      <description><![CDATA[Reproduction is not simple or easy, nor is it fair. Females often bear a larger reproductive burden of child bearing and child rearing. Reproductive strategies can be simplified into two primary strategies for males and two for females: males often either engage in sperm competition or physical competition while females strategize to get resources from males, or to find the best male genes for the offspring. Rape and violence, as reproductive strategies, occur in few species, but violence is especially prevalent among the great apes, probably because eggs are so scarce in these species. Among orangutans, rape is common. For gorillas, infanticide is a common form of reproductive violence, and male chimpanzees regularly fight each other and batter females.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/01%20-%20Evolution%20of%20Sex%20and%20Reproductive%20Strategies.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/01%20-%20Evolution%20of%20Sex%20and%20Reproductive%20Strategies.mp3" length="63821620" type="audio/mpeg"/>
      <itunes:duration>01:06:28</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Sex and Violence Among the Apes</title>
      <description><![CDATA[Chimpanzee males compete for position in a dominance hierarchy; status often depends on support from other members, including females, of the group. High-ranking males have much greater sexual access to females in estrus. Males control females by physical violence and intimidation. Chimpanzees also engage in purposeful raids to kill members of other chimpanzee groups. This inter-group violence can help explain intra-group violence. To fend off attack from other groups, males must remain in groups and that requires males to compete for mating opportunities within the community. Competition for the scarce resource, eggs, leads to male-male violence and male coercion of females. If the alpha male monopolized all reproductive potential, then evolution would push non-dominant males to either fight continually for dominance or to leave the group and find females elsewhere. The chimpanzee solution is to allow all males some, though very unequal, reproductive possibility.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/02%20-%20Sex%20and%20Violence%20Among%20the%20Apes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/02%20-%20Sex%20and%20Violence%20Among%20the%20Apes.mp3" length="69979413" type="audio/mpeg"/>
      <itunes:duration>01:12:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: From Ape to Human</title>
      <description><![CDATA[Throughout prehistoric, written, and recent history, human warfare has been commonplace. Nearly all societies engage in regular or periodic war. In many examples, human warfare has characteristics similar to chimpanzee war: an in-group fights with and kills members of the out-group. This information is not to be misinterpreted as either justifying human violence or considering it inevitable. When it comes to births and fecundity, though, humans are very different from the other great apes. Chimpanzees reproduce once every five to eight years; humans can give birth again within 18 months. It is likely that an increase in male contribution to child rearing allowed this greater fecundity.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/03%20-%20From%20Ape%20to%20Human.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/03%20-%20From%20Ape%20to%20Human.mp3" length="70457141" type="audio/mpeg"/>
      <itunes:duration>01:13:23</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: When Humans Were Scarce</title>
      <description><![CDATA[Hunter-gatherer populations were much less dense than later agriculturalists. The variety of their food supply protected them from crop failures and their sparseness reduced the spread of infectious diseases. Hunter-gatherers were healthier and worked less than early agriculturalists. Why didn’t their numbers increase up to the same level of Malthusian misery? Their numbers may have been limited by violence between groups. Agriculture is more work intense and offers a less varied diet. Populations seem to grow rapidly and then die out suddenly. Populations are subject to climatic- or disease-caused crop failure. But farming allows individuals to produce a surplus of food that can then be stolen by warrior tribes or military castes. The surplus allows for population growth, cities and stratified societies. The death rate, until perhaps the 1700s in Europe, is enormously high: only approximately a third of women survive to the end of their reproductive period. At this death rate, surviving women who are able to reproduce must have more than six children on average or the society goes extinct. All the great religions and cultures develop in this long period and all stress the requirement for high reproductive rates: “Be fruitful and multiply.”]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/04%20-%20When%20Humans%20Were%20Scarce.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/04%20-%20When%20Humans%20Were%20Scarce.mp3" length="65785611" type="audio/mpeg"/>
      <itunes:duration>01:08:31</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Why Is Africa Different?</title>
      <description><![CDATA[In addition to cultural controls acting to maximize fertility, there are important, and often competing, interests of individual families to limit fertility. Unwanted births are dealt with by infanticide in many cultures. Additionally, fertility is regularly controlled by limiting marriage within a culture. Another very important factor in population growth, especially in the tropics, is food availability. Heavy rains in the tropics wash nutrients away, leaving deficient soils. Much of Africa is either too dry or too wet. Africa was, until recently, not densely populated. Since land was available and because more children meant more security and power, a culture evolved that emphasized high fertility, justified by the need for descendants to pacify ancestors. Sub-Saharan (tropical) Africa has the highest birth rates in the world. As an example, Niger, just south of the Sahara desert has a fertility rate of almost eight children per woman while, in the Mediterranean zone, Morocco, just north of the Sahara, but also a Sunni Muslim country, has a rate of only 3.3 children per woman.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/05%20-%20Why%20Is%20Africa%20Different_.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/05%20-%20Why%20Is%20Africa%20Different_.mp3" length="68456370" type="audio/mpeg"/>
      <itunes:duration>01:11:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Malthusian Times</title>
      <description><![CDATA[In many regions, the central cultural idea is that of a lineage, a family and its line of male ancestors and descendants. The prime duty in these cultures is to keep the lineage going. Religion is small scale with the ancestors performing many of the functions of gods. Denser populations and larger political entities lead to large-scale religion where conformity is stressed and cultural rules are codified in a book and not subject to discussion with the ancestors. In pre-modern Sub-Saharan Africa, land was not limiting, so a maximum number of children was desired. Neither monogamy nor chastity were valued as much as fertility. Families were not nuclear; husbands and wives did not engage in many activities together; children were often raised by other members of the village and women had the responsibility for economic support of the children. In many areas of Sub-Saharan Africa, farming is the work of women. Women often prefer men with resources which leads to polygamy. Women in polygamous relationships form support groups for each other and men enjoy the fruits of several women’s labor and children. In temperate regions, the land eventually fills up and the dangers of overpopulation come to the fore. Peasants are miserably poor. Massive epidemics (the Black Death, 1347 and onward) and wars (the Catholic-Protestant wars, 1562-1648) can kill a third of the population.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/06%20-%20Malthusian%20Times.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/06%20-%20Malthusian%20Times.mp3" length="70424958" type="audio/mpeg"/>
      <itunes:duration>01:13:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Demographic Transition in Europe; Mortality Decline</title>
      <description><![CDATA[European populations grew only slowly during the period 1200-1700; factors include disease and wars. Human feces and rotting animal remains were not sequestered and often contaminated drinking water. Cities were so filthy that more people died in them than were born. About a third of children died in infancy, many from abandonment and lack of care during wet-nursing. Children that survived were subjected to harsh discipline to control their tendency to sin. Ineffective and even harmful treatments, like blood-letting, were all that medicine could offer. Starting with Newton’s Principia(1687) and the Enlightenment (eighteenth century), scientific attitudes began replacing religious ones: the biological and physical world became objects of study. Sanitation, hygiene and public health improved. Inoculation and vaccination were developed. The Industrial Revolution began. As death rates fell, population rose. While most believe that an increasing population is good, Malthus worries that population can grow faster than the food supply, trapping people in subsistence misery.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/07%20-%20Demographic%20Transition%20in%20Europe%3B%20Mortality%20Decline.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/07%20-%20Demographic%20Transition%20in%20Europe%3B%20Mortality%20Decline.mp3" length="66147145" type="audio/mpeg"/>
      <itunes:duration>01:08:54</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Demographic Transition in Europe; Fertility Decline</title>
      <description><![CDATA[Prior to Malthus, population growth was seen as good for the power and wealth of a country. The rapid population growth of America was crucial in expelling England (via the Revolution) and France (via the Louisiana Purchase) from the US. But in fact, the numbers of the poor were growing in Europe in the 1700s. Malthus argued that poverty was due to an imbalance between people and resources; since population could rise very fast, it could always outstrip any gains in productivity. He did not anticipate an exponential increase in production or a voluntary decrease in fertility. However, Malthus’ thinking is still important because high population levels and environmental limitations are in fact problematic today. By the eighteenth and nineteenth centuries, mortality was falling in Europe and this caused a population explosion. The productivity gains of the Industrial Revolution were nearly balanced by the increased population; per capita income of the working classes was not much improved. Fertility didn’t drop until late in the nineteenth century; per capita income started to grow rapidly. The reason for the fertility decline is not well explained by declining mortality or rising standard of living or any other socioeconomic factor. The mortality and later fertility drop is called the Demographic Transition. The extension of lifespan and the freedom from continual childbearing and child rearing is one of the most important changes ever in what it means to be a human.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/08%20-%20Demographic%20Transition%20in%20Europe%3B%20Fertility%20Decline.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/08%20-%20Demographic%20Transition%20in%20Europe%3B%20Fertility%20Decline.mp3" length="68720102" type="audio/mpeg"/>
      <itunes:duration>01:11:35</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Demographic Transition in Europe</title>
      <description><![CDATA[Prior to the Demographic Transition, fertility in northwestern Europe was controlled by limiting marriage. Marriage was regulated by landowners and the churches, and was not allowed unless a man had accumulated the resources necessary to support a family. Long periods of being landless, a servant, or an apprentice, precluded marriage. Once married, there was no control of fertility. But, only about half of adults were married at any given time, so fertility was about half of what it might have been. Eventually, contraception was accepted and fertility within marriage fell. Society no longer needed to control marriage so tightly and marriage rates rose dramatically. The options of marriage, sex and childbearing passed from community control to individual control. The fertility decline occurred very rapidly in Europe, mostly between 1870 and 1930. It has been difficult to prove a socioeconomic basis for the decline. The largest study, The Princeton European Fertility Project, argued that cultural transmission of new social norms was crucial. The Demographic Transition encompassed a ten-fold increase in population and a three-fold increase in life expectancy. It drastically changed the human experience of life.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/09%20-%20Demographic%20Transition%20in%20Europe.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/09%20-%20Demographic%20Transition%20in%20Europe.mp3" length="63794035" type="audio/mpeg"/>
      <itunes:duration>01:06:27</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Quantitative Aspects</title>
      <description><![CDATA[Census data is often politically influenced and hence inaccurate. The birthrate in developing countries is nearly twice that in developed countries. Most humans live in less developed countries, so the world birthrate is near the higher number. The world birthrate is two and a half times the death rate; we are not close to population stabilization. Almost everywhere, the death rate has been drastically reduced; further changes will not massively affect demographic trends. Changes in fertility rate now control population. Demographic data must be corrected for age structure. A young population in a poor country will have a lower death rate than an older population in a richer country. Countries with high birthrates and exploding populations will have a high proportion of children. There are more people in each younger age bracket than in older ones. Many more adolescents will come into reproductive ages than older women will leave fertile ages. Fertility per woman is falling in the world, but, since there are ever more childbearers, the number of children born does not drop. Because of this ‘momentum,’ it can take over 100 years from when fertility falls to replacement level (approximately 2 children per woman) to when population stabilizes. In developing countries, even though fertility has been reduced, population growth often outstrips economic growth. People may give up on modernization and instead, idealize a return to some imagined past that was glorious.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/10%20-%20Quantitative%20Aspects.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/10%20-%20Quantitative%20Aspects.mp3" length="59865219" type="audio/mpeg"/>
      <itunes:duration>01:02:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Low Fertility in Developed Countries (Guest Lecture by Michael Teitelbaum)</title>
      <description><![CDATA[Concerns about low fertility have been present in many countries for at least 100 years. A large population was considered essential to national power. But the issue is never simply a shortage of warm bodies: overall the world population has increased dramatically over this period and untold numbers would immigrate, if allowed. The issue is the number of the ‘right sort’ of people, defined as those having preferred national, religious, racial, ethnic, or language characteristics. Fertility levels are below replacement in many economically advanced countries. As a result, these countries are aging; medical and retirement costs are increasing. Countries must either raise fertility, accept immigrants, or adapt to a smaller, older population. Policies to raise fertility have not been very effective, except in severe dictatorships. To keep the ratio of working age people to dependents constant, hundreds of millions of immigrants would be required such that 70-80% of the population of receiving countries would be immigrants and their children. Adaptation is probably best, but the required changes (raise retirement age, tax the pension benefits of the wealthy, etc.) are politically difficult.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/11%20-%20%20Low%20Fertility%20in%20Developed%20Countries%20%28Guest%20Lecture%20by%20Michael%20Teitelbaum%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/11%20-%20%20Low%20Fertility%20in%20Developed%20Countries%20%28Guest%20Lecture%20by%20Michael%20Teitelbaum%29.mp3" length="69307335" type="audio/mpeg"/>
      <itunes:duration>01:12:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Human and Environmental Impacts</title>
      <description><![CDATA[Until recently, the world population has been growing faster than exponentially. Although the growth rate has slowed somewhat, there are about 80 million more people each year and about 3 billion more will be added by 2050 (a 50% increase). Population will probably increase more beyond that. Such growth is unprecedented and we cannot predict its long-term effects. The environmental impact of this population increase is bound to be astronomic. Large populations engender two problems: over-consumption in the rich countries which leads to environmental misery, and under-consumption in the poor countries which leads to human misery. People living in abject poverty ($1 per day) don’t limit their fertility. Factory jobs in poor countries pay double that, approximately $2 per day. For population to stabilize, income must rise. If population is to increase by 50%, income needs to double–we are looking at a tripling of the world economy. The environment is currently overstressed. Can it survive a tripling of the economy?]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/12%20-%20Human%20and%20Environmental%20Impacts.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/12%20-%20Human%20and%20Environmental%20Impacts.mp3" length="68338923" type="audio/mpeg"/>
      <itunes:duration>01:11:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Fertility Attitudes and Practices</title>
      <description><![CDATA[Surveys show that most women are having more children than they would prefer to have. Further, studies show that the vast majority of women know about various forms of contraception. One World Bank study has shown that family planning programs have little impact unless they are attended by improved living standards and increasing status of women.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/13%20-%20Fertility%20Attitudes%20and%20Practices.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/13%20-%20Fertility%20Attitudes%20and%20Practices.mp3" length="28371157" type="audio/mpeg"/>
      <itunes:duration>29:33</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Demographic Transition in Developing Countries</title>
      <description><![CDATA[By 1950, in most of the underdeveloped world, mortality had fallen to about half its pre-modern rate. The birth rate, however, had remained high and, by 1950, was about twice the death rate. For the rest of the century, both rates fell dramatically and in parallel, maintaining the gap. The enormous excess of births over deaths in this period is known as ‘the population explosion.’ By 1990, the world population was growing at almost 90 million a year. Comparing the Demographic Transition in Europe and in the currently developing countries, the latter started 100 years later at a much lower economic level, fell from much higher birth and death rates, occurred much faster and with a much higher population growth rate, and added vastly more people. The developing countries saw the benefits that had accrued to the West as a result of the transition and then rapidly appropriated it for themselves. But while European countries may have quadrupled their population over 200 years, third world countries grew by as much as ten times in a much shorter period and they are still growing at a rapid rate. The problems of this rapid growth (still about 80 million a year) abound. The traditional scourges of starvation (9 million deaths a year), disease (AIDS, Tuberculosis, Malaria – all claim between 1 and 2 million deaths per year) and war (Hiroshima and Nagasaki atomic bombs with approximately 200,000 deaths) are all far too small to stabilize population. People in developing countries who want to limit their fertility, are often afraid of contraceptives (especially side-effects) and yet are willing to undergo horrendously dangerous illegal abortions to avert a childbirth.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/14%20-%20Demographic%20Transition%20in%20Developing%20Countries.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/14%20-%20Demographic%20Transition%20in%20Developing%20Countries.mp3" length="67769245" type="audio/mpeg"/>
      <itunes:duration>01:10:35</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Female Disadvantage</title>
      <description><![CDATA[In East and South Asia there are many more boys than girls. Previously, this resulted from female infanticide, now it is sex-selective abortion. In those cultures, girls generally marry out of the family as teenagers and thus provide no benefit for the family that raised them. Bangladesh is agriculturally very rich, but its population is so dense that per capita income is one of the lowest in Asia. Despite the poverty, an excellent family planning program has greatly reduced fertility.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/15%20-%20Female%20Disadvantage.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/15%20-%20Female%20Disadvantage.mp3" length="71388772" type="audio/mpeg"/>
      <itunes:duration>01:14:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Population in Traditional China</title>
      <description><![CDATA[China’s early demographic history is similar to that of Europe; population grows only slowly due to war, disease and Malthusian resource limitation. Later, introduction of American foods allowed cultivated land to expand, but population expanded even more rapidly, leading to an extremely dense, but poor population. During this time, female infanticide was frequent, but almost all surviving girls got married. Within marriage, their fertility rate was much lower than that of their European counterparts. This system compares to the English with a low rate of marriage, but high fertility within marriage.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/16%20-%20Population%20in%20Traditional%20China.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/16%20-%20Population%20in%20Traditional%20China.mp3" length="34550684" type="audio/mpeg"/>
      <itunes:duration>35:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: Population in Modern China</title>
      <description><![CDATA[Families lived together in traditional China and sons remained on the land; division of family land led to tiny plots and rural poverty. Because labor was so cheap, the country did not urbanize or mechanize. The Communist government started out with a pro-natal stance, but after experiencing the famine of the Great Leap Forward, moved strongly to fertility control. Fertility declined rapidly in the 1970s, but to counter momentum, the One-Child Policy was introduced in 1979-80. Nevertheless, population has now risen to over 1.3 billion.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/17%20-%20Population%20in%20Modern%20China.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/17%20-%20Population%20in%20Modern%20China.mp3" length="63177127" type="audio/mpeg"/>
      <itunes:duration>01:05:48</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Economic Impact of Population Growth</title>
      <description><![CDATA[1) Population in China: Until recently, Chinese families did not much alter their fertility depending on life events such as deaths of children. However, under government prodding and eventually coercion, fertility dropped drastically in China in the 1970s, but to counteract momentum, the One-Child Policy started in 1979-80. 2) Population Growth and Economic Development: In Asia, rapid fertility drops have preceded economic booms by approximately fifteen years. In this time, children grow up and become workers. With many workers and fewer children to support, savings and investments rise causing the boom. Non-Asian countries with rapid fertility drops, like Ireland, fit this model. Sub-Saharan Africa, with still high fertility, makes little economic progress.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/18%20-%20Economic%20Impact%20of%20Population%20Growth.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/18%20-%20Economic%20Impact%20of%20Population%20Growth.mp3" length="68586355" type="audio/mpeg"/>
      <itunes:duration>01:11:26</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Economic Motivations for Fertility</title>
      <description><![CDATA[Data shows, consistently, that poor people have more children than rich people; economically speaking, children are an inferior good. Children are production goods because they do work, consumption goods because they are enjoyable, and investment goods because they support parents in old age. Jobs in the modern sector require education and health. To pay for this, parents have to focus their resources on fewer children.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/19%20-%20Economic%20Motivations%20for%20Fertility.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/19%20-%20Economic%20Motivations%20for%20Fertility.mp3" length="71065689" type="audio/mpeg"/>
      <itunes:duration>01:14:01</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Teen Sexuality and Teen Pregnancy</title>
      <description><![CDATA[Rates of teen pregnancy in the US are quite high, in contrast to European countries which have much lower rates, especially those with liberal attitudes toward sexuality. Traditionally, puberty and marriage were simultaneous. Now, the many years spent in education leaves a long time between those life stages. Sex education is not particularly strong. Contraception has allowed the rate of teen pregnancy to decrease steadily in spite of the fact that teen sex is consistently increasing. Non-marital childbearing is high in all industrialized countries.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/20%20-%20Teen%20Sexuality%20and%20Teen%20Pregnancy.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/20%20-%20Teen%20Sexuality%20and%20Teen%20Pregnancy.mp3" length="71768279" type="audio/mpeg"/>
      <itunes:duration>01:14:45</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Global Demography of Abortion</title>
      <description><![CDATA[Abortion is very common both worldwide and in the US. There is one abortion for every 3.2 live births. In places where contraception is not used, abortion is used as birth control. Neither legal nor religious proscriptions have a strong effect on abortion rates. In countries where abortion is illegal, maternal death rates are extraordinarily high. Legal, medically done abortions are safer than getting pregnant. Psychological responses depend on the individual and the culture.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/21%20-%20Global%20Demography%20of%20Abortion.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/21%20-%20Global%20Demography%20of%20Abortion.mp3" length="69115909" type="audio/mpeg"/>
      <itunes:duration>01:11:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Media and the Fertility Transition in Developing Countries (Guest Lecture by William Ryerson)</title>
      <description><![CDATA[Guest lecturer William Ryerson is President of the Population Media Center which produces radio and TV serial dramas in developing countries that aim to effect behavior change on women’s status, family planning and AIDS. Working with governments and in-country media professionals, these melodramas run for hundreds of episodes and are watched by millions. Careful research shows major changes in audience knowledge, attitudes and practices.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/22%20-%20Media%20and%20the%20Fertility%20Transition%20in%20Developing%20Countries%20%28Guest%20Lecture%20by%20William%20Ryerson%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/22%20-%20Media%20and%20the%20Fertility%20Transition%20in%20Developing%20Countries%20%28Guest%20Lecture%20by%20William%20Ryerson%29.mp3" length="73637392" type="audio/mpeg"/>
      <itunes:duration>01:16:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Biology and History of Abortion</title>
      <description><![CDATA[The idea that “life begins at conception” is not a scientific one. Since the disproof of ‘spontaneous generation’ (1668-1859), we have known that life only derives from life. Life arose billions of years ago and has continued since as a cycle. Assigning a beginning to a cycle (like the year) is arbitrary. The Bible describes the cycle as “Dust to Dust.” Exodus describes a forced abortion as a property crime, but taking the life of the mother as a capital crime. The New Testament does not refer to abortion.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/23%20-%20Biology%20and%20History%20of%20Abortion.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/23%20-%20Biology%20and%20History%20of%20Abortion.mp3" length="70668628" type="audio/mpeg"/>
      <itunes:duration>01:13:36</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Population and the Environment</title>
      <description><![CDATA[World population will continue to rise until at least 2050. Environmental impact is the product of the number of people and how much of their income and technology is devoted to either consumption or conservation. So far, the balance is far at the consumption end and, globally, environmental problems are increasing. Environmentalism has not come close to counteracting the footprint of a billion extra people every dozen years. The only massive success has been the decline in global fertility. People want fewer children, the contraceptive technology is available, and the cost is minimal. The only realistic possibility for ameliorating the environmental crisis might be to facilitate the continued decline of fertility.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Global_Problems_of_Population_Growth/24%20-%20Population%20and%20the%20Environment.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Global_Problems_of_Population_Growth/24%20-%20Population%20and%20the%20Environment.mp3" length="72130231" type="audio/mpeg"/>
      <itunes:duration>01:15:08</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/mcdb-150-2009/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
  </channel>
</rss>
