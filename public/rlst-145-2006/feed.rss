<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>RLST 145: Introduction to the Old Testament (Hebrew Bible)</title>
    <link>https://oyc.yale.edu/religious-studies/rlst-145</link>
    <description>From Open Yale Courses. RLST 145, Fall 2006. This course examines the Old Testament (Hebrew Bible) as an expression of the religious life and thought of ancient Israel, and a foundational document of Western civilization. A wide range of methodologies, including source criticism and the historical-critical school, tradition criticism, redaction criticism, and literary and canonical approaches are applied to the study and interpretation of the Bible. Special emphasis is placed on the Bible against the backdrop of its historical and cultural setting in the Ancient Near East.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:20 +0000</lastBuildDate>
    <itunes:author>Christine Hayes and Yale University</itunes:author>
    <dc:creator>Christine Hayes</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/rlst-145-2006/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: The Parts of the Whole</title>
      <description><![CDATA[This lecture provides an introduction to the literature of the Hebrew Bible and its structure and contents. Common misconceptions about the Bible are dispelled: the Bible is a library of books from diverse times and places rather than a single, unified book; biblical narratives contain complex themes and realistic characters and are not “pious parables” about saintly persons; the Bible is a literarily sophisticated narrative not for children; the Bible is an account of the odyssey of a people rather than a book of theology; and finally, the Bible was written by many human contributors with diverse perspectives and viewpoints.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/01%20-%20The%20Parts%20of%20the%20Whole.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/01%20-%20The%20Parts%20of%20the%20Whole.mp3" length="43931778" type="audio/mpeg"/>
      <itunes:duration>45:45</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: The Hebrew Bible in Its Ancient Near Eastern Setting: Biblical Religion in Context</title>
      <description><![CDATA[In this lecture, the Hebrew Bible is understood against the background of Ancient Near Eastern culture. Drawing from and critiquing the work of Yehezkel Kaufmann, the lecture compares the religion of the Hebrew Bible with the cultures of the Ancient Near East. Two models of development are discussed: an evolutionary model of development in which the Hebrew Bible is continuous with Ancient Near Eastern culture and a revolutionary model of development in which the Israelite religion is radically discontinuous with Ancient Near Eastern culture. At stake in this debate is whether the religion of the Hebrew Bible is really the religion of ancient Israel.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/02%20-%20The%20Hebrew%20Bible%20in%20its%20Ancient%20Near%20Eastern%20Setting_%20%20Biblical%20Religion%20in%20Context.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/02%20-%20The%20Hebrew%20Bible%20in%20its%20Ancient%20Near%20Eastern%20Setting_%20%20Biblical%20Religion%20in%20Context.mp3" length="44347229" type="audio/mpeg"/>
      <itunes:duration>46:11</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: The Hebrew Bible in Its Ancient Near Eastern Setting: Genesis 1-4 in Context</title>
      <description><![CDATA[In the first of a series of lectures on the book of Genesis, the basic elements of biblical monotheism are compared with Ancient Near Eastern texts to show a non-mythological, non-theogonic conception of the deity, a new conception of the purpose and meaning of human life, nature, magic and myth, sin and evil, ethics (including the universal moral law) and history. The two creation stories are explored and the work of Nahum Sarna is introduced.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/03%20-%20The%20Hebrew%20Bible%20in%20its%20Ancient%20Near%20Eastern%20Setting_%20Genesis%201-4%20in%20Context.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/03%20-%20The%20Hebrew%20Bible%20in%20its%20Ancient%20Near%20Eastern%20Setting_%20Genesis%201-4%20in%20Context.mp3" length="45808833" type="audio/mpeg"/>
      <itunes:duration>47:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Doublets and Contradictions, Seams and Sources: Genesis 5-11 and the Historical-Critical Method</title>
      <description><![CDATA[This lecture continues the discussion on Genesis, including the familiar accounts of Cain and Abel, the Flood and Noahide covenant. The story of Cain and Abel expresses the notion of the God-endowed sanctity of human life and a “universal moral law” governing the world. Examination of the contradictions and doublets in the flood story leads to a discussion of the complex composition and authorship of the Pentateuch. These features as well as anachronisms challenge traditional religious convictions of Moses as the author of the first five books of the Bible.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/04%20-%20Doublets%20and%20Contradictions%2C%20Seams%20and%20Sources_%20Genesis%205-11%20and%20the%20historical-critical%20method.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/04%20-%20Doublets%20and%20Contradictions%2C%20Seams%20and%20Sources_%20Genesis%205-11%20and%20the%20historical-critical%20method.mp3" length="45958880" type="audio/mpeg"/>
      <itunes:duration>47:52</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Critical Approaches to the Bible: Introduction to Genesis 12-50</title>
      <description><![CDATA[This lecture introduces the modern critical study of the Bible, including source theories and Wellhausen’s Documentary Hypothesis, as well as form criticism and tradition criticism. The main characteristics of each biblical source (J, E, P, and D) according to classic source theory are explained. This lecture also raises the question of the historical accuracy of the Bible and the relation of archaeology to the biblical record.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/05%20-%20Critical%20Approaches%20to%20the%20Bible_%20%20Introduction%20to%20Genesis%2012-50.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/05%20-%20Critical%20Approaches%20to%20the%20Bible_%20%20Introduction%20to%20Genesis%2012-50.mp3" length="46787693" type="audio/mpeg"/>
      <itunes:duration>48:44</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Biblical Narrative: The Stories of the Patriarchs (Genesis 12-36)</title>
      <description><![CDATA[This lecture continues with a review of scholarly views on the historical accuracy of the Bible. The narratives of the patriarchs and matriarchs are introduced and the covenant between Abraham and God–which ultimately leads to the formation of a nation–is explained. Central themes of the patriarchal stories include: God’s call to Abraham, God’s promise of a blessed and fruitful nation, threats to this promise (including the story of the binding of Isaac for sacrifice). Finally, after a significant character transformation, the third patriarch Jacob becomes Yisrael (“he who struggles with God”).]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/06%20-%20Biblical%20Narrative_%20The%20Stories%20of%20the%20Patriarchs%20%28Genesis%2012-36%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/06%20-%20Biblical%20Narrative_%20The%20Stories%20of%20the%20Patriarchs%20%28Genesis%2012-36%29.mp3" length="47305963" type="audio/mpeg"/>
      <itunes:duration>49:16</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Israel in Egypt: Moses and the Beginning of Yahwism (Genesis 37- Exodus 4)</title>
      <description><![CDATA[The book of Genesis concludes with the story of Joseph and the descent of the 12 tribes into Egypt, setting the stage for the Exodus in which God is seen as redeemer and liberator. Moses is the first in a line of apostolic (messenger) prophets and Yahwism is initiated. Mark Smith’s thesis describing the emergence of Israelite religion through a process of convergence and divergence is presented as an alternative to the evolutionary-revolutionary dichotomy presented in Lecture 2.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/07%20-%20Israel%20in%20Egypt_%20%20Moses%20and%20the%20Beginning%20of%20Yahwism%20%28Genesis%2037-%20Exodus%204%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/07%20-%20Israel%20in%20Egypt_%20%20Moses%20and%20the%20Beginning%20of%20Yahwism%20%28Genesis%2037-%20Exodus%204%29.mp3" length="44205123" type="audio/mpeg"/>
      <itunes:duration>46:02</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Exodus: From Egypt to Sinai (Exodus 5-24, 32; Numbers)</title>
      <description><![CDATA[This lecture traces the account of the Exodus (and the origin of the Passover festival as a historicization of older nature festivals) and Israel’s liberation from bondage under Pharaoh. The story reaches its climax with the covenant concluded between God and Israel through Moses at Sinai. Drawing heavily on the work of Jon Levenson, the lecture examines Ancient Near Eastern parallels to the Sinaitic covenant and describes the divine-human relationship (an intersection of law and love) that the covenant seeks to express.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/08%20-%20Exodus_%20%20From%20Egypt%20to%20Sinai%20%28Exodus%205-24%2C%2032%3B%20Numbers%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/08%20-%20Exodus_%20%20From%20Egypt%20to%20Sinai%20%28Exodus%205-24%2C%2032%3B%20Numbers%29.mp3" length="45673414" type="audio/mpeg"/>
      <itunes:duration>47:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: The Priestly Legacy: Cult and Sacrifice, Purity and Holiness in Leviticus and Numbers</title>
      <description><![CDATA[In this lecture, the Priestly source (P) found primarily in Leviticus and Numbers is introduced. The symbolism of the sacrificial cult and purity system, the differences between moral and ritual impurity, as well as holiness and purity are explained within the Priestly context. The concept of holiness and imitatio dei, or human imitation of God, is explained.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/09%20-%20The%20Priestly%20Legacy_%20%20Cult%20and%20Sacrifice%2C%20Purity%20and%20Holiness%20in%20Leviticus%20and%20Numbers.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/09%20-%20The%20Priestly%20Legacy_%20%20Cult%20and%20Sacrifice%2C%20Purity%20and%20Holiness%20in%20Leviticus%20and%20Numbers.mp3" length="46641825" type="audio/mpeg"/>
      <itunes:duration>48:35</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Biblical Law: The Three Legal Corpora of JE (Exodus), P (Leviticus and Numbers) and D (Deuteronomy)</title>
      <description><![CDATA[This lecture introduces biblical law in a comparative approach that identifies similarities and differences between Israelite law and other Ancient Near Eastern legal traditions, such as the Code of Hammurabi. Distinctive features of Israelite law are explained as flowing from the claim of divine authorship.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/10%20-%20Biblical%20Law_%20%20The%20Three%20Legal%20Corpora%20of%20JE%20%28Exodus%29%2C%20P%20%28Leviticus%20and%20Numbers%29%20and%20D%20%28Deuteronomy%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/10%20-%20Biblical%20Law_%20%20The%20Three%20Legal%20Corpora%20of%20JE%20%28Exodus%29%2C%20P%20%28Leviticus%20and%20Numbers%29%20and%20D%20%28Deuteronomy%29.mp3" length="48676033" type="audio/mpeg"/>
      <itunes:duration>50:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: On the Steps of Moab: Deuteronomy</title>
      <description><![CDATA[This lecture, focusing on Moses’s final address to the Israelites and transfer of authority to Joshua, describes Moses as the paradigmatic leader of biblical tradition. The structure of Deuteronomy is then outlined.   Attention is given to updated and revised laws within Deuteronomy which exemplify the activity of adaptive interpretation of earlier tradition. The main themes of Deuteronomy are presented and include the notion of God’s chosen people and chosen city, social justice, covenantal love and the centralization of cultic worship.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/11%20-%20On%20the%20Steps%20of%20Moab_%20Deuteronomy.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/11%20-%20On%20the%20Steps%20of%20Moab_%20Deuteronomy.mp3" length="46001094" type="audio/mpeg"/>
      <itunes:duration>47:55</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: The Deuteronomistic History: Life in the Land (Joshua and Judges)</title>
      <description><![CDATA[This lecture concludes the study of Deuteronomy and traces the contribution of the Deuteronomistic School: a historiosophy according to which Israel’s fortunes are dependent upon and an indicator of her fidelity to the covenant. The books of the Former Prophets are introduced with attention to their historical and geographical context. The book of Joshua’s account of Israel’s conquest of Canaan is contrasted with scholarly accounts of Israel’s emergence in Canaan and formation as a nation state.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/12%20-%20The%20Deuteronomistic%20History_%20%20Life%20in%20the%20Land%20%28Joshua%20and%20Judges%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/12%20-%20The%20Deuteronomistic%20History_%20%20Life%20in%20the%20Land%20%28Joshua%20and%20Judges%29.mp3" length="48296944" type="audio/mpeg"/>
      <itunes:duration>50:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: The Deuteronomistic History: Prophets and Kings (1 and 2 Samuel)</title>
      <description><![CDATA[The transition from a tribal society under the leadership of elders and eventually charismatic “judges” to a nation under a monarch is traced through the books of Judges and 1 and 2 Samuel. Early stories of local heroes are woven together into a larger history that conforms to the exilic perspectives of the Deuteronomistic School. An extended look at representations of Saul and David (including God’s covenant with David) reveal historical shifts and some ambivalence about monarchy and the ideal form of leadership.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/13%20-%20The%20Deuteronomistic%20History_%20%20Prophets%20and%20Kings%20%281%20and%202%20Samuel%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/13%20-%20The%20Deuteronomistic%20History_%20%20Prophets%20and%20Kings%20%281%20and%202%20Samuel%29.mp3" length="47538766" type="audio/mpeg"/>
      <itunes:duration>49:31</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: The Deuteronomistic History: Response to Catastrophe (1 and 2 Kings)</title>
      <description><![CDATA[The tension between covenant theology, emphasizing the conditional Mosaic convenant from Mt. Sinai, and royal theology emphasizing the unconditional covenant with David in his palace on Mt. Zion, is traced. Following Solomon’s death, the united kingdom separated into a northern and a southern kingdom (named Israel and Judah respectively), the former falling to the Assyrians in 722 and the latter to the Babylonians in 586. Analysis of the Deuteronomistic School’s response to these historical crises and subsequent exile to Babylonia is evidenced through redaction criticism.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/14%20-%20The%20Deuteronomistic%20History_%20%20Response%20to%20Catastrophe%20%281%20and%202%20Kings%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/14%20-%20The%20Deuteronomistic%20History_%20%20Response%20to%20Catastrophe%20%281%20and%202%20Kings%29.mp3" length="49513623" type="audio/mpeg"/>
      <itunes:duration>51:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Hebrew Prophecy: The Non-Literary Prophets</title>
      <description><![CDATA[This lecture concludes the discussion of the Deuteronomistic historian’s efforts to show that idolatry and associated sins lead to God’s wrath and periods of trouble. The remainder of the lecture is an introduction to the phenomenon of Israelite prophecy which included ecstatic prophecy and prophetic guilds. The non-literary prophets of the historical books of the Bible and their various roles (as God’s zealot; as conscience of the king) are examined.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/15%20-%20Hebrew%20Prophecy_%20The%20Non-literary%20Prophets.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/15%20-%20Hebrew%20Prophecy_%20The%20Non-literary%20Prophets.mp3" length="47848056" type="audio/mpeg"/>
      <itunes:duration>49:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Literary Prophecy: Amos</title>
      <description><![CDATA[This lecture introduces the literary prophets of both the northern and southern kingdoms. The prophetic books are anthologies of oracles the sequence of which is often determined by literary rather than chronological considerations. This lecture studies the literary features and major themes of classical Israelite prophecy as evidenced in particular in the book of the eighth-century northern prophet Amos. The prophets denounced moral decay and false piety as directly responsible for the social injustice that outrages God. While the Deuteronomist blames the nation’s misfortunes on acts of idolatry, the prophets stress that the nation will be punished for everyday incidents of immorality. The literary prophets counterbalance their warnings with messages of great hope and consolation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/16%20-%20Literary%20Prophecy_%20Amos.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/16%20-%20Literary%20Prophecy_%20Amos.mp3" length="46071729" type="audio/mpeg"/>
      <itunes:duration>47:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: Literary Prophecy: Hosea and Isaiah</title>
      <description><![CDATA[The lecture focuses on the eighth-century northern prophet Hosea, a linguistically difficult book set against the backdrop of the expansionist Assyrian Empire. Hosea’s marriage symbolizes Israel’s relationship with God and serves to remind Israel of God’s forbearance and Israel’s obligations and pledge to loyalty under the covenant at Sinai. The second half of the lecture shifts to Isaiah and his emphasis on the Davidic Covenant, rather than the Mosaic one, a key distinction between him and Hosea. Themes in Isaiah include the salvation of a remnant, Israel’s election to a mission and an eschatology that centers around a “messiah” (anointed) king of the house of David.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/17%20-%20Literary%20Prophecy_%20%20Hosea%20and%20Isaiah.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/17%20-%20Literary%20Prophecy_%20%20Hosea%20and%20Isaiah.mp3" length="47002942" type="audio/mpeg"/>
      <itunes:duration>48:57</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Literary Prophecy: Micah, Zephaniah, Nahum and Habbakuk</title>
      <description><![CDATA[Micah, eighth-century southern prophet and contemporary of Isaiah, is discussed. Structurally, the book of Micah alternates three prophecies of doom and destruction and three prophecies of hope and restoration. Micah attacks the doctrine of the inviolability of Zion and employs the literary form of a covenant lawsuit (or riv) in his denunciation of the nation. Several short prophetic books are also discussed: Zephaniah; the Book of Nahum, depicting the downfall of Assyria and distinguished for its vivid poetic style; and the book of Habbakuk, which contains philosophical musings on God’s behavior. The final part of the lecture turns to the lengthy book of Jeremiah. A prophet at the time of the destruction and exile, Jeremiah predicted an end to the exile after 70 years and a new covenant that would be inscribed on the hearts of the nation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/18%20-%20Literary%20Prophecy_%20Micah%2C%20Zephaniah%2C%20Nahum%20and%20Habbakuk.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/18%20-%20Literary%20Prophecy_%20Micah%2C%20Zephaniah%2C%20Nahum%20and%20Habbakuk.mp3" length="46718730" type="audio/mpeg"/>
      <itunes:duration>48:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Literary Prophecy: Perspectives on the Exile (Jeremiah, Ezekiel and 2nd Isaiah)</title>
      <description><![CDATA[The destruction of Jerusalem challenged the faith of the nation. What was the meaning of this event and how could such tremendous evil and suffering be reconciled with the nature of God himself? Professor Hayes shows how Israel’s prophets attempted to answer this question, turning the nation’s defeat and despair into an occasion for renewing faith in Israel’s God. The lecture continues with an in-depth study of the book of Ezekiel. Ezekiel’s denunciations of Jerusalem are among the most lurid and violent in the Bible and he concludes that destruction is the only possible remedy. Ezekiel’s visions include God’s withdrawal from Jerusalem to be with his people in exile, and his ultimate return. Ezekiel’s use of dramatic prophetic signs, his rejection of collective divine punishment and assertion of individual responsibility are discussed. The last part of the lecture turns to Second Isaiah and the famous “servant songs” that find a universal significance in Israel’s suffering.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/19%20-%20Literary%20Prophecy_%20Perspectives%20on%20the%20Exile%20%28Jeremiah%2C%20Ezekiel%20and%202nd%20Isaiah%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/19%20-%20Literary%20Prophecy_%20Perspectives%20on%20the%20Exile%20%28Jeremiah%2C%20Ezekiel%20and%202nd%20Isaiah%29.mp3" length="45234139" type="audio/mpeg"/>
      <itunes:duration>47:07</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Responses to Suffering and Evil: Lamentations and Wisdom Literature</title>
      <description><![CDATA[This lecture begins with the Book of Lamentations, a short book of dirges that laments the destruction of Jerusalem and moves on to introduce the third and final section of the Hebrew Bible - the Ketuvim, or “Writings.” This section of the Bible contains three books that exemplify the ancient Near Eastern literary genre of “Wisdom” – Proverbs, Job and Ecclesiastes. Proverbs reinforces the Deuteronomistic idea of divine retributive justice according to which the good prosper and the evil are punished. The conventional assumption of a moral world order is attacked in the Book of Job. The book explores whether people will sustain virtue when suffering and afflicted, and brings charges of negligence and mismanagement against God for failing to punish the wicked and allowing the righteous to suffer.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/20%20-%20Responses%20to%20Suffering%20and%20Evil_%20%20Lamentations%20and%20Wisdom%20Literature.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/20%20-%20Responses%20to%20Suffering%20and%20Evil_%20%20Lamentations%20and%20Wisdom%20Literature.mp3" length="50762903" type="audio/mpeg"/>
      <itunes:duration>52:52</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Biblical Poetry: Psalms and Song of Songs</title>
      <description><![CDATA[After a detailed explanation of the requirements for the paper assignment, Professor Hayes turns to the Writings - the third section of the Bible - and considers a recent approach to the study of the Bible, called canonical criticism. The books in this section of the Bible explore various questions associated with suffering and evil. An example is the book of Ecclesiastes which constitutes a second attack on the optimism and piety of conventional religious thinking. The lecture concludes with a discussion of a number of Psalms, their genre, purpose, and language.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/21%20-%20Biblical%20Poetry_%20Psalms%20and%20Song%20of%20Songs.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/21%20-%20Biblical%20Poetry_%20Psalms%20and%20Song%20of%20Songs.mp3" length="46712461" type="audio/mpeg"/>
      <itunes:duration>48:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: The Restoration: 1 and 2 Chronicles, Ezra and Nehemiah</title>
      <description><![CDATA[This lecture continues the discussion of the psalms, and the genres and forms in which they appear, such as psalms of praise and thanksgiving, divine kingship, lament and petition, blessing and cursing, or wisdom. Another poetic book of the Bible is the Song of Songs, an erotic work the sexually explicit content of which has been piously reinterpreted over the centuries. The second half of the lecture turns to the period of the Restoration when the Judean exiles returned to what was now the province of Yehud under Cyrus, the Persian ruler. The books of 1 and 2 Chronicles refer to some of the events of this time as well as the books of Ezra and Nehemiah. Ezra and Nehemiah are said to renew the Mosaic covenant with the Torah at its center, and to institute a number of social and religious reforms (including a universal ban on intermarriage that will ultimately fail) in order to consolidate the struggling community.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/22%20-%20The%20Restoration_%201%20and%202%20Chronicles%2C%20Ezra%20and%20Nehemiah.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/22%20-%20The%20Restoration_%201%20and%202%20Chronicles%2C%20Ezra%20and%20Nehemiah.mp3" length="47315158" type="audio/mpeg"/>
      <itunes:duration>49:17</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: Visions of the End: Daniel and Apocalyptic Literature</title>
      <description><![CDATA[The Book of Ruth, in which a foreign woman enters the community of Israel and becomes great-grandmother to none other than King David, expresses a view of gentiles entirely opposed to that of Ezra and Nehemiah. Other prophets of the Restoration period are discussed, including Third Isaiah who also envisions other nations joining Israel in the worship of Yahweh. This period also sees the rise of apocalyptic literature in works like Zechariah, Joel and Daniel. Written during a period of persecution in the 2nd c. BCE the book of Daniel contains many features and themes of apocalyptic literature, including an eschatology according to which God dramatically intervenes in human history, destroying the wicked (understood as other nations) and saving the righteous (understood as Israel).]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/23%20-%20Visions%20of%20the%20End_%20Daniel%20and%20Apocalyptic.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/23%20-%20Visions%20of%20the%20End_%20Daniel%20and%20Apocalyptic.mp3" length="47848056" type="audio/mpeg"/>
      <itunes:duration>49:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Alternative Visions: Esther, Ruth, and Jonah</title>
      <description><![CDATA[In this lecture, two final books of the Bible are examined and their attitudes towards foreign nations compared. In contrast to Daniel’s reliance on divine intervention to punish the wicked, the book of Esther focuses on human initiative in defeating the enemies of Israel. Finally, the book of Jonah–in which the wicked Assyrians repent and are spared divine punishment–expresses the view that God is compassionate and concerned with all creation. Professor Hayes concludes the course with remarks regarding the dynamic and complex messages presented in the Hebrew Bible.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/24%20-%20Alternative%20Visions_%20%20Esther%2C%20Ruth%20and%20Jonah.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Introduction_to_the_Old_Testament_Hebrew_Bible/24%20-%20Alternative%20Visions_%20%20Esther%2C%20Ruth%20and%20Jonah.mp3" length="27752995" type="audio/mpeg"/>
      <itunes:duration>28:54</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/rlst-145-2006/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
  </channel>
</rss>
