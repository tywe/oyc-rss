<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>PHIL 176: Death</title>
    <link>https://oyc.yale.edu/death/phil-176</link>
    <description>From Open Yale Courses. PHIL 176, Spring 2007. There is one thing I can be sure of: I am going to die. But what am I to make of that fact? This course will examine a number of issues that arise once we begin to reflect on our mortality. The possibility that death may not actually be the end is considered. Are we, in some sense, immortal? Would immortality be desirable? Also a clearer notion of what it is to die is examined. What does it mean to say that a person has died? What kind of fact is that? And, finally, different attitudes to death are evaluated. Is death an evil? How? Why? Is suicide morally permissible? Is it rational? How should the knowledge that I am going to die affect the way I live my life?</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:15 +0000</lastBuildDate>
    <itunes:author>Shelly Kagan and Yale University</itunes:author>
    <dc:creator>Shelly Kagan</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/phil-176-2007/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Course Introduction</title>
      <description><![CDATA[Professor Kagan introduces the course and the material that will be covered during the semester. He aims to clarify what the class will focus on in particular and which subjects it will steer away from. The emphasis will be placed on philosophical questions that arise when one contemplates the nature of death. The first half of the course will address metaphysical questions while the second half will focus on value theory.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/01%20-%20Course%20introduction.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/01%20-%20Course%20introduction.mp3" length="44212229" type="audio/mpeg"/>
      <itunes:duration>46:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: The Nature of Persons: Dualism vs. Physicalism</title>
      <description><![CDATA[Professor Kagan discusses the two main positions with regard to the question, “What is a person?” On the one hand, there is the dualist view, according to which a person is a body and a soul. On the other hand, the physicalist view argues that a person is just a body. The body, however, has a certain set of abilities and is capable of a large range of activities.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/02%20-%20The%20nature%20of%20persons_%20dualism%20vs.%20physicalism.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/02%20-%20The%20nature%20of%20persons_%20dualism%20vs.%20physicalism.mp3" length="39967435" type="audio/mpeg"/>
      <itunes:duration>41:37</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Arguments for the Existence of the Soul, Part I</title>
      <description><![CDATA[The lecture focuses on arguments that might be offered as proof for the existence of the soul. The first series of arguments discussed is those known as “inferences to the best explanation.” That is, we posit the existence of things we cannot see so as to explain something else that is generally agreed to take place.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/03%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/03%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20I.mp3" length="43963961" type="audio/mpeg"/>
      <itunes:duration>45:47</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Introduction to Plato's Phaedo; Arguments for the Existence of the Soul, Part II</title>
      <description><![CDATA[After a brief introduction to Plato’s Phaedo, more arguments are offered in this lecture in defense of the existence of an immaterial soul. The emphasis here is on the fact that we need to believe in the existence of a soul in order to explain the claim that we possess free will. This is an argument dualists use as an objection to the physicalists: since no merely physical entity could have free will, there must be more to us than just being a physical object.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/04%20-%20Introduction%20to%20Plato%27s%20Phaedo%3B%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20II.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/04%20-%20Introduction%20to%20Plato%27s%20Phaedo%3B%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20II.mp3" length="47099909" type="audio/mpeg"/>
      <itunes:duration>49:03</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Arguments for the Existence of the Soul, Part III: Free Will and Near-Death Experiences</title>
      <description><![CDATA[Professor Kagan discusses in detail the argument of free will as proof for the existence of an immaterial soul. The argument consists of three premises: 1) We have free will. 2) Nothing subject to determinism has free will. 3) All purely physical systems are subject to determinism. The conclusion drawn from this is that humans are not a purely physical system; but Professor Kagan explains why this argument is not truly compelling. In addition, near-death experiences and the Cartesian argument are discussed at length.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/05%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20III_%20Free%20will%20and%20near-death%20experiences.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/05%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20III_%20Free%20will%20and%20near-death%20experiences.mp3" length="46114361" type="audio/mpeg"/>
      <itunes:duration>48:02</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Arguments for the Existence of the Soul, Part IV; Plato, Part I</title>
      <description><![CDATA[The lecture begins with a continued discussion of the Cartesian argument and its weaknesses. The lecture then turns to Plato’s metaphysical views in the context of his work, Phaedo. The key point in the discussion is the idea that in addition to the ordinary empirical world that we are familiar with, we posit the existence of a second realm in which the Platonic forms exist. These forms are the abstract properties that we attribute to physical objects, such as beauty, justice, goodness and so on. Since it is the soul that conceives of these Platonic forms and ideas, Plato argues that the soul not only outlives the body but lasts forever. It is perfect, immaterial and indestructible.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/06%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20IV%3B%20Plato%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/06%20-%20Arguments%20for%20the%20existence%20of%20the%20soul%2C%20Part%20IV%3B%20Plato%2C%20Part%20I.mp3" length="34272323" type="audio/mpeg"/>
      <itunes:duration>35:42</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 7: Plato, Part II: Arguments for the Immortality of the Soul</title>
      <description><![CDATA[The discussion of Plato’s Phaedo continues, presenting more arguments for the existence and immortality of the soul. One such argument is “the argument from the nature of the forms,” which states that because the forms are non-physical objects and cannot be grasped by something physical like the body, it follows that they must be grasped by the soul which must be non-physical as well. This argument is followed by the “argument from recycling” and “the argument from recollection.”]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/07%20-%20Plato%2C%20Part%20II_%20Arguments%20for%20the%20immortality%20of%20the%20soul.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/07%20-%20Plato%2C%20Part%20II_%20Arguments%20for%20the%20immortality%20of%20the%20soul.mp3" length="44763517" type="audio/mpeg"/>
      <itunes:duration>46:37</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Plato, Part III: Arguments for the Immortality of the Soul (cont.)</title>
      <description><![CDATA[The lecture focuses exclusively on one argument for the immortality of the soul from Plato’s Phaedo, namely, “the argument from simplicity.” Plato suggests that in order for something to be destroyed, it must have parts, that is, it must be possible to “take it apart.” Arguing that the soul is simple, that it does not have parts, Plato believes that it would logically follow that the soul is indestructible.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/08%20-%20Plato%2C%20Part%20III_%20Arguments%20for%20the%20immortality%20of%20the%20soul%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/08%20-%20Plato%2C%20Part%20III_%20Arguments%20for%20the%20immortality%20of%20the%20soul%20%28cont.%29.mp3" length="47906570" type="audio/mpeg"/>
      <itunes:duration>49:54</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Plato, Part IV: Arguments for the Immortality of the Soul (cont.)</title>
      <description><![CDATA[Professor Kagan elaborates on the “argument from simplicity” and discusses in detail Plato’s claims that the soul is simple, changeless and therefore indestructible. The final Platonic argument under discussion is the “argument from essential properties” in which the essential properties of the soul are addressed. At the end of the lecture the question of whether one needs to argue for physicalism is posed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/09%20-%20Plato%2C%20Part%20IV_%20Arguments%20for%20the%20immortality%20of%20the%20soul%20%28cont.%29.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/09%20-%20Plato%2C%20Part%20IV_%20Arguments%20for%20the%20immortality%20of%20the%20soul%20%28cont.%29.mp3" length="48322857" type="audio/mpeg"/>
      <itunes:duration>50:20</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Personal Identity, Part I: Identity Across Space and Time and the Soul Theory</title>
      <description><![CDATA[The lecture focuses on the question of the metaphysical key to personal identity. What does it mean for a person that presently exists to be the very same person in the future? The first approach to answering this question is the “soul theory,” that is, the key to being the same person is having the same soul. Difficulties with that approach are then discussed, independent of the question whether souls exist or not.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/10%20-%20Personal%20identity%2C%20Part%20I_%20Identity%20across%20space%20and%20time%20and%20the%20soul%20theory.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/10%20-%20Personal%20identity%2C%20Part%20I_%20Identity%20across%20space%20and%20time%20and%20the%20soul%20theory.mp3" length="47836771" type="audio/mpeg"/>
      <itunes:duration>49:49</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Personal Identity, Part II: The Body Theory and the Personality Theory</title>
      <description><![CDATA[Two more views regarding the metaphysical key to personal identity are discussed: the body view and the personality view. According to the body view, an individual is identified in terms of his or her physical body. According to the personality view, an individual is identified by his or her unique set of beliefs, desires, memories, goals, and so on.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/11%20-%20Personal%20identity%2C%20Part%20II_%20The%20body%20theory%20and%20the%20personality%20theory.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/11%20-%20Personal%20identity%2C%20Part%20II_%20The%20body%20theory%20and%20the%20personality%20theory.mp3" length="48553153" type="audio/mpeg"/>
      <itunes:duration>50:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Personal Identity, Part III: Objections to the Personality Theory</title>
      <description><![CDATA[The lecture focuses on the problems directly related to the personality theory as key to personal identity. The theory states that a person retains his or her individuality so long as he or she has the same ongoing personality. The main objection raised to this claim is the problem of duplication. The lecture explores cases in which the same personality has been transferred or exported to multiple bodies.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/12%20-%20Personal%20identity%2C%20Part%20III_%20Objections%20to%20the%20personality%20theory.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/12%20-%20Personal%20identity%2C%20Part%20III_%20Objections%20to%20the%20personality%20theory.mp3" length="49768578" type="audio/mpeg"/>
      <itunes:duration>51:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Personal Identity, Part IV: What Matters?</title>
      <description><![CDATA[The personality theory is revised to state that the key to personal identity is having the same personality provided that there is no branching, that is, provided there is no transfer or duplication of the same personality from one body to another. Similar “no branching” requirements are added to the other theories as well. At the end of class, Professor Kagan suggests a shift from thinking about the survival of the soul in terms of “what does it take to survive?” to “what is it that matters in survival?”]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/13%20-%20Personal%20identity%2C%20Part%20IV%3B%20What%20matters_.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/13%20-%20Personal%20identity%2C%20Part%20IV%3B%20What%20matters_.mp3" length="46923112" type="audio/mpeg"/>
      <itunes:duration>48:52</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 14: What Matters (cont.); The Nature of Death, Part I</title>
      <description><![CDATA[The suggestion is made that what matters in survival is the future existence of someone with a personality similar to one’s own. Professor Kagan then turns to the question, “what is it to die?”. In answering this question, attention is first drawn to the bodily and mental functions that are crucial in defining the moment of death.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/14%20-%20What%20matters%20%28cont.%29%3B%20The%20nature%20of%20death%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/14%20-%20What%20matters%20%28cont.%29%3B%20The%20nature%20of%20death%2C%20Part%20I.mp3" length="45407592" type="audio/mpeg"/>
      <itunes:duration>47:17</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 15: The Nature of Death (cont.); Believing You Will Die</title>
      <description><![CDATA[The lecture explores the question of the state of being dead. Even though the most logical claim seems to be that when a person stops P-functioning he or she is dead, a more careful consideration must allow for exceptions, such as when one is asleep or in a coma. Professor Kagan then suggests that on some level nobody believes that he or she is going to die. As a case in point, he takes Tolstoy’s famous character Ivan Ilych.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/15%20-%20The%20nature%20of%20death%20%28cont.%29%3B%20Believing%20you%20will%20die.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/15%20-%20The%20nature%20of%20death%20%28cont.%29%3B%20Believing%20you%20will%20die.mp3" length="42479788" type="audio/mpeg"/>
      <itunes:duration>44:14</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 16: Dying Alone; The Badness of Death, Part I</title>
      <description><![CDATA[Professor Kagan puts forward the claim that Tolstoy’s character Ivan Ilych is quite the typical man in terms of his views on mortality. All of his life he has known that death is imminent but has never really believed it. When he suddenly falls ill and is about to die, the fact of his mortality shocks him. In trying to further access how people think about death, Professor Kagan explores the claim that “we all die alone,” presents a variety of arguments against it and ends by considering whether the primary badness of death could lie in the effects on those who are left behind.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/16%20-%20Dying%20alone%3B%20The%20badness%20of%20death%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/16%20-%20Dying%20alone%3B%20The%20badness%20of%20death%2C%20Part%20I.mp3" length="47850145" type="audio/mpeg"/>
      <itunes:duration>49:50</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 17: The Badness of Death, Part II: The Deprivation Account</title>
      <description><![CDATA[This lecture continues to explore the issue of why death may be bad. According to the deprivation account, what is bad about death is the fact that because one ceases to exist, one becomes deprived of the good things in life. Being dead is not intrinsically bad; it is comparatively bad and one is worse off only by virtue of not being able to enjoy the things one enjoyed while alive, such as watching the sunset, listening to music, and discussing philosophy.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/17%20-%20The%20badness%20of%20death%2C%20Part%20II_%20The%20deprivation%20account.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/17%20-%20The%20badness%20of%20death%2C%20Part%20II_%20The%20deprivation%20account.mp3" length="49540372" type="audio/mpeg"/>
      <itunes:duration>51:36</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 18: The Badness of Death, Part III; Immortality, Part I</title>
      <description><![CDATA[The discussion of the badness of death continues by asking whether it is bad that we do not exist before our birth. The views of a number of contemporary philosophers, such as Tom Nagle, Fred Feldman, and Derek Parfit, are introduced. Then Professor Kagan turns to the subject of immortality. Would it be desirable to live forever, and if so, under what circumstances one might enjoy such a prolonged existence? The lecture concludes with Bernard Williams’ take on immortality which posits that no kind of human life can continue to be enjoyable and attractive for eternity.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/18%20-%20The%20badness%20of%20death%2C%20Part%20III%3B%20Immortality%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/18%20-%20The%20badness%20of%20death%2C%20Part%20III%3B%20Immortality%2C%20Part%20I.mp3" length="48547719" type="audio/mpeg"/>
      <itunes:duration>50:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 19: Immortality Part II; The Value of Life, Part I</title>
      <description><![CDATA[The lecture begins with further exploration of the question of whether it is desirable to live forever under the right circumstances, and then turns to consideration of some alternative theories of the nature of well-being. What makes a life worth living? One popular theory is hedonism, but the thought experiment of being on an “experience machine” suggests that this view may be inadequate.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/19%20-%20Immortality%20Part%20II%3B%20The%20value%20of%20life%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/19%20-%20Immortality%20Part%20II%3B%20The%20value%20of%20life%2C%20Part%20I.mp3" length="47053515" type="audio/mpeg"/>
      <itunes:duration>49:00</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 20: The Value of Life, Part II; Other Bad Aspects of Death, Part I</title>
      <description><![CDATA[Lecture 20 continues the discussion of the value of life. It considers the neutral container theory, which holds that the value of life is simply a function of its contents, both pleasant and painful, and contrasts this with the valuable container theory, which assigns value to being alive itself. The lecture then turns to a consideration of some of the other aspects of death that may contribute to the badness of death. Among the issues addressed are the inevitability, variability and unpredictability of death.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/20%20-%20The%20value%20of%20life%2C%20Part%20II%3B%20Other%20bad%20aspects%20of%20death%2C%20Part%20I.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/20%20-%20The%20value%20of%20life%2C%20Part%20II%3B%20Other%20bad%20aspects%20of%20death%2C%20Part%20I.mp3" length="48630893" type="audio/mpeg"/>
      <itunes:duration>50:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Other Bad Aspects of Death, Part II</title>
      <description><![CDATA[Further bad aspects of death are considered, including ubiquity, or the fact that death may occur at any time and strike anyone. Professor Kagan invites students to contemplate the possibility of death-free time periods, vacation spots, and activities. Then there is consideration of the value of the human condition, which consists of life, followed by death. Finally, the question is raised as to whether it could be appropriate to refuse to face the facts about our mortality. Professor Kagan distinguishes between two ways in which thinking about these could influence human behavior. On the one hand, it may give one the reason to behave differently; on the other hand, it may just cause a change in behavior.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/21%20-%20Other%20bad%20aspects%20of%20death%2C%20Part%20II.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/21%20-%20Other%20bad%20aspects%20of%20death%2C%20Part%20II.mp3" length="47788705" type="audio/mpeg"/>
      <itunes:duration>49:46</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 22: Fear of Death</title>
      <description><![CDATA[Professor Kagan explores the issue of how thinking about death may influence the way we live. Fear as an emotional response to death is discussed as well as whether it is appropriate and under what conditions. A distinction is made between fear of the process of dying, and fear of death itself and what may come when one is dead. Finally, a number of other negative emotions are considered as possible appropriate responses to death and dying, such as anger, sadness, and sorrow.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/22%20-%20Fear%20of%20death.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/22%20-%20Fear%20of%20death.mp3" length="45910397" type="audio/mpeg"/>
      <itunes:duration>47:49</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
    <item>
      <title>Lecture 23: How to Live Given the Certainty of Death</title>
      <description><![CDATA[In this lecture, Professor Kagan invites students to pose the question of how one should live life knowing that it will certainly end in death. He also explores the issue of how we should set our goals and how we should go about achieving them, bearing in mind the time constraints. Other questions raised are how this ultimately affects the quality of our work and our accomplishments, as well as how we decide what is worth doing in life.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/23%20-%20How%20to%20live%20given%20the%20certainty%20of%20death.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/23%20-%20How%20to%20live%20given%20the%20certainty%20of%20death.mp3" length="44716705" type="audio/mpeg"/>
      <itunes:duration>46:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-23.jpeg"/>
      <itunes:order>23</itunes:order>
    </item>
    <item>
      <title>Lecture 24: Suicide, Part I: The Rationality of Suicide</title>
      <description><![CDATA[This is the first of a series of lectures on suicide. Two very distinct contexts are presented in which the subject can be further explored. The first is rationality and the question of under what circumstances it makes sense to end one’s own life. The second is morality and the question of whether we can ever ethically justify resorting to suicide. The lecture’s focus is on the rational requirements of suicide, and Professor Kagan introduces a number of cases which demonstrate that ending one’s life, in certain instances, may be rationally sound.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/24%20-%20Suicide%2C%20Part%20I_%20The%20rationality%20of%20suicide.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/24%20-%20Suicide%2C%20Part%20I_%20The%20rationality%20of%20suicide.mp3" length="43495429" type="audio/mpeg"/>
      <itunes:duration>45:18</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-24.jpeg"/>
      <itunes:order>24</itunes:order>
    </item>
    <item>
      <title>Lecture 25: Suicide, Part II: Deciding under Uncertainty</title>
      <description><![CDATA[The discussion of suicide continues. A few more cases are introduced to consider circumstances under which it might be rational to end one’s life, and more graphs are drawn that show relevant variations in the quality of one’s life. A question is then posed about how one should make a decision about continuing or ending life, given that one cannot know the future with certainty. Finally, two quick moral arguments concerning suicide which rest on theological premises are presented.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/25%20-%20Suicide%2C%20Part%20II_%20Deciding%20under%20uncertainty.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/25%20-%20Suicide%2C%20Part%20II_%20Deciding%20under%20uncertainty.mp3" length="48245953" type="audio/mpeg"/>
      <itunes:duration>50:15</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-25.jpeg"/>
      <itunes:order>25</itunes:order>
    </item>
    <item>
      <title>Lecture 26: Suicide, Part III: The Morality of Suicide and Course Conclusion</title>
      <description><![CDATA[The lecture begins by examining the consequences a suicide has on both the person committing it and those around this person. The question is raised, however, whether this factor is the only that counts morally, as utilitarians claim, or whether other factors matter morally as well, as deontologists claim. The moral relevance of a deontological prohibition against harming the innocent is considered. A concluding summary of the course is offered.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Death/26%20-%20Suicide%2C%20Part%20III_%20The%20morality%20of%20suicide%20and%20course%20conclusion.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Death/26%20-%20Suicide%2C%20Part%20III_%20The%20morality%20of%20suicide%20and%20course%20conclusion.mp3" length="45863585" type="audio/mpeg"/>
      <itunes:duration>47:46</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phil-176-2007/img/episode-26.jpeg"/>
      <itunes:order>26</itunes:order>
    </item>
  </channel>
</rss>
