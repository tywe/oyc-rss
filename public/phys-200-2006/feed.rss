<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0">
  <channel>
    <title>PHYS 200: Fundamentals of Physics I</title>
    <link>https://oyc.yale.edu/physics/phys-200</link>
    <description>From Open Yale Courses. PHYS 200, Fall 2006. This course provides a thorough introduction to the principles and methods of physics for students who have good preparation in physics and mathematics. Emphasis is placed on problem solving and quantitative reasoning. This course covers Newtonian mechanics, special relativity, gravitation, thermodynamics, and waves.</description>
    <itunes:explicit>yes</itunes:explicit>
    <copyright>Yale University, CC BY-NC-SA 3.0</copyright>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-podgen v1.1.0 https://podgen.readthedocs.org</generator>
    <language>en-US</language>
    <lastBuildDate>Mon, 08 Nov 2021 23:19:16 +0000</lastBuildDate>
    <itunes:author>Ramamurti Shankar and Yale University</itunes:author>
    <dc:creator>Ramamurti Shankar</dc:creator>
    <dc:creator>Yale University</dc:creator>
    <itunes:category text="Education">
      <itunes:category text="Courses"/>
    </itunes:category>
    <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/course.jpeg"/>
    <itunes:complete>Yes</itunes:complete>
    <atom:link href="https://tywe-oyc.web.app/phys-200-2006/feed.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title>Lecture 1: Course Introduction and Newtonian Mechanics</title>
      <description><![CDATA[Professor Shankar introduces the course and answers student questions about the material and the requirements. He gives an overview of Newtonian mechanics and explains its two components: kinematics and dynamics. He then reviews basic concepts in calculus through two key equations: x = x0 + v0t + ½ at2 and v2 = v02+ 2 a (x-x0), tracing the fate of a particle in one dimension along the x-axis.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/01%20-%20Course%20Introduction%20and%20Newtonian%20Mechanics.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/01%20-%20Course%20Introduction%20and%20Newtonian%20Mechanics.mp3" length="70417434" type="audio/mpeg"/>
      <itunes:duration>01:13:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-01.jpeg"/>
      <itunes:order>1</itunes:order>
    </item>
    <item>
      <title>Lecture 2: Vectors in Multiple Dimensions</title>
      <description><![CDATA[In this lecture, Professor Shankar discusses motion in more than one dimension. Vectors are introduced and discussed in multiple dimensions. Vector magnitude and direction are also explained. Null vectors, minus vectors, unit and velocity vectors are discussed along with their properties. Finally, several specific problems are solved to demonstrate how vectors can be added, and problems of projectile motion are expounded.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/02%20-%20Vectors%20in%20Multiple%20Dimensions.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/02%20-%20Vectors%20in%20Multiple%20Dimensions.mp3" length="64137179" type="audio/mpeg"/>
      <itunes:duration>01:06:48</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-02.jpeg"/>
      <itunes:order>2</itunes:order>
    </item>
    <item>
      <title>Lecture 3: Newton's Laws of Motion</title>
      <description><![CDATA[This lecture introduces Newton’s Laws of Motion. The First Law on inertia states that every object will remain in a state of rest or uniform motion in a straight line unless acted upon by an external force. The Second Law (F = ma) relates the cause (the force F) to the acceleration. Several different forces are discussed in the context of this law. The lecture ends with the Third Law which states that action and reaction are equal and opposite.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/03%20-%20Newton%27s%20Laws%20of%20Motion.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/03%20-%20Newton%27s%20Laws%20of%20Motion.mp3" length="65632637" type="audio/mpeg"/>
      <itunes:duration>01:08:22</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-03.jpeg"/>
      <itunes:order>3</itunes:order>
    </item>
    <item>
      <title>Lecture 4: Newton's Laws (cont.) and Inclined Planes</title>
      <description><![CDATA[The lecture begins with the application of Newton’s three laws, with the warning that they are not valid for objects that move at speeds comparable to the speed of light or objects that are incredibly small and of the atomic scale. Friction and static friction are discussed. The dreaded inclined plane is dealt with head on. Finally, Professor Shankar explains the motion of objects using Newton’s laws in specific problems related to objects in circular motion, such as roller coasters and a planet orbiting the Sun.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/04%20-%20Newton%27s%20Laws%20%28cont%27d%29%20and%20Inclined%20Planes.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/04%20-%20Newton%27s%20Laws%20%28cont%27d%29%20and%20Inclined%20Planes.mp3" length="64718143" type="audio/mpeg"/>
      <itunes:duration>01:07:24</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-04.jpeg"/>
      <itunes:order>4</itunes:order>
    </item>
    <item>
      <title>Lecture 5: Work-Energy Theorem and Law of Conservation of Energy</title>
      <description><![CDATA[The lecture begins with a review of the loop-the-loop problem. Professor Shankar then reviews basic terminology in relation to work, kinetic energy and potential energy. He then goes on to define the Work-Energy Theorem. Finally, the Law of Conservation of Energy is discussed and demonstrated with specific examples.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/05%20-%20Work-Energy%20Theorem%20and%20Law%20of%20Conservation%20of%20Energy.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/05%20-%20Work-Energy%20Theorem%20and%20Law%20of%20Conservation%20of%20Energy.mp3" length="67452432" type="audio/mpeg"/>
      <itunes:duration>01:10:15</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-05.jpeg"/>
      <itunes:order>5</itunes:order>
    </item>
    <item>
      <title>Lecture 6: Law of Conservation of Energy in Higher Dimensions</title>
      <description><![CDATA[The discussion on the Law of Conservation of Energy continues but is applied in higher dimensions. The notion of a function with two variables is reviewed. Conservative forces are explained and students are taught how to recognize and manufacture them.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/06%20-%20Law%20of%20Conservation%20of%20Energy%20in%20Higher%20Dimensions.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/06%20-%20Law%20of%20Conservation%20of%20Energy%20in%20Higher%20Dimensions.mp3" length="68174247" type="audio/mpeg"/>
      <itunes:duration>01:11:00</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-06.jpeg"/>
      <itunes:order>6</itunes:order>
    </item>
    <item>
      <title>Lecture 8: Dynamics of a Multiple-Body System and Law of Conservation of Momentum</title>
      <description><![CDATA[The dynamics of a many-body system is examined. Through a variety of examples, the professor demonstrates how to locate the center of mass and how to evaluate it for a number of objects. Finally, the Law of Conservation of Momentum is introduced and discussed. The lecture ends with problems of collision in one dimension focusing on the totally elastic and totally inelastic cases.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/07%20-%20Kepler%27s%20Laws.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/07%20-%20Kepler%27s%20Laws.mp3" length="69280167" type="audio/mpeg"/>
      <itunes:duration>01:12:10</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-07.jpeg"/>
      <itunes:order>7</itunes:order>
    </item>
    <item>
      <title>Lecture 9: Rotations, Part I: Dynamics of Rigid Bodies</title>
      <description><![CDATA[Part I of Rotations. The lecture begins with examining rotation of rigid bodies in two dimensions. The concepts of “rotation” and “translation” are explained. The use of radians is introduced. Angular velocity, angular momentum, angular acceleration, torque and inertia are also discussed. Finally, the Parallel Axis Theorem is expounded.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/08%20-%20Dynamics%20of%20Multiple-Body%20System%20and%20Law%20of%20Conservation%20of%20Momentum.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/08%20-%20Dynamics%20of%20Multiple-Body%20System%20and%20Law%20of%20Conservation%20of%20Momentum.mp3" length="69280167" type="audio/mpeg"/>
      <itunes:duration>01:12:10</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-08.jpeg"/>
      <itunes:order>8</itunes:order>
    </item>
    <item>
      <title>Lecture 10: Rotations, Part II: Parallel Axis Theorem</title>
      <description><![CDATA[Part II of Rotations. The lecture begins with an explanation of the Parallel Axis Theorem and how it is applied in problems concerning rotation of rigid bodies. The moment of inertia of a disk is discussed as a demonstration of the theorem. Angular momentum and angular velocity are examined in a variety of problems.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/09%20-%20Rotations_%20Part%20I%3B%20Dynamics%20of%20Rigid%20Bodies.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/09%20-%20Rotations_%20Part%20I%3B%20Dynamics%20of%20Rigid%20Bodies.mp3" length="70896416" type="audio/mpeg"/>
      <itunes:duration>01:13:51</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-09.jpeg"/>
      <itunes:order>9</itunes:order>
    </item>
    <item>
      <title>Lecture 11: Torque</title>
      <description><![CDATA[This lecture is a continuation of an analogue to Newton’s law: τ= lα. While previous problems examined situations in which τ is not zero, this time the focus is on extreme cases in which there is no torque at all. If there is no torque, α is zero and the angular velocity is constant. The lecture starts with a simple example of a seesaw and moves on to discuss a collection of objects that are somehow subject to a variety of forces but remain in static equilibrium.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/10%20-%20Rotations_%20Part%20II%3B%20Parallel%20Axis%20Theorem.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/10%20-%20Rotations_%20Part%20II%3B%20Parallel%20Axis%20Theorem.mp3" length="72430326" type="audio/mpeg"/>
      <itunes:duration>01:15:26</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-10.jpeg"/>
      <itunes:order>10</itunes:order>
    </item>
    <item>
      <title>Lecture 12: Introduction to Relativity</title>
      <description><![CDATA[This is the first of a series of lectures on relativity. The lecture begins with a historical overview and goes into problems that aim to describe a single event as seen by two independent observers. Maxwell’s theory, as well as the Galilean and Lorentz transformations are also discussed.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/11%20-%20Torque.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/11%20-%20Torque.mp3" length="70299152" type="audio/mpeg"/>
      <itunes:duration>01:13:13</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-11.jpeg"/>
      <itunes:order>11</itunes:order>
    </item>
    <item>
      <title>Lecture 13: Lorentz Transformation</title>
      <description><![CDATA[This lecture offers detailed analysis of the Lorentz transformations which relate the coordinates of an event in two frames in relative motion. It is shown how length, time and simultaneity are relative.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/12%20-%20Introduction%20to%20Relativity.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/12%20-%20Introduction%20to%20Relativity.mp3" length="68504435" type="audio/mpeg"/>
      <itunes:duration>01:11:21</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-12.jpeg"/>
      <itunes:order>12</itunes:order>
    </item>
    <item>
      <title>Lecture 14: Introduction to the Four-Vector</title>
      <description><![CDATA[The four-vector is introduced that unifies space-time coordinates x, y, z and t into a single entity whose components get mixed up under Lorentz transformations. The length of this four-vector, called the space-time interval, is shown to be invariant (the same for all observers). Likewise energy and momentum are unified into the energy-momentum four-vector.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/13%20-%20Lorentz%20Transformation.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/13%20-%20Lorentz%20Transformation.mp3" length="65713722" type="audio/mpeg"/>
      <itunes:duration>01:08:27</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-13.jpeg"/>
      <itunes:order>13</itunes:order>
    </item>
    <item>
      <title>Lecture 15: Four-Vector in Relativity</title>
      <description><![CDATA[The discussion of four-vector in relativity continues but this time the focus is on the energy-momentum of a particle. The invariance of the energy-momentum four-vector is due to the fact that rest mass of a particle is invariant under coordinate transformations.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/14%20-%20Introduction%20to%20the%20Four-Vector.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/14%20-%20Introduction%20to%20the%20Four-Vector.mp3" length="69675975" type="audio/mpeg"/>
      <itunes:duration>01:12:34</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-14.jpeg"/>
      <itunes:order>14</itunes:order>
    </item>
    <item>
      <title>Lecture 16: The Taylor Series and Other Mathematical Concepts</title>
      <description><![CDATA[The lecture covers a number of mathematical concepts. The Taylor series is introduced and its properties discussed, supplemented by various examples. Complex numbers are explained in some detail, especially in their polar form. The lecture ends with a discussion of simple harmonic motion.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/15%20-%20Four-Vector%20in%20Relativity.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/15%20-%20Four-Vector%20in%20Relativity.mp3" length="68860954" type="audio/mpeg"/>
      <itunes:duration>01:11:43</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-15.jpeg"/>
      <itunes:order>15</itunes:order>
    </item>
    <item>
      <title>Lecture 17: Simple Harmonic Motion</title>
      <description><![CDATA[The focus of the lecture is simple harmonic motion. Professor Shankar gives several examples of physical systems, such as a mass M attached to a spring, and explains what happens when such systems are disturbed. Amplitude, frequency and period of simple harmonic motion are also defined in the course of the lecture. Several problems are solved in order to demonstrate various cases of oscillation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/16%20-%20The%20Taylor%20Series%20and%20Other%20Mathematical%20Concepts.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/16%20-%20The%20Taylor%20Series%20and%20Other%20Mathematical%20Concepts.mp3" length="70705408" type="audio/mpeg"/>
      <itunes:duration>01:13:39</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-16.jpeg"/>
      <itunes:order>16</itunes:order>
    </item>
    <item>
      <title>Lecture 18: Simple Harmonic Motion (cont.) and Introduction to Waves</title>
      <description><![CDATA[This lecture continues the topic of harmonic motions. Problems are introduced and solved to explore various aspects of oscillation. The second half of the lecture is an introduction to the nature and behavior of waves. Both longitudinal and transverse waves are defined and explained.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/17%20-%20Simple%20Harmonic%20Motion.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/17%20-%20Simple%20Harmonic%20Motion.mp3" length="71028491" type="audio/mpeg"/>
      <itunes:duration>01:13:59</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-17.jpeg"/>
      <itunes:order>17</itunes:order>
    </item>
    <item>
      <title>Lecture 20: Fluid Dynamics and Statics and Bernoulli's Equation</title>
      <description><![CDATA[The focus of the lecture is on fluid dynamics and statics. Different properties are discussed, such as density and pressure. The Archimedes’ Principle is introduced and demonstrated through a number of problems. The final topic of the lecture is Bernoulli’s Equation.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/18%20-%20Simple%20Harmonic%20Motion%20%28con%27t%29%20and%20Introduction%20to%20Waves.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/18%20-%20Simple%20Harmonic%20Motion%20%28con%27t%29%20and%20Introduction%20to%20Waves.mp3" length="72066701" type="audio/mpeg"/>
      <itunes:duration>01:15:04</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-18.jpeg"/>
      <itunes:order>18</itunes:order>
    </item>
    <item>
      <title>Lecture 21: Thermodynamics</title>
      <description><![CDATA[This is the first of a series of lectures on thermodynamics. The discussion begins with understanding “temperature.” Zeroth’s law is introduced and explained. Concepts such as “absolute zero” and “triple point of water” are defined. Measuring temperature through a number of instruments is addressed as well as the different scales of measurement. The second half of the lecture is devoted to heat and heat transfer. Concepts such as “convection” and “conduction” are explained thoroughly.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/19%20-%20Waves.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/19%20-%20Waves.mp3" length="69019779" type="audio/mpeg"/>
      <itunes:duration>01:11:53</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-19.jpeg"/>
      <itunes:order>19</itunes:order>
    </item>
    <item>
      <title>Lecture 22: The Boltzmann Constant and First Law of Thermodynamics</title>
      <description><![CDATA[This lecture continues the topic of thermodynamics, exploring in greater detail what heat is, and how it is generated and measured. The Boltzmann Constant is introduced. The microscopic meaning of temperature is explained. The First Law of Thermodynamics is presented.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/20%20-%20Fluid%20Dynamics%20and%20Statics%20and%20Bernoulli%27s%20Equation.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/20%20-%20Fluid%20Dynamics%20and%20Statics%20and%20Bernoulli%27s%20Equation.mp3" length="69625820" type="audio/mpeg"/>
      <itunes:duration>01:12:31</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-20.jpeg"/>
      <itunes:order>20</itunes:order>
    </item>
    <item>
      <title>Lecture 23: The Second Law of Thermodynamics and Carnot's Engine</title>
      <description><![CDATA[Why does a dropped egg that spatters on the floor not rise back to your hands even though no laws prohibit it? The answer to such irreversibility resides the Second Law of Thermodynamics which explained in this and the next lecture. The Carnot heat engine is discussed in detail to show how there is an upper limit to the efficiency of heat engines and how the concept of entropy arises from macroscopic considerations.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/21%20-%20Thermodynamics.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/21%20-%20Thermodynamics.mp3" length="68603909" type="audio/mpeg"/>
      <itunes:duration>01:11:27</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-21.jpeg"/>
      <itunes:order>21</itunes:order>
    </item>
    <item>
      <title>Lecture 24: The Second Law of Thermodynamics (cont.) and Entropy</title>
      <description><![CDATA[The focus of the lecture is the concept of entropy. Specific examples are given to calculate the entropy change for a number of different processes. Boltzmann’s microscopic formula for entropy is introduced and used to explain irreversibility.]]></description>
      <guid isPermaLink="false">https://archive.org/download/oyc_Fundamentals_of_Physics_I/22%20-%20The%20Boltzmann%20Constant%20and%20First%20Law%20of%20Thermodynamics.mp3</guid>
      <enclosure url="https://archive.org/download/oyc_Fundamentals_of_Physics_I/22%20-%20The%20Boltzmann%20Constant%20and%20First%20Law%20of%20Thermodynamics.mp3" length="71683433" type="audio/mpeg"/>
      <itunes:duration>01:14:40</itunes:duration>
      <itunes:image href="https://tywe-oyc.web.app/phys-200-2006/img/episode-22.jpeg"/>
      <itunes:order>22</itunes:order>
    </item>
  </channel>
</rss>
